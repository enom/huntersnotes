import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WelcomeComponent } from './welcome/welcome.component';
import { TinkerComponent } from './tinker/tinker.component';
import { NotFoundComponent } from './welcome/not-found.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
  {
    path: 'monsters',
    loadChildren: './monsters/monsters.module#MonstersModule'
  },
  { /** @todo Look into removing the listing and just keep top nav for sizes */
    path: 'sizes',
    loadChildren: './sizes/sizes.module#SizesModule'
  },
  {
    path: 'types',
    loadChildren: './types/types.module#TypesModule'
  },
  {
    path: 'expeditions',
    loadChildren: './expeditions/expeditions.module#ExpeditionsModule'
  },
  {
    path: 'drops',
    loadChildren: './drops/drops.module#DropsModule'
  },
  {
    path: 'search',
    children: [
      {
        path: ':text',
        component: SearchComponent
      },
      {
        path: '',
        component: SearchComponent
      }
    ]
  },
  {
    path: '',
    component: WelcomeComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)// , { enableTracing: true })
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
