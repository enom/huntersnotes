import { Data } from '../data/data';
import { Linkable } from '../shared/linkable';

export class Size implements Data {
  id: string;
  type = 'size';
  value: any;
}

export class SizeDecorator implements Linkable {
  size: Size;
  id: string;
  name: string;

  constructor(
    size: Size
  ) {
    this.size = size;
    this.id = size.id;
    this.name = size.value;
  }

  url(): string {
    return `/monsters/${this.id}`;
  }

  label(): string {
    return this.name;
  }
}
