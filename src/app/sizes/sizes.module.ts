import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { SizesRoutingModule } from './sizes-routing.module';
import { SizesComponent } from './sizes.component';

@NgModule({
  imports: [
    SharedModule,
    SizesRoutingModule
  ],
  declarations: [
    SizesComponent
  ]
})

export class SizesModule { }
