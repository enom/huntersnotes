import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Params, ActivatedRoute } from '@angular/router';

import { Size, SizeDecorator } from './size';
import { Monster, MonsterDecorator } from '../monsters/monster';
import { Data, Collection, Decorator } from '../data/data';
import { DataService } from '../data/data.service';

import { Large, Small, Endemic } from '../data/sizes';

@Component({
  selector: 'app-sizes',
  templateUrl: './sizes.component.html',
  styleUrls: ['./sizes.component.scss']
})

export class SizesComponent implements OnInit {
  request: Params;
  monsters: MonsterDecorator[] = [];
  sizes: SizeDecorator[] = [];
  size: SizeDecorator;

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private data: DataService
  ) {
    route.params.subscribe((params: Params) => {
      this.request = params;

      if (params.id) {
        this.getSize(params.id);
      } else {
        this.getSizes();
      }
    });
   }

  ngOnInit() { }

  getSize(id: string): void {
    // Handles associating monsters to our size
    const getMonsters = (monsters: Collection) => {
      this.monsters = monsters.all()
        .filter((monster: Monster) => {
          return monster.notes.find((data: Data) => data.id === id) !== undefined;
        })
        .map((monster: Monster) => new MonsterDecorator(monster));
    };

    // Saves size data and fetches monsters
    const getSize = (size: Size) => {
      this.size = new SizeDecorator(size);
      this.data
        .getMonsters()
        .subscribe(getMonsters);
    };

    this.data
      .getSize(id)
      .subscribe(getSize);
  }

  getSizes(): void {
    const subscribe = (sizes: Collection) => {
      this.sizes = sizes.all().map((size: Size) => new SizeDecorator(size));
    };

    this.data
      .getSizes()
      .subscribe(subscribe);
  }

}
