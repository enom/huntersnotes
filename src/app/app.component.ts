import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  nav: { collapse: boolean } = { collapse: true };

  constructor () { }

  toggleNav () {
    this.nav.collapse = !this.nav.collapse;
  }

  closeNav () {
    this.nav.collapse = true;
  }
}
