import { Component, OnInit, Input } from '@angular/core';

import { DamageDecorator } from '../damage';

@Component({
  selector: 'app-monsters-damage',
  templateUrl: './damage.component.html',
  styleUrls: ['./damage.component.scss']
})

export class MonstersDamageComponent implements OnInit {
  @Input() damages: DamageDecorator[];

  constructor() { }

  ngOnInit() { }

}
