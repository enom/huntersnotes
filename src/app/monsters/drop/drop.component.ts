import { Component, OnInit, Input } from '@angular/core';

import { DropDecorator } from '../../drops/drop';

@Component({
  selector: 'app-monsters-drop',
  templateUrl: './drop.component.html',
  styleUrls: ['./drop.component.scss']
})

export class MonstersDropComponent implements OnInit {
  @Input() drops: DropDecorator[];

  constructor() { }

  ngOnInit() { }

}
