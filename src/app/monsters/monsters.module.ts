import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { MonstersRoutingModule } from './monsters-routing.module';
import { MonstersComponent } from './monsters.component';
import { MonstersDamageComponent } from './damage/damage.component';
import { MonstersDropComponent } from './drop/drop.component';
import { MonstersAnatomyComponent } from './anatomy/anatomy.component';

@NgModule({
  imports: [
    SharedModule,
    MonstersRoutingModule
  ],
  declarations: [
    MonstersComponent,
    MonstersDamageComponent,
    MonstersDropComponent,
    MonstersAnatomyComponent
  ]
})

export class MonstersModule { }
