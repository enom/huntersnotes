import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { MonstersComponent } from './monsters.component';

const routes: Routes = [
  {
    path: ':id',
    component: MonstersComponent
  },
  {
    path: '',
    pathMatch: true,
    redirectTo: 'large'
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class MonstersRoutingModule { }
