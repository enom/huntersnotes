import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Params, ActivatedRoute } from '@angular/router';
import { Observable, Observer } from 'rxjs';

import { DataService } from '../data/data.service';
import { Data, Collection, Decorator } from '../data/data';
import { Large } from '../data/sizes';
import * as Sizes from '../data/sizes';

import { Monster, MonsterDecorator } from './monster';
import { Type, TypeDecorator } from '../types/type';
import { Size, SizeDecorator } from '../sizes/size';
import { Expedition, ExpeditionDecorator } from '../expeditions/expedition';

class Mapped {

}

@Component({
  selector: 'app-monsters',
  templateUrl: './monsters.component.html',
  styleUrls: ['./monsters.component.scss']
})

export class MonstersComponent implements OnInit {
  cache: MonsterDecorator[] = [];
  keyed: { [key: string]: MonsterDecorator[] } = {};

  sizes: SizeDecorator[] = [];
  types: TypeDecorator[] = [];

  monsters: MonsterDecorator[] = [];
  monster: MonsterDecorator;

  filter: SizesDecorator | TypeDecorator;
  request: Params;

  show: { help: boolean, filters: boolean } = { help: false, filters: false };

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private data: DataService
  ) {
    this.route = route;
    this.request = route.params;
  }

  ngOnInit(): void {
    const getParams = (params: Params) => {
      this.request = params;
      this.fetch(params.id);
    };

    const getMonsters = (monsters: Collection) => {
      this.cache = monsters.all()
        .map((monster: Monster) => new MonsterDecorator(monster))
        .sort((a: MonsterDecorator, b: MonsterDecorator) => {
          if (a.name < b.name) { return -1; }
          if (a.name > b.name) { return 1; }
          return 0;
        });

      this.cache
        .forEach((monster: MonsterDecorator) => this.associate(monster));

      this.route.params
        .subscribe(getParams);
    };

    this.data
      .getMonsters()
      .subscribe(getMonsters);
  }

  associate(monster: MonsterDecorator): void {
    const link = (decorator: MonsterDecorator) => (note: Data) => {
      if (note instanceof Size) {
        if (this.sizes.findIndex((dec) => dec.id === note.id) < 0) {
          this.sizes.push(new SizeDecorator(note));
        }
      }

      if (note instanceof Type) {
        if (this.types.findIndex((dec) => dec.id === note.id) < 0) {
          this.types.push(new TypeDecorator(note));
        }
      }
    };

    monster.monster.notes
      .each(link(monster));
  }

  fetch(id: string): void {
    const filter = (data: Data) => (monster: MonsterDecorator) => {
      if (monster.monster.notes.has(data.id)) { return true; }
      if (monster.monster.carves.has(data.id)) { return true; }
      if (monster.monster.rewards.has(data.id)) { return true; }
      return false;
    };

    const subscribed = (data: Data) => {
      this.monster = this.cache
        .find((monster: MonsterDecorator) => monster.id === data.id);

      if (data instanceof Size) {
        this.filter = new SizeDecorator(data);
        this.monsters = this.cache.filter(filter(data));
      }

      if (data instanceof Type) {
        this.filter = new TypeDecorator(data);
        this.monsters = this.cache.filter(filter(data));
      }
    };

    this.data
      .getData(id)
      .subscribe(subscribed);
  }

  searchable(monster: MonsterDecorator, value: string): boolean {
    return (new RegExp(value, 'i')).test(monster.name);
  }

  searched(monsters: MonsterDecorator[] | false): void {
    if (monsters === false) {
      this.fetch(this.filter ? this.filter.id : null);
    } else {
      this.monsters = monsters;
    }
  }

  toggleFilters(): void {
    this.show.filters = !this.show.filters;
  }

  toggleHelp(): void {
    this.show.help = !this.show.help;
  }

}
