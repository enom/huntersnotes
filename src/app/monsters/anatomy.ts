import { Data, Collection } from '../data/data';
import { Damage, DamageDecorator } from './damage';

export class Anatomy extends Collection implements Data {
  id: string;
  type = 'anatomy';
  value: any;
}

export class AnatomyAndAnother extends Anatomy {

}

export class AnatomyOrAnother extends Anatomy {

}

export class AnatomyDecorator {
  id: string;
  name: string;
  breaks: DamageDecorator;
  weakness: DamageDecorator[];

  constructor(
    public anatomy: Anatomy
  ) {
    const breaks = anatomy.reset().some({ type: 'injury' }).partial();
    const weakness = anatomy.reset().some({ type: 'weakness' }).partial();

    this.id = anatomy.id;
    this.name = anatomy.value;
    this.breaks = breaks.map((damage: Damage) => new DamageDecorator(damage))[0];
    this.weakness = weakness.map((damage: Damage) => new DamageDecorator(damage));
  }
}
