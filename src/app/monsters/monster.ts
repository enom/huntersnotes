
import { Data, Collection } from '../data/data';
import { Linkable } from '../shared/linkable';
import { Size, SizeDecorator } from '../sizes/size';
import { Type, TypeDecorator } from '../types/type';
import { Expedition, ExpeditionDecorator } from '../expeditions/expedition';
import { Anatomy, AnatomyDecorator } from './anatomy';
import { Damage, DamageDecorator } from './damage';
import { Drop, Carve,  DropDecorator } from '../drops/drop';

export class Monster implements Data {
  id: string;
  type = 'monster';
  value: any;
  notes: Collection = new Collection;
  attacks: Collection = new Collection;
  carves: Collection = new Collection;
  rewards: Collection = new Collection;
}

export class MonsterDecorator implements Linkable {
  id: string;
  name: string;
  size: SizeDecorator;
  type: TypeDecorator;
  attacks: DamageDecorator[] = [];
  expeditions: ExpeditionDecorator[] = [];
  anatomy: AnatomyDecorator[] = [];
  elements: DamageDecorator[] = [];
  ailments: DamageDecorator[] = [];
  carves: DropDecorator[] = [];
  rewards: DropDecorator[] = [];

  constructor(
    public monster: Monster
  ) {
    // console.log(`decorating`, monster)

    const notes = monster.notes.all();
    const carves = monster.carves.all();
    const rewards = monster.rewards.all();
    const attacks = monster.attacks.all();

    this.id = monster.id;
    this.name = monster.value;

    const size = notes
      .find((data: Data) => data.type === 'size');

    const type = notes
      .find((data: Data) => data.type === 'type');

    const expeditions = notes
      .filter((data: Data) => data.type === 'expedition')
      .map((expedition: Expedition) => new ExpeditionDecorator(expedition));

    const anatomy = notes
      .filter((data: Data) => data.type === 'anatomy')
      .map((part: Anatomy) => new AnatomyDecorator(part));

    const elements = notes
      .filter((data: Data) => data.type === 'element')
      .map((damage: Damage) => new DamageDecorator(damage));

    const ailments = notes
      .filter((data: Data) => data.type === 'ailment')
      .map((damage: Damage) => new DamageDecorator(damage));

    if (size !== undefined) { this.size = new SizeDecorator(size); }
    if (type !== undefined) { this.type = new TypeDecorator(type); }

    this.attacks = attacks.map((damage: Damage) => new DamageDecorator(damage));
    this.expeditions = expeditions;
    this.anatomy = anatomy;
    this.elements = elements;
    this.ailments = ailments;
    this.carves = carves.map((drop: Drop) => new DropDecorator(drop));
    this.rewards = rewards.map((drop: Drop) => new DropDecorator(drop));
  }

  url(): string {
    return `/monsters/${this.id}`;
  }

  label(): string {
    return this.name;
  }
}
