import { Component, OnInit, Input } from '@angular/core';

import { AnatomyDecorator } from '../anatomy';

@Component({
  selector: 'app-monsters-anatomy',
  templateUrl: './anatomy.component.html',
  styleUrls: ['../monsters.component.scss', './anatomy.component.scss']
})

export class MonstersAnatomyComponent implements OnInit {
  @Input() anatomy: AnatomyDecorator[];

  constructor() { }

  ngOnInit() { }

}
