import { MonstersRoutingModule } from './monsters-routing.module';

describe('MonstersRoutingModule', () => {
  let monstersRoutingModule: MonstersRoutingModule;

  beforeEach(() => {
    monstersRoutingModule = new MonstersRoutingModule();
  });

  it('should create an instance', () => {
    expect(monstersRoutingModule).toBeTruthy();
  });
});
