import { Data } from '../data/data';

export class Damage implements Data {
  id: string;
  type: string;
  value: any;
  primary: number;
  secondary: number;

  constructor(
    primary?: number,
    secondary?: number
  ) {
    this.primary = primary;
    this.secondary = secondary;
  }
}

export class DamageDecorator {
  damage: Damage;
  id: string;
  name: string;
  verb: string;
  primary: number;
  secondary: number;

  constructor(
    damage: Damage
  ) {
    this.damage = damage;
    this.id = damage.id;
    this.name = damage.value;
    this.primary = damage.primary;
    this.secondary = damage.secondary;

    switch (damage.id) {
      case 'breakable': this.verb = 'Broken'; break;
      case 'severable': this.verb = 'Severed'; break;
    }
  }
}

export class Element extends Damage {
  type = 'element';
}

export class Ailment extends Damage {
  type = 'ailment';
}

export class Weakness extends Damage {
  type = 'weakness';
}

export class Injury extends Damage {
  type = 'injury';
}
