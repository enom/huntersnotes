import { Size } from '../sizes/size';

export class Large extends Size {
  id = 'large';
  value = 'Large';
}

export class Small extends Size {
  id = 'small';
  value = 'Small';
}

export class Endemic extends Size {
  id = 'endemic';
  value = 'Endemic';
}
