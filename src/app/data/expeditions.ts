
import { Expedition } from '../expeditions/expedition';

export class AncientForest extends Expedition {
  id = 'ancient-forest';
  value = 'Ancient Forest';
}

export class WildspireWaste extends Expedition {
  id = 'wildspire-waste';
  value = 'Wildspire Waste';
}

export class CoralHighlands extends Expedition {
  id = 'coral-highlands';
  value = 'Coral Highlands';
}

export class RottenVale extends Expedition {
  id = 'rotten-vale';
  value = 'Rotten Vale';
}

export class EldersRecess extends Expedition {
  id = 'elders-recess';
  value = 'Elder\'s Recess';
}

export class CavernsOfElDorado extends Expedition {
  id = 'caverns-of-el-dorado';
  value = 'Caverns of El Dorado';
}
