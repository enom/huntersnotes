import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy, AnatomyAndAnother, AnatomyOrAnother } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import {ElderDragon} from '../types';
import { WildspireWaste, EldersRecess } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Back, Wings, Legs, Horn } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun
} from '../damages';

import {
  ElderDragonBone, ElderDragonBlood
} from '../materials';

export class Nergigante extends Monster {
  id = 'nergigante';
  value = 'Nergigante';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new ElderDragon,
    new WildspireWaste, new EldersRecess,
    // Physiology
    new Horn([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Head([
      new Slashing(2), new Bludgeoning(3), new Piercing(2),
    ]),
    new Forelegs([
      new Slashing(3), new Bludgeoning(2), new Piercing(2),
    ]),
    new Tail([
      new Severable,
    ]),
    // Weaknesses
    new Fire(1), new Water(1), new Thunder(3), new Ice(1), new Dragon(2),
    new Poison(2), new Sleep(2), new Paralysis(2), new Blast(2), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    // N/A
  ]);

  // 2% Carve, 3% Tail Carve and 3% Horn Break
  carves: Collection = new Collection([
    new ImmortalDragonscale(5),
    new NergiganteCarapace(4),
    new NergiganteTalon(3),
    new NergiganteHornPlus(2),
    new NergiganteRegrowthPlate(3),
    new NergiganteGem(1),
    new NergiganteTail(0),
  ]);

  // 6% Silver Reward and 13% Gold Reward
  rewards: Collection = new Collection([
    new NergiganteCarapace(4),
    new ImmortalDragonscale(4),
    new NergiganteRegrowthPlate(3),
    new NergiganteTalon(3),
    new ElderDragonBone(3),
    new ElderDragonBlood(2),
    new NergiganteGem(0),
  ]);

}

export class ImmortalDragonscale extends Carve {
  id = 'immortal-dragonscale';
  value = 'Immortal Dragonscale';
}

export class NergiganteCarapace extends Carve {
  id = 'nergigante-carapace';
  value = 'Nergigante Carapace';
}

export class NergiganteTalon extends Carve {
  id = 'nergigante-talon';
  value = 'Nergigante Talon';
}

export class NergiganteHornPlus extends Carve {
  id = 'nergigante-horn-plus';
  value = 'Nergigante Horn +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Horn;
}

export class NergiganteRegrowthPlate extends Drop {
  id = 'nergigante-regrowth-plate';
  value = 'Nergigante Regrowth Plate';
}

export class NergiganteGem extends Carve {
  id = 'nergigante-gem';
  value = 'Nergigante Gem';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyAndAnother([new Horn, new Tail]);
}

export class NergiganteTail extends Carve {
  id = 'nergigante-tail';
  value = 'Nergigante Tail';
  injury: Injury = new Severable;
  anatomy: Anatomy = new Tail;
}
