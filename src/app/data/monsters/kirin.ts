import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { ElderDragon} from '../types';
import { CloralHighlands } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Horn } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Thunderblight
} from '../damages';

import {
  Lightcrystal, Novacrystal, ElderDragonBone, ElderDragonBlood
} from '../materials';

export class Kirin extends Monster {
  id = 'kirin';
  value = 'Kirin';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new ElderDragon,
    new CloralHighlands,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(3), new Water(2), new Thunder(-1), new Ice(2), new Dragon(1),
    new Poison(1), new Sleep(2), new Paralysis(-1), new Blast(2), new Stun(1),
  ]);

  attacks: Collection = new Collection([
    new Thunder, new Thunderblight,
  ]);

  carves: Collection = new Collection([
    new KirinThunderhorn(3),
    new KirinHide(5),
    new KirinMane(4, 4),
    new KirinTail(4),
    new KirinAzureHorn(0, 3),
    new KirinHidePlus(0, 5),
    new KirinThundertail(0, 4),
  ]);

  rewards: Collection = new Collection([
    new KirinHide(4),
    new KirinThunderhorn(3),
    new KirinMane(5, 4),
    new KirinTail(4),
    new Lightcrystal(2),
    new KirinHidePlus(0, 4),
    new KirinThundertail(0, 3),
    new Novacrystal(0, 2),
    new ElderDragonBone(0, 3),
    new ElderDragonBlood(0, 3),
  ]);

}

export class KirinThunderhorn extends Carve {
  id = 'kirin-thunderhorn';
  value = 'Kirin Thunderhorn';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Horn;
}

export class KirinHide extends Carve {
  id = 'kirin-hide';
  value = 'Kirin Hide';
}

export class KirinMane extends Drop {
  id = 'kirin-mane';
  value = 'Kirin Mane';
}

export class KirinTail extends Carve {
  id = 'kirin-tail';
  value = 'Kirin Tail';
  anatomy: Anatomy = new Tail;
}

export class KirinAzureHorn extends Carve {
  id = 'kirin-azure-horn';
  value = 'Kirin Azure Horn';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Horn;
}

export class KirinHidePlus extends Drop {
  id = 'kirin-hide-plus';
  value = 'Kirin Hide +';
}

export class KirinThundertail extends Carve {
  id = 'kirin-thundertail';
  value = 'Kirin Thundertail';
  anatomy: Anatomy = new Tail;
}
