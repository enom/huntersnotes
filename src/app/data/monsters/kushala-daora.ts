import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import {ElderDragon} from '../types';
import { AncientForest, EldersRecess } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Horn, Wings } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun
} from '../damages';

import { ElderDragonBone, ElderDragonBlood } from '../materials';

export class KushalaDaora extends Monster {
  id = 'kushala-daora';
  value = 'Kushala Daora';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new ElderDragon,
    new AncientForest, new EldersRecess,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(2),
    ]),
    new Forelegs([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Tail([
      new Severable,
      new Slashing(2), new Bludgeoning(2), new Piercing(3),
    ]),
    // Weaknesses
    new Fire(1), new Water(-1), new Thunder(3), new Ice(-1), new Dragon(2),
    new Poison(3), new Sleep(1), new Paralysis(1), new Blast(3), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Stun,
  ]);

  // 2% Carve, 3% Tail Carve and 3% Head Break
  carves: Collection = new Collection([
    new DaoraDragonScalePlus(5),
    new DaoraCarapace(4),
    new DaoraClawPlus(3),
    new DaoraHornPlus(2),
    new DaoraWebbing(3),
    new DaoraGem(1),
    new DaoraTail(2),
  ]);

  // 6% Silver Reward and 13% Gold Reward
  rewards: Collection = new Collection([
    new DaoraCarapace(4),
    new DaoraDragonScalePlus(4),
    new DaoraClawPlus(3),
    new DaoraWebbing(3),
    new ElderDragonBone(2),
    new ElderDragonBlood(3),
    new DaoraGem(1),
  ]);
}

export class DaoraDragonScalePlus extends Carve {
  id = 'daora-dragon-scale-plus';
  value = 'Daora Dragon Scale +';
}

export class DaoraCarapace extends Drop {
  id = 'daora-carapace';
  value = 'Daora Carapace';
}

export class DaoraClawPlus extends Carve {
  id = 'daora-claw-plus';
  value = 'Daora Claw +';
}

export class DaoraHornPlus extends Carve {
  id = 'daora-horn-plus';
  value = 'Daora Horn +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Horn;
}

export class DaoraWebbing extends Carve {
  id = 'daora-webbing';
  value = 'Daora Webbing';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Wings;
}

export class DaoraGem extends Carve {
  id = 'daora-gem';
  value = 'Daora Gem';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class DaoraTail extends Carve {
  id = 'daora-tail';
  value = 'Daora Tail';
  anatomy: Anatomy = new Tail;
}
