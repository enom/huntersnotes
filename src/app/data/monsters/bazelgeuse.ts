import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';
import { Monster } from '../../monsters/monster';
import { Anatomy } from '../../monsters/anatomy';
import { Injury } from '../../monsters/damage';

import { Large } from '../sizes';
import { FlyingWyvern } from '../types';
import {
  AncientForest,
  CloralHighlands,
  RottenVale,
  WildspireWaste ,
  EldersRecess
} from '../expeditions';

import { Head, Chest, Forelegs, Legs, Back, Wings, Tail } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Fireblight,
} from '../damages';

import {
  FertileMud, WyvernGem, MonsterBoneM, MonsterKeenbone, MonsterHardbone
} from '../materials';

export class Bazelgeuse extends Monster {
  id = 'bazelgeuse';
  value = 'Bazelgeuse';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new FlyingWyvern,
    new AncientForest, new CloralHighlands, new RottenVale,
    new WildspireWaste, new EldersRecess,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Wings([
      new Breakable,
      new Slashing(1), new Bludgeoning(1), new Piercing(3),
    ]),
    new Back([
      new Breakable,
    ]),
    new Tail([
      new Severable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(-1), new Water(1), new Thunder(3), new Ice(2), new Dragon(2),
    new Poison(2), new Sleep(2), new Paralysis(2), new Blast(1), new Stun(1),
  ]);

  attacks: Collection = new Collection([
    new Fire, new Fireblight,
  ]);

  // 2% Carve, 3% Tail Carve and 1% Back Break
  carves: Collection = new Collection([
    new BazelgeuseScalePlus(0, 5),
    new BazelgeuseCarapace(0, 4),
    new BazelgeuseTalon(0, 3),
    new BazelgeuseFuse(0, 3),
    new BazelgeuseWing(0, 3),
    new BazelgeuseGem(0, 1),
    new BazelgeuseTail,
  ]);

  // 6% Silver Reward, 13% Gold Reward and 2% Capture
  rewards: Collection = new Collection([
    new BazelgeuseCarapace(4),
    new BazelgeuseScalePlus(4),
    new BazelgeuseFuse(3),
    new BazelgeuseTalon(3),
    new BazelgeuseWing(2),
    new BazelgeuseTail(2),
    new MonsterHardbone(3),
    new BazelgeuseGem,
  ]);
}

export class BazelgeuseScalePlus extends Carve {
  id = 'bazelgeuse-scale-plus';
  value = 'Bazelgeuse Scale +';
}

export class BazelgeuseCarapace extends Carve {
  id = 'bazelgeuse-carapace';
  value = 'Bazelgeuse Carapace';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Back;
}

export class BazelgeuseTalon extends Carve {
  id = 'bazelgeuse-talon';
  value = 'Bazelgeuse Talon';
}

export class BazelgeuseFuse extends Carve {
  id = 'bazelgeuse-fuse';
  value = 'Bazelgeuse Fuse';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class BazelgeuseWing extends Carve {
  id = 'bazelgeuse-wing';
  value = 'Bazelgeuse Wing';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Wings;
}

export class BazelgeuseTail extends Carve {
  id = 'bazelgeuse-tail';
  value = 'Bazelgeuse Tail';
}

export class BazelgeuseGem extends Carve {
  id = 'bazelgeuse-gem';
  value = 'Bazelgeuse Gem';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}
