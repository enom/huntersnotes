import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';
import { Monster } from '../../monsters/monster';
import { Anatomy } from '../../monsters/anatomy';
import { Injury } from '../../monsters/damage';

import { Large } from '../sizes';
import { BruteWyvern } from '../types';
import { WildspireWaste } from '../expeditions';
import { Head, Chest, Forelegs, Legs, Tail } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Waterblight,
} from '../damages';

import {
  FertileMud, WyvernGem, MonsterBoneM, MonsterKeenbone
} from '../materials';

export class Barroth extends Monster {
  id = 'barroth';
  value = 'Barroth';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new BruteWyvern,
    new WildspireWaste,
    // Physiology
    new Head([
      new Breakable,
    ]),
    new Forelegs([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Legs([
      new Breakable,
    ]),
    new Tail([
      new Severable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(3, -1), new Water(-1, 3), new Thunder(-1), new Ice(2), new Dragon(1),
    new Poison(3), new Sleep(2), new Paralysis(3), new Blast(3), new Stun(1),
  ]);

  attacks: Collection = new Collection([
    new Waterblight,
  ]);

  // 2% Carve
  carves: Collection = new Collection([
    new BarrothRidge(4),
    new BarrothClaw(3),
    new BarrothTail(5, 3),
    new BarrothShell(3),
    new BarrothScalp(3, 3),
    new FertileMud(3),
    new BarrothRidgePlus(0, 4),
    new BarrothClawPlus(0, 3),
    new BarrothCarapace(0, 5),
    new WyvernGem(0, 1),
  ]);

  // 2% Capture, 1% Expedition, 1% Quest Reward, 8% Silver Reward and 13% Gold Reward
  rewards: Collection = new Collection([
    new BarrothRidge(4),
    new BarrothShell(4),
    new BarrothClaw(3),
    new BarrothScalp(2, 3),
    new MonsterBoneM(3),
    new FertileMud(3),
    new BarrothRidgePlus(0, 4),
    new BarrothCarapace(0, 4),
    new BarrothClawPlus(0, 3),
    new MonsterKeenbone(0, 3),
    new WyvernGem(0, 1),
  ]);
}

export class BarrothRidge extends Carve {
  id = 'barroth-ridge';
  value = 'Barroth Ridge';
}

export class BarrothClaw extends Carve {
  id = 'barroth-claw';
  value = 'Barroth Claw';
  anatomy: Anatomy = new Forelegs;
  injury: Injury = new Breakable;
}

export class BarrothTail extends Carve {
  id = 'barroth-tail';
  value = 'Barroth Tail';
  anatomy: Anatomy = new Tail;
  injury: Injury = new Severable;
}

export class BarrothShell extends Carve {
  id = 'barroth-shell';
  value = 'Barroth Shell';
  anatomy: Anatomy = new Legs;
  injury: Injury = new Breakable;
}

export class BarrothScalp extends Carve {
  id = 'barroth-scalp';
  value = 'Barroth Scalp';
  anatomy: Anatomy = new Head;
  injury: Injury = new Breakable;
}

export class BarrothRidgePlus extends Carve {
  id = 'barroth-ridge-plus';
  value = 'Barroth Ridge +';
}

export class BarrothClawPlus extends Carve {
  id = 'barroth-claw-plus';
  value = 'Barroth Claw +';
  anatomy: Anatomy = new Forelegs;
  injury: Injury = new Breakable;
}

export class BarrothCarapace extends Drop {
  id = 'barroth-carapace';
  value = 'Barroth Carapace';
  anatomy: Anatomy = new Legs;
  injury: Injury = new Breakable;
}
