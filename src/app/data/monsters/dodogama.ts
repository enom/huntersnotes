import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { FangedWyvern } from '../types';
import { EldersRecess } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Legs, StomachSac  } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Blastblight,
} from '../damages';

import {
MonsterKeenbone, NourishingExtract } from '../materials';

export class Dodogama extends Monster {
  id = 'dodogama';
  value = 'Dodogama';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new FangedWyvern,
    new EldersRecess,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new StomachSac([
      new Breakable,
      new Bludgeoning, new Slashing, new Piercing,
    ]),
    new Tail([
      new Severable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(-1), new Water(2), new Thunder(3), new Ice(2), new Dragon(1),
    new Poison(3), new Sleep(2), new Paralysis(2), new Blast(1), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Fire, new Blastblight,
  ]);

  carves: Collection = new Collection([
    new DodogamaHidePlus(0, 4),
    new DodogamaJaw(0, 3),
    new DodogamaScalePlus(0, 5),
    new DodogamaTail(0, 3),
    new DodogamaTalon(0, 3),
  ]);

  rewards: Collection = new Collection([
    new DodogamaHidePlus(0, 4),
    new DodogamaScalePlus(0, 4),
    new DodogamaJaw(0, 2),
    new DodogamaTalon(0, 3),
    new MonsterKeenbone(0, 3),
    new NourishingExtract(0, 3),
  ]);

}


export class DodogamaHidePlus extends Carve {
  id = 'dodogama-hide-plus';
  value = 'Dodogama Hide +';
}

export class DodogamaJaw extends Carve {
  id = 'dodogama-jaw';
  value = 'Dodogama Jaw';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class DodogamaScalePlus extends Carve {
  id = 'dodogama-scale-plus';
  value = 'Dodogama Scale +';
}

export class DodogamaTail extends Carve {
  id = 'dodogama-tail';
  value = 'Dodogama Tail';
  anatomy: Anatomy = new Tail;
}

export class DodogamaTalon extends Drop {
  id = 'dodogama-talon';
  value = 'Dodogama Talon';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Forelegs;
}
