import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { BruteWyvern } from '../types';
import {
  AncientForest,
  CloralHighlands,
  EldersRecess,
  RottenVale,
  WildspireWaste
 } from '../expeditions';
import { Head, Chest, Legs, Tail } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Dragonblight,
} from '../damages';

import {
  MonsterHardbone
} from '../materials';

export class Deviljho extends Monster {
  id = 'deviljho';
  value = 'Deviljho';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new BruteWyvern,
    new AncientForest, new CloralHighlands, new RottenVale,
    new WildspireWaste, new EldersRecess,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Chest([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Tail([
      new Severable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    // Weaknesses
    new Fire(2), new Water(2), new Thunder(3), new Ice(1), new Dragon(3),
    new Poison(2), new Sleep(2), new Paralysis(2), new Blast(2), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Dragon, new Dragonblight,
  ]);

  carves: Collection = new Collection([
    new DeviljhoHide(5),
    new DeviljhoScale(4),
    new DeviljhoTalon(3),
    new DeviljhoTallfang(3),
    new DeviljhoScalp(2),
    new DeviljhoSaliva(3),
    new DeviljhoTail(2),
    new DeviljhoGem(1),
  ]);

  rewards: Collection = new Collection([
    new DeviljhoHide(4),
    new DeviljhoScale(4),
    new DeviljhoTalon(3),
    new DeviljhoTallfang(3),
    new DeviljhoSaliva(3),
    new DeviljhoTail(2),
    new MonsterHardbone(3),
  ]);
}


export class DeviljhoHide extends Carve {
  id = 'deviljho-hide';
  value = 'Deviljho Hide';
}

export class DeviljhoScale extends Carve {
  id = 'deviljho-scale';
  value = 'Deviljho Scale';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Chest;
}

export class DeviljhoTalon extends Carve {
  id = 'deviljho-talon';
  value = 'Deviljho Talon';
}

export class DeviljhoTallfang extends Carve {
  id = 'deviljho-tallfang';
  value = 'Deviljho Tallfang';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class DeviljhoScalp extends Carve {
  id = 'deviljho-scalp';
  value = 'Deviljho Scalp';
}

export class DeviljhoSaliva extends Drop {
  id = 'deviljho-saliva';
  value = 'Deviljho Saliva';
}

export class DeviljhoTail extends Carve {
  id = 'deviljho-tail';
  value = 'Deviljho Tail';
  injury: Injury = new Severable;
  anatomy: Anatomy = new Tail;
}

export class DeviljhoGem extends Carve {
  id = 'deviljho-gem';
  value = 'Deviljho Gem';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}
