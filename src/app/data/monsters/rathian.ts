import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy, AnatomyOrAnother, AnatomyAndAnother } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { FlyingWyvern } from '../types';
import { AncientForest, WildspireWaste } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Back, Wings } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Fireblight,
} from '../damages';

import {
  MonsterBoneL, FlameSac, MonsterKeenbone, InfernoSac, RathWingtalon
} from '../materials';

export class Rathian extends Monster {
  id = 'rathian';
  value = 'Rathian';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new FlyingWyvern,
    new AncientForest, new WildspireWaste,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Wings([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Back([
      new Breakable,
    ]),
    new Tail([
      new Severable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(-1), new Water(1), new Thunder(2), new Ice(1), new Dragon(3),
    new Poison(1), new Sleep(2), new Paralysis(2), new Blast(1), new Stun(3),
  ]);

  attacks: Collection = new Collection([
    new Fire, new Fireblight, new Poison,
  ]);

  // 1% Carve, 1% Tail Carve, 1% Back Break and 3% Head Break
  carves: Collection = new Collection([
    new RathianPlate(1, 1),
    new RathianScale(5),
    new RathianShell(4),
    new RathianSpike(2),
    new RathianWebbing(3, 4),
    new RathWingtalon(3), // Injury = new Break // Anatomy = new Wings
    new RathianCarapace(0, 4),
    new RathianScalePlus(0, 5),
    new RathianSpikePlus(0, 3),
    new RathianRuby(0, 1),
  ]);

  // 1% Capture, 1% Quest Reward, 8% Silver Reward and 13% Gold Reward
  rewards: Collection = new Collection([
    new RathianShell(4),
    new RathianScale(4),
    new RathWingtalon(3),
    new RathianWebbing(2, 3),
    new RathianPlate(1, 1),
    new MonsterBoneL(3),
    new FlameSac(3),
    new RathianCarapace(0, 4),
    new RathianScalePlus(0, 4),
    new RathianSpikePlus(0, 2),
    new MonsterKeenbone(0, 3),
    new InfernoSac(0, 3),
    new RathianRuby(0),
  ]);
}

export class RathianPlate extends Carve {
  id = 'rathian-plate';
  value = 'Rathian Plate';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyOrAnother([new Head, new Back, new Tail]);
}

export class RathianScale extends Carve {
  id = 'rathian-scale';
  value = 'Rathian Scale';
}

export class RathianShell extends Drop {
  id = 'rathian-shell';
  value = 'Rathian Shell';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class RathianSpike extends Carve {
  id = 'rathian-spike';
  value = 'Rathian Spike';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyAndAnother([new Back, new Tail]);
}


export class RathianCarapace extends Drop {
  id = 'rathian-carapace';
  value = 'Rathian Carapace';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class RathianScalePlus extends Carve {
  id = 'rathian-scale-plus';
  value = 'Rathian Scale +';
}

export class RathianWebbing extends Carve {
  id = 'rathian-webbing';
  value = 'Rathian Webbing';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Wings;
}

export class RathianSpikePlus extends Carve {
  id = 'rathian-spike-plus';
  value = 'Rathian Spike +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyAndAnother([new Back, new Tail]);
}

export class RathianRuby extends Carve {
  id = 'rathian-ruby';
  value = 'Rathian Ruby';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyAndAnother([new Head, new Back, new Tail]);
}
