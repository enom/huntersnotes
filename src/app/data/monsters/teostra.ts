import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy, AnatomyOrAnother, AnatomyAndAnother } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import {ElderDragon} from '../types';
import { WildspireWaste, EldersRecess } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Back, Wings, Horn } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Fireblight, Blastblight,
} from '../damages';

import {
  ElderDragonBone, ElderDragonBlood,
  FireDragonScalePlus, TeostraCarapace, TeostraClawPlus, TeostraHornPlus,
  TeostraMane, TeostraTail, TeostraWebbing, TeostraPowder
} from '../materials';

export class Teostra extends Monster {
  id = 'teostra';
  value = 'Teostra';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new ElderDragon,
    new WildspireWaste, new EldersRecess,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Wings([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Tail([
      new Severable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(-1), new Water(3), new Thunder(1), new Ice(3), new Dragon(1),
    new Poison(2), new Sleep(1), new Paralysis(1), new Blast(1), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Fire, new Fireblight, new Blastblight,
  ]);

  // 2% Carve, 3% Tail Carve and 2% Head Break
  carves: Collection = new Collection([
    new FireDragonScalePlus(0, 5),
    new TeostraCarapace(0, 4),
    new TeostraClawPlus(0, 3),
    new TeostraHornPlus(0, 2), // Injury = new Breakable // Anatomy = new Head
    new TeostraMane(0, 3),
    new TeostraGem(0, 1),
    new TeostraTail(0, 3), // Injury = new Breakable // Anatomy = new Tail
    new TeostraWebbing(0, 2), // Injury = new Breakable // Anatomy = new Wings
  ]);

  // 6% Silver Reward and 13% Gold Reward
  rewards: Collection = new Collection([
    new TeostraCarapace(0, 4),
    new FireDragonScalePlus(0, 4),
    new TeostraPowder(0, 3),
    new TeostraMane(0, 3),
    new TeostraClawPlus(0, 3),
    new ElderDragonBone(0, 2),
    new ElderDragonBlood(0, 2),
    new TeostraGem(0, 1),
  ]);
}

export class TeostraGem extends Carve {
  id = 'teostra-gem';
  value = 'Teostra Gem';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyAndAnother([new Head, new Tail]);
}
