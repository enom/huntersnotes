import { Monster } from '../../monsters/monster';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';
import { Anatomy } from '../../monsters/anatomy';
import { Injury } from '../../monsters/damage';

import { Large } from '../sizes';
import { FangedWyvern } from '../types';
import { AncientForest } from '../expeditions';
import { Head, Chest, Forelegs, Legs } from '../anatomies';

import {
  Breakable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun
} from '../damages';

import {
  IronOre, MonsterBoneS, MonsterBonePlus
} from '../materials';

export class GreatJagras extends Monster {
  id = 'great-jagras';
  value = 'Great Jagras';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new FangedWyvern, new AncientForest,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3)
    ]),
    new Chest([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3)
    ]),
    new Forelegs([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2)
    ]),
    // Weaknesses
    new Fire(3), new Water(-1), new Thunder(2), new Ice(2), new Dragon(1),
    new Poison(3), new Sleep(3), new Paralysis(3), new Blast(3), new Stun(3)
  ]);

  attacks: Collection = new Collection([
    // N/A
  ]);

  carves: Collection = new Collection([
    new GreatJagrasScale(5),
    new GreatJagrasHide(4),
    new GreatJagrasMane(3, 3),
    new GreatJagrasClaw(4),
    new GreatJagrasScalePlus(0, 5),
    new GreatJagrasHidePlus(0, 4),
    new GreatJagrasClawPlus(0, 4),
  ]);

  rewards: Collection = new Collection([
    new GreatJagrasHide(5),
    new GreatJagrasScale(4),
    new GreatJagrasMane(3, 3),
    new GreatJagrasClaw(2),
    new IronOre(3),
    new MonsterBoneS(3),
    new GreatJagrasHidePlus(0, 5),
    new GreatJagrasScalePlus(0, 4),
    new GreatJagrasClawPlus(0, 2),
    new MonsterBonePlus(0, 4),
  ]);
}

export class GreatJagrasScale extends Carve {
  id = 'great-jagras-scale';
  value = 'Great Jagras Scale';
  anatomy: Anatomy = new Legs;
}

export class GreatJagrasHide extends Carve {
  id = 'great-jagras-hide';
  value = 'Great Jagras Hide';
}

export class GreatJagrasMane extends Carve {
  id = 'great-jagras-mane';
  value = 'Great Jagras Mane';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class GreatJagrasClaw extends Carve {
  id = 'great-jagras-claw';
  value = 'Great Jagras Claw';
}

export class GreatJagrasScalePlus extends Carve {
  id = 'great-jagras-scale-plus';
  value = 'Great Jagras Scale +';
}

export class GreatJagrasHidePlus extends Carve {
  id = 'great-jagras-hide-plus';
  value = 'Great Jagras Hide +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Chest;
}

export class GreatJagrasClawPlus extends Drop {
  id = 'great-jagras-claw-plus';
  value = 'Great Jagras Claw +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Forelegs;
}
