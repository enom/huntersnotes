import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { BirdWyvern } from '../types';
import { AncientForest } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Back, Wings } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun
} from '../damages';

import {
  MonsterBoneM, PoisonSac, MonsterKeenbone, ToxinSac, BirdWyvernGem
} from '../materials';

export class PukeiPukei extends Monster {
  id = 'pukei-pukei';
  value = 'Pukei Pukei';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new BirdWyvern,
    new AncientForest,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Wings([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Back([
      new Breakable,
    ]),
    new Tail([
      new Severable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(2), new Water(-1), new Thunder(3), new Ice(2), new Dragon(1),
    new Poison(1), new Sleep(3), new Paralysis(3), new Blast(2), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Poison,
  ]);

  carves: Collection = new Collection([
    new PukeiPukeiShell(4),
    new PukeiPukeiQuill(3),
    new PukeiPukeiScale(5),
    new PukeiPukeiTail(3, 3),
    new PukeiPukeiSac(3),
    new PukeiPukeiCarapace(0, 4),
    new PukeiPukeiWing(0, 3),
    new PukeiPukeiScalePlus(0, 4),
    new PukeiPukeiSacPlus(0, 3),
    new BirdWyvernGem(0, 1),
  ]);

  rewards: Collection = new Collection([
    new PukeiPukeiShell(4),
    new PukeiPukeiScale(4),
    new PukeiPukeiQuill(3),
    new PukeiPukeiSac(2),
    new MonsterBoneM(3),
    new PoisonSac(3),
    new PukeiPukeiCarapace(0, 4),
    new PukeiPukeiScalePlus(0, 3),
    new PukeiPukeiWing(0, 3),
    new PukeiPukeiSacPlus(0, 2),
    new MonsterKeenbone(0, 3),
    new ToxinSac(0, 3),
    new BirdWyvernGem(0, 1),
  ]);
}

export class PukeiPukeiShell extends Carve {
  id = 'pukei-pukei-shell';
  value = 'Pukei-Pukei Shell';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Back;
}

export class PukeiPukeiQuill extends Drop {
  id = 'pukei-pukei-quill';
  value = 'Pukei-Pukei Quill';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Wings;
}

export class PukeiPukeiScale extends Carve {
  id = 'pukei-pukei-scale';
  value = 'Pukei-Pukei Scale';
}

export class PukeiPukeiTail extends Carve {
  id = 'pukei-pukei-tail';
  value = 'Pukei-Pukei Tail';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Tail;
}

export class PukeiPukeiSac extends Carve {
  id = 'pukei-pukei-sac';
  value = 'Pukei-Pukei Sac';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class PukeiPukeiCarapace extends Carve {
  id = 'pukei-pukei-carapace';
  value = 'Pukei-Pukei Carapace';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Back;
}

export class PukeiPukeiWing extends Carve {
  id = 'pukei-pukei-wing';
  value = 'Pukei-Pukei Wing';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Wings;
}

export class PukeiPukeiScalePlus extends Carve {
  id = 'pukei-pukei-scale-plus';
  value = 'Pukei-Pukei Scale +';
}

export class PukeiPukeiSacPlus extends Carve {
  id = 'pukei-pukei-sac-plus';
  value = 'Pukei-Pukei Sac +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}
