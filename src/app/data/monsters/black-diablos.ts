import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { FlyingWyvern } from '../types';
import { WildspireWaste } from '../expeditions';
import { Head, Horn, Tail, Back, Wings, Chest } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun
} from '../damages';

import {
  MonsterBonePlus, DashExtract, MonsterHardbone
} from '../materials';

export class BlackDiablos extends Monster {
  id = 'black-diablos';
  value = 'Black Diablos';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new FlyingWyvern,
    new WildspireWaste,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(2), new Bludgeoning(3), new Piercing(2),
    ]),
    new Chest([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(2),
    ]),
    new Wings([
      new Breakable,
      new Slashing(2), new Bludgeoning(1), new Piercing(3),
    ]),
    // Weaknesses
    new Fire(-1), new Water(2), new Thunder(1), new Ice(3), new Dragon(-1),
    new Poison(2), new Sleep(2), new Paralysis(3), new Blast(2), new Stun(1),
  ]);

  attacks: Collection = new Collection([
    // N/A
  ]);

  // 3% Carve and 3% Tail Carve
  carves: Collection = new Collection([
    new BlackDiablosRidge(0, 4),
    new DiablosFang(0, 5),
    new BlackDiablosCarapace(0, 3),
    new DiablosTailcase(0, 3),
    new BlosMedulla(0, 3),
    new WyvernGem(0, 1),
    new BlackSpiralHornPlus(0, 1),
  ]);

  // 3% Capture, 1% Quest Reward, 8% Silver Reward and 16% Gold Reward
  rewards: Collection = new Collection([
    new BlackDiablosRidge(0, 4),
    new BlackDiablosCarapace(0, 4),
    new DiablosFang(0, 3),
    new BlosMedulla(0, 2),
    new MonsterHardbone(0, 3),
    new DashExtract(0, 3),
    new WyvernGem(0, 1),
  ]);
}


export class BlackDiablosRidge extends Drop {
  id = 'black-diablos-ridge';
  value = 'Black Diablos Ridge +';
}

export class DiablosFang extends Carve {
  id = 'diablos-fang';
  value = 'Diablos Fang';
}

export class BlackDiablosCarapace extends Carve {
  id = 'black-diablos-carapace';
  value = 'Black Diablos Carapace';
}

export class DiablosTailcase extends Carve {
  id = 'diablos-tailcase';
  value = 'Diablos Tailcase';
  injury: Injury = new Severable;
  anatomy: Anatomy = new Tail;
}

export class BlosMedulla extends Carve {
  id = 'blos-medulla';
  value = 'Blos Medulla';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Back;
}

export class WyvernGem extends Carve {
  id = 'wyvern-gem';
  value = 'Wyvern Gem';
  injury: Injury = new Severable;
  anatomy: Anatomy = new Tail;
}

export class BlackSpiralHornPlus extends Carve {
  id = 'black-spiral-horn-plus';
  value = 'Black Spiral Horn +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Horn;
}
