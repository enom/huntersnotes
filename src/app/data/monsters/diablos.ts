import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { FlyingWyvern } from '../types';
import { WildspireWaste } from '../expeditions';
import { Head, Horn, Tail, Back, Wings, Chest } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun
} from '../damages';

import {
  MonsterBonePlus, DashExtract, MonsterHardbone
} from '../materials';

export class Diablos extends Monster {
  id = 'diablos';
  value = 'Diablos';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new FlyingWyvern,
    new WildspireWaste,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(2), new Bludgeoning(3), new Piercing(2),
    ]),
    new Wings([
      new Breakable,
      new Slashing(2), new Bludgeoning(1), new Piercing(3),
    ]),
    new Chest([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(-1), new Water(2), new Thunder(1), new Ice(3), new Dragon(2),
    new Poison(2), new Sleep(2), new Paralysis(3), new Blast(2), new Stun(1),
  ]);

  attacks: Collection = new Collection([
    new Stun,
  ]);

  // 3% Carve and 3% Tail Carve
  carves: Collection = new Collection([
    new DiablosRidge(4),
    new DiablosFang(3, 3),
    new DiablosShell(5),
    new DiablosTailcase(3, 3),
    new DiablosMarrow(3),
    new TwistedHorn(1),
    new DiablosRidgePlus(0, 4),
    new DiablosCarapace(0, 5),
    new BlosMedulla(0, 3),
    new MajesticHorn(0, 1),
    new WyvernGem(0, 1),
  ]);

  // 3% Capture, 1% Quest Reward, 8% Silver Reward and 16% Gold Reward
  rewards: Collection = new Collection([
    new DiablosRidge(4),
    new DiablosShell(4),
    new DiablosFang(3, 3),
    new DiablosMarrow(2),
    new MonsterBonePlus(3),
    new DashExtract(3, 3),
    new DiablosRidgePlus(0, 4),
    new DiablosCarapace(0, 4),
    new BlosMedulla(0, 2),
    new MonsterHardbone(0, 3),
    new WyvernGem(0, 1),
  ]);
}


export class DiablosRidge extends Drop {
  id = 'diablos-ridge';
  value = 'Diablos Ridge';
}

export class DiablosFang extends Carve {
  id = 'diablos-fang';
  value = 'Diablos Fang';
}

export class DiablosShell extends Carve {
  id = 'diablos-shell';
  value = 'Diablos Shell';
}

export class DiablosTailcase extends Carve {
  id = 'diablos-tailcase';
  value = 'Diablos Tailcase';
  anatomy: Anatomy = new Tail;
}

export class DiablosMarrow extends Carve {
  id = 'diablos-marrow';
  value = 'Diablos Marrow';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Back;
}

export class TwistedHorn extends Carve {
  id = 'twisted-horn';
  value = 'Twisted Horn';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Horn;
}

export class DiablosRidgePlus extends Drop {
  id = 'diablos-ridge-plus';
  value = 'Diablos Ridge +';
}

export class DiablosCarapace extends Carve {
  id = 'diablos-carapace';
  value = 'Diablos Carapace';
}

export class BlosMedulla extends Carve {
  id = 'blos-medulla';
  value = 'Blos Medulla';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Back;
}

export class MajesticHorn extends Carve {
  id = 'majestic-horn';
  value = 'Majestic Horn';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Horn;
}

export class WyvernGem extends Carve {
  id = 'wyvern-gem';
  value = 'Wyvern Gem';
  injury: Injury = new Severable;
  anatomy: Anatomy = new Tail;
}
