import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy, AnatomyAndAnother } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { FangedWyvern } from '../types';
import { CloralHighlands, RottenVale } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Legs } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Bleeding,
} from '../damages';

import {
  NourishingExtract, MonsterBonePlus, MonsterHardbone
} from '../materials';

export class Odogaron extends Monster {
  id = 'odogaron';
  value = 'Odogaron';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new FangedWyvern,
    new CloralHighlands, new RottenVale,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Forelegs([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Legs([
      new Breakable,
    ]),
    new Tail([
      new Severable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(1), new Water(1), new Thunder(2), new Ice(3), new Dragon(-1),
    new Poison(1), new Sleep(2), new Paralysis(3), new Blast(2), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Bleeding,
  ]);

  // 2% Carve, 3% Tail Carve and 1% Head Break
  carves: Collection = new Collection([
    new OdogaronSinew(4),
    new OdogaronScale(5),
    new OdogaronClaw(4),
    new OdogaronFang(3),
    new OdogaronTail(0, 2),
    new OdogaronSinewPlus(0, 4),
    new OdogaronScalePlus(0, 5),
    new OdogaronClawPlus(0, 4),
    new OdogaronFangPlus(0, 3),
    new OdogaronGem(0, 1),
    new OdogaronPlate(0, 1),
  ]);

  // 2% Capture, 6% Silver Reward and 13% Gold Reward
  rewards: Collection = new Collection([
    new OdogaronSinew(4),
    new OdogaronScale(4),
    new OdogaronClaw(3),
    new OdogaronTail(2, 2),
    new MonsterBonePlus(3),
    new NourishingExtract(3, 3),
    new OdogaronSinewPlus(0, 4),
    new OdogaronScalePlus(0, 4),
    new OdogaronClawPlus(0, 3),
    new OdogaronPlate(0, 1),
    new MonsterHardbone(0, 3),
    new OdogaronGem(0),
  ]);
}

export class OdogaronSinew extends Carve {
  id = 'odogaron-sinew';
  value = 'Odogaron Sinew';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Legs;
}

export class OdogaronScale extends Carve {
  id = 'odogaron-scale';
  value = 'Odogaron Scale';
}

export class OdogaronClaw extends Carve {
  id = 'odogaron-claw';
  value = 'Odogaron Claw';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Forelegs;
}

export class OdogaronFang extends Drop {
  id = 'odogaron-fang';
  value = 'Odogaron Fang';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class OdogaronTail extends Carve {
  id = 'odogaron-tail';
  value = 'Odogaron Tail';
  anatomy: Anatomy = new Tail;
}

export class OdogaronSinewPlus extends Carve {
  id = 'odogaron-sinew-plus';
  value = 'Odogaron Sinew +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Legs;
}

export class OdogaronScalePlus extends Carve {
  id = 'odogaron-scale-plus';
  value = 'Odogaron Scale +';
}

export class OdogaronClawPlus extends Carve {
  id = 'odogaron-claw-plus';
  value = 'Odogaron Claw +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Forelegs;
}

export class OdogaronFangPlus extends Drop {
  id = 'odogaron-fang-plus';
  value = 'Odogaron Fang +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class OdogaronGem extends Carve {
  id = 'odogaron-gem';
  value = 'Odogaron Gem';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyAndAnother([new Head, new Tail]);
}

export class OdogaronPlate extends Carve {
  id = 'odogaron-plate';
  value = 'Odogaron Plate';
  anatomy: Anatomy = new Tail;
}
