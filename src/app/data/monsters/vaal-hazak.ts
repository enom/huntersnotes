import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy, AnatomyOrAnother, AnatomyAndAnother } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import {ElderDragon} from '../types';
import { RottenVale } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Back, Wings } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Miasma,
} from '../damages';

import {
  ElderDragonBone, ElderDragonBlood
} from '../materials';

export class VaalHazak extends Monster {
  id = 'vaal-hazak';
  value = 'Vaal Hazak';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new ElderDragon,
    new RottenVale,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(2), new Bludgeoning(3), new Piercing(2),
    ]),
    new Chest([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(1),
    ]),
    new Forelegs([
      new Breakable,
    ]),
    new Tail([
      new Severable,
      new Slashing(3), new Bludgeoning(1), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(3), new Water(-1), new Thunder(1), new Ice(2), new Dragon(3),
    new Poison(1), new Sleep(1), new Paralysis(1), new Blast(2), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Miasma,
  ]);

  // 2% Carve, 3% Tail Carve and 2% Head Break
  carves: Collection = new Collection([
    new VaalHazakCarapace(4),
    new DeceasedScale(5),
    new VaalHazakTalon(3),
    new VaalHazakFangPlus(2),
    new VaalHazakWing(3),
    new VaalHazakGem(1),
    new VaalHazakMembrane(1),
    new VaalHazakTail(1),
  ]);

  // 6% Silver Reward and 13% Gold Reward
  rewards: Collection = new Collection([
    new VaalHazakCarapace(4),
    new DeceasedScale(3),
    new VaalHazakWing(3),
    new VaalHazakMembrane(3),
    new VaalHazakTalon(3),
    new ElderDragonBone(2),
    new ElderDragonBlood(3),
    new VaalHazakGem(1),
  ]);
}

export class VaalHazakCarapace extends Carve {
  id = 'vaal-hazak-carapace';
  value = 'Vaal Hazak Carapace';
}

export class DeceasedScale extends Carve {
  id = 'deceased-scale';
  value = 'Deceased Scale';
}

export class VaalHazakTalon extends Carve {
  id = 'vaal-hazak-talon';
  value = 'Vaal Hazak Talon';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Forelegs;
}

export class VaalHazakFangPlus extends Carve {
  id = 'vaal-hazak-fang-plus';
  value = 'Vaal Hazak Fang +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class VaalHazakWing extends Carve {
  id = 'vaal-hazak-wing';
  value = 'Vaal Hazak Wing';
  anatomy: Anatomy = new Wings;
}

export class VaalHazakGem extends Carve {
  id = 'vaal-hazak-gem';
  value = 'Vaal Hazak Gem';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyOrAnother([new Head, new Tail]);
}

export class VaalHazakMembrane extends Drop {
  id = 'vaal-hazak-membrane';
  value = 'Vaal Hazak Membrane';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Chest;
}

export class VaalHazakTail extends Carve {
  id = 'vaal-hazak-tail';
  value = 'Vaal Hazak Tail';
  injury: Injury = new Severable;
  anatomy: Anatomy = new Tail;
}
