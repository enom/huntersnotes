import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { PiscineWyvern } from '../types';
import { EldersRecess } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Horn, Wings, Back } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun
} from '../damages';

import {MonsterHardbone, InfernoSac, WyvernGem } from '../materials';

export class Lavasioth extends Monster {
  id = 'lavasioth';
  value = 'Lavasioth';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new PiscineWyvern,
    new EldersRecess,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Back([
      new Breakable,
    ]),
    new Chest([
      new Breakable,
      new Slashing(3), new Bludgeoning(2), new Piercing(3),
    ]),
    new Tail([
      new Breakable,
    ]),
    // Weaknesses
    new Fire(-1, 1), new Water(3, 2), new Thunder(2, -1), new Ice(2, -1), new Dragon(1),
    new Poison(3), new Sleep(1), new Paralysis(2), new Blast(1), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Fire,
  ]);

  // 2% Carve, 3% Tail Carve and 3% Head Break
  carves: Collection = new Collection([
    new LavasiothScalePlus(0, 5),
    new LavasiothCarapace(0, 4),
    new LavasiothFangPlus(0, 3),
    new LavasiothFinPlus(0, 4),
    new WyvernGem(0, 1),
  ]);

  // 6% Silver Reward and 13% Gold Reward
  rewards: Collection = new Collection([
    new LavasiothCarapace(0, 4),
    new LavasiothScalePlus(0, 4),
    new LavasiothFangPlus(0, 3),
    new LavasiothFinPlus(0, 2),
    new MonsterHardbone(0, 3),
    new InfernoSac(0, 3),
    new WyvernGem(0, 1),
  ]);
}

export class LavasiothScalePlus extends Carve {
  id = 'lavasioth-scale-plus';
  value = 'Lavasioth Scale +';
}

export class LavasiothCarapace extends Carve {
  id = 'lavasioth-carapace';
  value = 'Lavasioth Carapace';
}

export class LavasiothFangPlus extends Carve {
  id = 'lavasioth-fang-plus';
  value = 'Lavasioth Fang +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class LavasiothFinPlus extends Drop {
  id = 'lavasioth-fin-plus';
  value = 'Lavasioth Fin +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Chest;
}
