import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy, AnatomyOrAnother, AnatomyAndAnother } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { BirdWyvern } from '../types';
import { CloralHighlands } from '../expeditions';
import { Head, Chest, Forelegs, Tail } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun
} from '../damages';

import {
  MonsterBoneL, DashExtract, MonsterBonePlus, BirdWyvernGem
} from '../materials';

export class TzitziYaKu extends Monster {
  id = 'tzitzi-ya-ku';
  value = 'Tzitzi-Ya-Ku';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new BirdWyvern,
    new CloralHighlands,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    // Weaknesses
    new Fire(2), new Water(2), new Thunder(3), new Ice(3), new Dragon(2),
    new Poison(2), new Sleep(2), new Paralysis(2), new Blast(2), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Stun,
  ]);

  carves: Collection = new Collection([
    new TzitziYaKuScale(5),
    new TzitziYaKuHide(4),
    new TzitziYaKuClaw(3),
    new TzitziYaKuPhotophore(4),
    new TzitziYaKuScalePlus(0, 5),
    new TzitziYaKuHidePlus(0, 4),
    new TzitziYaKuClawPlus(0, 3),
    new TzitziYaKuPhotopherePlus(0, 4),
    new BirdWyvernGem(0, 1),
  ]);

  rewards: Collection = new Collection([
    new TzitziYaKuHide(4),
    new TzitziYaKuScale(4),
    new TzitziYaKuClaw(3),
    new TzitziYaKuPhotophore(2),
    new MonsterBoneL(3),
    new DashExtract(3, 4),
    new TzitziYaKuHidePlus(0, 4),
    new TzitziYaKuScalePlus(0, 4),
    new TzitziYaKuClawPlus(0, 3),
    new TzitziYaKuPhotopherePlus(0, 2),
    new MonsterBonePlus(0, 4),
    new BirdWyvernGem(0, 1),
  ]);
}

export class TzitziYaKuScale extends Carve {
  id = 'tzitzi-ya-ku-scale';
  value = 'Tzitzi-Ya-Ku Scale';
}

export class TzitziYaKuHide extends Carve {
  id = 'tzitzi-ya-ku-hide';
  value = 'Tzitzi-Ya-Ku Hide';
}

export class TzitziYaKuClaw extends Drop {
  id = 'tzitzi-ya-ku-claw';
  value = 'Tzitzi-Ya-Ku Claw';
}

export class TzitziYaKuPhotophore extends Carve {
  id = 'tzitzi-ya-ku-photophore';
  value = 'Tzitzi-Ya-Ku Photophore';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class TzitziYaKuScalePlus extends Carve {
  id = 'tzitzi-ya-ku-scale-plus';
  value = 'Tzitzi-Ya-Ku Scale +';
}

export class TzitziYaKuHidePlus extends Carve {
  id = 'tzitzi-ya-ku-hide-plus';
  value = 'Tzitzi-Ya-Ku Hide +';
}

export class TzitziYaKuClawPlus extends Drop {
  id = 'tzitzi-ya-ku-claw-plus';
  value = 'Tzitzi-Ya-Ku Claw +';
}

export class TzitziYaKuPhotopherePlus extends Carve {
  id = 'tzitzi-ya-ku-photophere-plus';
  value = 'Tzitzi-Ya-Ku Photophore +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}
