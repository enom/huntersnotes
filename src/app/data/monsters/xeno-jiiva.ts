import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy, AnatomyOrAnother, AnatomyAndAnother } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import {ElderDragon} from '../types';
import { EldersRecess } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Back, Wings } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Fireblight, Dragonblight,
} from '../damages';

import {
  ElderDragonBone, ElderDragonBlood
} from '../materials';

export class XenoJiiva extends Monster {
  id = 'xeno-jiiva';
  value = 'Xeno\'Jiiva';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new ElderDragon,
    new EldersRecess,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Wings([
      new Breakable,
    ]),
    new Forelegs([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(1),
    ]),
    new Tail([
      new Severable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    // Weaknesses
    new Fire(2), new Water(2), new Thunder(2), new Ice(2), new Dragon(2),
    new Poison(3), new Sleep(-1), new Paralysis(1), new Blast(2), new Stun(1),
  ]);

  attacks: Collection = new Collection([
    new Dragon, new Dragonblight, new Fireblight
  ]);

  // 5% Carve, 7% Tail Carve and 4% Head Break
  carves: Collection = new Collection([
    new XenoJiivaShell(5),
    new XenoJiivaSoulscale(4),
    new XenoJiivaClaw(3),
    new XenoJiivaHorn(2),
    new XenoJiivaWing(3),
    new XenoJiivaGem(1),
    new XenoJiivaVeil(3),
  ]);

  // 3% Quest Reward, 3% Silver Reward (Doesn’t exist atm it might be a placeholder)
  // and 3% Gold Reward (Doesn’t exist atm it might be a placeholder)
  rewards: Collection = new Collection([
    new XenoJiivaShell(4),
    new XenoJiivaSoulscale(3),
    new XenoJiivaVeil(3),
    new XenoJiivaWing(3),
    new ElderDragonBone(3),
    new ElderDragonBlood(3),
    new XenoJiivaGem(1),
  ]);
}

export class XenoJiivaWing extends Carve {
  id = 'xeno-jiiva-wing';
  value = 'Xeno\'Jiiva Wing';
}

export class XenoJiivaShell extends Carve {
  id = 'xeno-jiiva-shell';
  value = 'Xeno\'Jiiva Shell';
}

export class XenoJiivaSoulscale extends Carve {
  id = 'xeno-jiiva-soulscale';
  value = 'Xeno\'Jiiva Soulscale';
}

export class XenoJiivaClaw extends Carve {
  id = 'xeno-jiiva-claw';
  value = 'Xeno\'Jiiva Claw';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Forelegs;
}

export class XenoJiivaHorn extends Carve {
  id = 'xeno-jiiva-horn';
  value = 'Xeno\'Jiiva Horn';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class XenoJiivaGem extends Carve {
  id = 'xeno-jiiva-gem';
  value = 'Xeno\'Jiiva Gem';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyAndAnother([new Head, new Tail]);
}

export class XenoJiivaVeil extends Carve {
  id = 'xeno-jiiva-veil';
  value = 'Xeno\'Jiiva Veil';
}
