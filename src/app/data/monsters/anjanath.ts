import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { BruteWyvern } from '../types';
import { AncientForest, WildspireWaste } from '../expeditions';
import { Head, Chest, Legs, Tail } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Fireblight,
} from '../damages';

import {
  MonsterBoneL, FlameSac,
  MonsterKeenbone, InfernoSac
} from '../materials';

export class Anjanath extends Monster {
  id = 'anjanath';
  value = 'Anjanath';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new BruteWyvern,
    new AncientForest, new WildspireWaste,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Tail([
      new Severable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Legs([
      new Breakable,
    ]),
    // Weaknesses
    new Fire(-1), new Water(3), new Thunder(2), new Ice(2), new Dragon(1),
    new Poison(2), new Sleep(2), new Paralysis(2), new Blast(1), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Fire, new Poison, new Fireblight,
  ]);

  // 2% Carve, 3% Tail Carve, 3% Head Break
  carves: Collection = new Collection([
    new AnjanathPelt(4),
    new AnjanathScale(5),
    new AnjanathFang(3),
    new AnjanathNosebone(3),
    new AnjanathPlate(1, 1),
    new AnjanathTail(2, 2),
    new AnjanathPeltPlus(0, 4),
    new AnjanathScalePlus(0, 5),
    new AnjanathFangPlus(0, 3),
    new AnjanathNosebonePlus(0, 4),
    new AnjanathGem(0, 1),
  ]);

  // 2% Capture, 6% Silver Reward, 13% Gold Reward
  rewards: Collection = new Collection([
    new AnjanathPelt(4),
    new AnjanathScale(4),
    new AnjanathNosebone(3),
    new AnjanathTail(2),
    new AnjanathPlate(1),
    new MonsterBoneL(3),
    new FlameSac(3),
    new AnjanathPeltPlus(0, 4),
    new AnjanathScalePlus(0, 4),
    new AnjanathNosebonePlus(0, 3),
    new MonsterKeenbone(0, 3),
    new InfernoSac(0, 3),
    new AnjanathGem(0, 1),
  ]);
}


export class AnjanathPelt extends Carve {
  id = 'anjanath-pelt';
  value = 'Anjanath Pelt';
}

export class AnjanathScale extends Carve {
  id = 'anjanath-scale';
  value = 'Anjanath Scale';
}

export class AnjanathFang extends Drop {
  id = 'anjanath-fang';
  value = 'Anjanath Fang';
  anatomy: Anatomy = new Head;
  injury: Injury = new Breakable;
}

export class AnjanathNosebone extends Carve {
  id = 'anjanath-nosebone';
  value = 'Anjanath Nosebone';
}

export class AnjanathPlate extends Carve {
  id = 'anjanath-plate';
  value = 'Anjanath Plate';
  anatomy: Anatomy = new Head;
  injury: Injury = new Breakable;
}

export class AnjanathTail extends Carve {
  id = 'anjanath-tail';
  value = 'Anjanath Tail';
  anatomy: Anatomy = new Tail;
  injury: Injury = new Severable;
}

export class AnjanathPeltPlus extends Carve {
  id = 'anjanath-pelt-plus';
  value = 'Anjanath Pelt +';
}

export class AnjanathScalePlus extends Carve {
  id = 'anjanath-scale-plus';
  value = 'Anjanath Scale +';
  anatomy: Anatomy = new Legs;
  injury: Injury = new Breakable;
}

export class AnjanathFangPlus extends Drop {
  id = 'anjanath-fang-plus';
  value = 'Anjanath Fang +';
  anatomy: Anatomy = new Head;
  injury: Injury = new Breakable;
}

export class AnjanathNosebonePlus extends Carve {
  id = 'anjanath-nosebone-plus';
  value = 'Anjanath Nosebone +';
}

export class AnjanathGem extends Carve {
  id = 'anjanath-gem';
  value = 'Anjanath Gem';
}
