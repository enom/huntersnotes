import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy, AnatomyOrAnother, AnatomyAndAnother } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { FlyingWyvern } from '../types';
import { AncientForest, EldersRecess } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Back, Wings } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Fireblight
} from '../damages';

import {
  FlameSac, MonsterBonePlus, MonsterHardbone, InfernoSac, RathWingtalon
} from '../materials';

export class AzureRathalos extends Monster {
  id = 'azure-rathalos';
  value = 'Azure Rathalos';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new FlyingWyvern,
    new AncientForest, new EldersRecess,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Wings([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Back([
      new Breakable,
    ]),
    new Tail([
      new Severable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(-1), new Water(1), new Thunder(1), new Ice(2), new Dragon(3),
    new Poison(1), new Sleep(2), new Paralysis(2), new Blast(1), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Fire, new Fireblight, new Poison, new Stun,
  ]);

  // 1% Carve, 2% Tail Carve, 3% Head Break and 1% Back Break
  carves: Collection = new Collection([
    new RathalosScalePlus(5),
    new AzureRathalosCarapace(4),
    new AzureRathalosWing(4),
    new RathalosMedulla(3),
    new RathalosPlate(1),
    new RathalosRuby(1),
    new AzureRathalosTail(2),
  ]);

  // 1% Capture, 6% Silver Reward and 13% Gold Reward
  rewards: Collection = new Collection([
    new AzureRathalosCarapace(4),
    new AzureRathalosScalePlus(4),
    new AzureRathalosWing(3),
    new AzureRathalosTail(2),
    new RathalosPlate(1),
    new MonsterHardbone(3),
    new InfernoSac(3),
    new RathalosMedulla(3),
    new RathalosRuby(1),
  ]);
}

export class RathalosScalePlus extends Carve {
  id = 'rathalos-scale-plus';
  value = 'Rathalos Scale +';
}

export class AzureRathalosCarapace extends Carve {
  id = 'azure-rathalos-carapace';
  value = 'Azure Rathalos Carapace';
}

export class AzureRathalosWing extends Carve {
  id = 'azure-rathalos-wing';
  value = 'Azure Rathalos Wing';
  anatomy: Anatomy = new Wings;
}

export class RathalosMedulla extends Carve {
  id = 'rathalos-medulla';
  value = 'Rathalos Medulla';
}

export class RathalosPlate extends Carve {
  id = 'rathalos-plate';
  value = 'Rathalos Plate';
}

export class RathalosRuby extends Carve {
  id = 'rathalos-ruby';
  value = 'Rathalos Ruby';
}

export class AzureRathalosTail extends Carve {
  id = 'azure-rathalos-tail';
  value = 'Azure Rathalos Tail';
  anatomy: Anatomy = new Tail;
}

export class AzureRathalosScalePlus extends Carve {
  id = 'azure-rathalos-scale-plus';
  value = 'Azure Rathalos Scale +';
}
