import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { PiscineWyvern } from '../types';
import { WildspireWaste } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Legs, Back } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Waterblight,
} from '../damages';

import {
  MonsterBoneM, AquaSac, MonsterKeenbone, TorrentSac, WyvernGem
} from '../materials';

export class Jyuratodus extends Monster {
  id = 'jyuratodus';
  value = 'Jyuratodus';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new PiscineWyvern,
    new WildspireWaste,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Back([
      new Breakable,
    ]),
    new Legs([
      new Breakable,
    ]),
    new Tail([
      new Severable,
      new Slashing(3), new Bludgeoning(3), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(1, -1), new Water(-1, 3), new Thunder(3, -1), new Ice(1), new Dragon(1),
    new Poison(2), new Sleep(1), new Paralysis(2), new Blast(1), new Stun(3),
  ]);

  attacks: Collection = new Collection([
    new Water, new Waterblight,
  ]);

  carves: Collection = new Collection([
    new JyuratodusScale(5),
    new JyuratodusShell(4),
    new JyuratodusFang(3),
    new JyuratodusFin(4),
    new JyuratodusScalePlus(0, 5),
    new JyuratodusCarapace(0, 4),
    new JyuratodusFangPlus(0, 3),
    new JyuratodusFinPlus(0, 4),
    new WyvernGem(0, 1),
  ]);

  rewards: Collection = new Collection([
    new JyuratodusShell(4),
    new JyuratodusScale(4),
    new JyuratodusFang(3),
    new JyuratodusFin(2),
    new MonsterBoneM(3),
    new AquaSac(3),
    new JyuratodusCarapace(0, 4),
    new JyuratodusScalePlus(0, 4),
    new JyuratodusFangPlus(0, 3),
    new JyuratodusFinPlus(0, 2),
    new MonsterKeenbone(0, 3),
    new TorrentSac(0, 3),
    new WyvernGem(0, 1),
  ]);

}

export class JyuratodusScale extends Carve {
  id = 'jyuratodus-scale';
  value = 'Jyuratodus Scale';
}

export class JyuratodusShell extends Carve {
  id = 'jyuratodus-shell';
  value = 'Jyuratodus Shell';
}

export class JyuratodusFang extends Carve {
  id = 'jyuratodus-fang';
  value = 'Jyuratodus Fang';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class JyuratodusFin extends Drop {
  id = 'jyuratodus-fin';
  value = 'Jyuratodus Fin';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Chest;
}

export class JyuratodusScalePlus extends Carve {
  id = 'jyuratodus-scale-plus';
  value = 'Jyuratodus Scale +';
}

export class JyuratodusCarapace extends Carve {
  id = 'jyuratodus-carapace';
  value = 'Jyuratodus Carapace';
}

export class JyuratodusFangPlus extends Carve {
  id = 'jyuratodus-fang-plus';
  value = 'Jyuratodus Fang +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class JyuratodusFinPlus extends Carve {
  id = 'jyuratodus-fin-plus';
  value = 'Jyuratodus Fin +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Chest;
}
