import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';
import { Monster } from '../../monsters/monster';
import { Anatomy } from '../../monsters/anatomy';
import { Injury } from '../../monsters/damage';

import { Large } from '../sizes';
import { ElderDragon} from '../types';
import { EldersRecess } from '../expeditions';
import { Head, Chest, Forelegs, Legs, Back, Wings, Horn, Tail } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Bleeding, Fireblight, Thunderblight,
} from '../damages';

import {
  AetheryteShard
} from '../materials';

export class Behemoth extends Monster {
  id = 'behemoth';
  value = 'Behemoth';

  notes: Collection = new Collection([
    new Large,
    new ElderDragon,
    new EldersRecess,
    // Physiology
    new Horn([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Forelegs([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Tail([
      new Severable,
      new Slashing(3), new Bludgeoning(2), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(1), new Water(2), new Thunder(1), new Ice(2), new Dragon(3),
    new Poison(2), new Sleep(2), new Paralysis(2), new Blast(2), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Fire, new Thunder, new Bleeding, new Fireblight, new Thunderblight
  ]);

  carves: Collection = new Collection([
    new BehemothMane(0, 5),
    new BehemothBone(0, 4),
    new BehemothTail(0, 3),
    new BehemothShearclaw(0, 3),
    new BehemothGreatHorn(0, 2),
  ]);

  rewards: Collection = new Collection([
    new BehemothBone(0, 5),
    new BehemothMane(0, 4),
    new BehemothShearclaw(0, 3),
    new BehemothTail(0, 2),
    new BehemothGreatHorn(0, 1),
    new AetheryteShard(0, 4),
  ]);
}

export class BehemothMane extends Carve {
  id = 'behemoth-mane';
  value = 'Behemoth Mane';
}

export class BehemothBone extends Carve {
  id = 'behemoth-bone';
  value = 'Behemoth Bone High Rank';
}

export class BehemothTail extends Carve {
  id = 'behemoth-tail';
  value = 'Behemoth Tail';
  injury: Injury = new Severable;
  anatomy: Anatomy = new Tail;
}

export class BehemothShearclaw extends Carve {
  id = 'behemoth-shearclaw';
  value = 'Behemoth Shearclaw';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Forelegs;
}

export class BehemothGreatHorn extends Carve {
  id = 'behemoth-great-horn';
  value = 'Behemoth Great Horn';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Horn;
}
