import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { BirdWyvern } from '../types';
import { AncientForest, WildspireWaste } from '../expeditions';
import { Head, Chest, Forelegs, Tail } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun
} from '../damages';

import { MonsterBoneM, MonsterBonePlus, BirdWyvernGem } from '../materials';

export class KuluYaKu extends Monster {
  id = 'kulu-ya-ku';
  value = 'Kulu-Ya-Ku';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new BirdWyvern,
    new AncientForest, new WildspireWaste,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Forelegs([
      new Breakable,
    ]),
    // Weaknesses
    new Fire(2), new Water(3), new Thunder(2), new Ice(2), new Dragon(2),
    new Poison(2), new Sleep(2), new Paralysis(2), new Blast(2), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    // N/A
  ]);

  carves: Collection = new Collection([
    new KuluYaKuScale(5),
    new KuluYaKuHide(4),
    new KuluYaKuPlume(3),
    new KuluYaKuBeak(4),
    new KuluYaKuScalePlus(0, 5),
    new KuluYaKuHidePlus(0, 4),
    new KuluYaKuPlumePlus(0, 3),
    new KuluYaKuBeakPlus(0, 4),
    new BirdWyvernGem(0, 1),
  ]);

  rewards: Collection = new Collection([
    new KuluYaKuHide(5),
    new KuluYaKuScale(4),
    new KuluYaKuPlume(3),
    new KuluYaKuBeak(2),
    new MonsterBoneM(4),
    new KuluYaKuHidePlus(0, 5),
    new KuluYaKuScalePlus(0, 4),
    new KuluYaKuPlumePlus(0, 3),
    new KuluYaKuBeakPlus(0, 3),
    new MonsterBonePlus(0, 3),
    new BirdWyvernGem(0, 1),
  ]);
}

export class KuluYaKuScale extends Carve {
  id = 'kulu-ya-ku-scale';
  value = 'Kulu-Ya-Ku Scale';
}

export class KuluYaKuHide extends Carve {
  id = 'kulu-ya-ku-hide';
  value = 'Kulu-Ya-Ku Hide';
}

export class KuluYaKuPlume extends Drop {
  id = 'kulu-ya-ku-plume';
  value = 'Kulu-Ya-Ku Plume';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Forelegs;
}

export class KuluYaKuBeak extends Carve {
  id = 'kulu-ya-ku-beak';
  value = 'Kulu-Ya-Ku Beak';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class KuluYaKuScalePlus extends Carve {
  id = 'kulu-ya-ku-scale-plus';
  value = 'Kulu-Ya-Ku Scale +';
}

export class KuluYaKuHidePlus extends Carve {
  id = 'kulu-ya-ku-hide-plus';
  value = 'Kulu-Ya-Ku Hide +';
}

export class KuluYaKuPlumePlus extends Drop {
  id = 'kulu-ya-ku-plume-plus';
  value = 'Kulu-Ya-Ku Plume +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Forelegs;
}

export class KuluYaKuBeakPlus extends Carve {
  id = 'kulu-ya-ku-beak-plus';
  value = 'Kulu-Ya-Ku Beak +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}
