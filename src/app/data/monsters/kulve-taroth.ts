import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import {ElderDragon} from '../types';
import { CavernsOfElDorado } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Horn } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Fireblight,
} from '../damages';

import { ElderDragonBone, ElderDragonBlood } from '../materials';

export class KulveTaroth extends Monster {
  id = 'kulve-taroth';
  value = 'Kulve Taroth';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new ElderDragon,
    new CavernsOfElDorado,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(2),
    ]),
    new Horn([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Forelegs([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(3),
    ]),
    // Weaknesses
    new Fire(-1, 1), new Water(2), new Thunder(-1, 3), new Ice(3, -1), new Dragon(2, 1),
    new Poison(1), new Sleep(1), new Paralysis(1), new Blast(1), new Stun(1),
  ]);

  attacks: Collection = new Collection([
    new Fire, new Fireblight,
  ]);

  carves: Collection = new Collection([
    new KulveTarothGoldenSpiralhorn(4),
    new KulveTarothGoldenShell(5),
    new MeldedWeapon(5),
    new DissolvedWeapon(2),
    new SublimatedWeapon(2),
    new KulveTarothGoldenScale(4),
    new KulveTarothGoldenTailshell(4),
    new KulveTarothGoldenNugget(5),
    new KulveTarothGoldenGlimstone(1),
  ]);

  rewards: Collection = new Collection([
    new KulveTarothGoldenScale(4),
    new KulveTarothGoldenShell(5),
    new KulveTarothGoldenSpiralhorn(3),
    new KulveTarothGoldenTailshell(3),
    new KulveTarothGoldenGlimstone(2),
    new ElderDragonBone(2),
    new ElderDragonBlood(2),
  ]);
}

export class KulveTarothGoldenSpiralhorn extends Carve {
  id = 'kulve-taroth-golden-spiralhorn';
  value = 'Kulve Taroth Golden Spiralhorn';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Horn;
}

export class KulveTarothGoldenShell extends Carve {
  id = 'kulve-taroth-golden-shell';
  value = 'Kulve Taroth Golden Shell';
}

export class MeldedWeapon extends Carve {
  id = 'melded-weapon';
  value = 'Melded Weapon';
}

export class DissolvedWeapon extends Carve {
  id = 'dissolved-weapon';
  value = 'Dissolved Weapon';
}

export class SublimatedWeapon extends Carve {
  id = 'sublimated-weapon';
  value = 'Sublimated Weapon';
}

export class KulveTarothGoldenScale extends Drop {
  id = 'kulve-taroth-golden-scale';
  value = 'Kulve Taroth Golden Scale';
}

export class KulveTarothGoldenTailshell extends Carve {
  id = 'kulve-taroth-golden-tailshell';
  value = 'Kulve Taroth Golden Tailshell';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Tail;
}

export class KulveTarothGoldenNugget extends Carve {
  id = 'kulve-taroth-golden-nugget';
  value = 'Kulve Taroth Golden Nugget';
}

export class KulveTarothGoldenGlimstone extends Carve {
  id = 'kulve-taroth-golden-glimstone';
  value = 'Kulve Taroth Golden Glimstone';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Horn;
}
