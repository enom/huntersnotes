import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { FangedWyvern } from '../types';
import { RottenVale } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Legs } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun
} from '../damages';

import {
  MonsterBoneL, ParalysisSac, MonsterBonePlus, OmniplegiaSac
} from '../materials';

export class GreatGirros extends Monster {
  id = 'great-girros';
  value = 'Great Girros';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new FangedWyvern,
    new RottenVale,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Forelegs([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Tail([
      new Severable,
      new Slashing(2), new Bludgeoning(2), new Piercing(1),
    ]),
    // Weaknesses
    new Fire(2), new Water(3), new Thunder(-1), new Ice(2), new Dragon(1),
    new Poison(2), new Sleep(3), new Paralysis(1), new Blast(2), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Paralysis,
  ]);

  carves: Collection = new Collection([
    new GreatGirrosHide(4),
    new GreatGirrosHood(3),
    new GreatGirrosScale(5),
    new GreatGirrosTail(3, 3),
    new GreatGirrosFang(3),
    new GreatGirrosHoodPlus(0, 3),
    new GreatGirrosScalePlus(0, 5),
    new GreatGirrosFangPlus(0, 3),
  ]);

  rewards: Collection = new Collection([
    new GreatGirrosHide(4),
    new GreatGirrosScale(4),
    new GreatGirrosHood(2),
    new GreatGirrosFang(3),
    new MonsterBoneL(3),
    new ParalysisSac(3),
    new GreatGirrosScalePlus(0, 4),
    new GreatGirrosHoodPlus(0, 2),
    new GreatGirrosFangPlus(0, 3),
    new MonsterBonePlus(0, 3),
    new OmniplegiaSac(0, 3),
  ]);

}

export class GreatGirrosHide extends Carve {
  id = 'great-girros-hide';
  value = 'Great Girros Hide';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Forelegs;
}

export class GreatGirrosHood extends Carve {
  id = 'great-girros-hood';
  value = 'Great Girros Hood';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class GreatGirrosScale extends Carve {
  id = 'great-girros-scale';
  value = 'Great Girros Scale';
  anatomy: Anatomy = new Legs;
}

export class GreatGirrosFang extends Drop {
  id = 'great-girros-fang';
  value = 'Great Girros Fang';
}

export class GreatGirrosHoodPlus extends Carve {
  id = 'great-girros-hood-plus';
  value = 'Great Girros Hood +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class GreatGirrosScalePlus extends Carve {
  id = 'great-girros-scale-plus';
  value = 'Great Girros Scale +';
}

export class GreatGirrosTail extends Carve {
  id = 'great-girros-tail';
  value = 'Great Girros Tail';
  anatomy: Anatomy = new Tail;
}

export class GreatGirrosFangPlus extends Drop {
  id = 'great-girros-fang-plus';
  value = 'Great Girros Fang +';
}
