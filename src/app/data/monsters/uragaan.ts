import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy, AnatomyOrAnother, AnatomyAndAnother } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { BruteWyvern } from '../types';
import { EldersRecess } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Back } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Fireblight,
} from '../damages';

import {
  MonsterHardbone, InfernoSac, FirecellStone
} from '../materials';

export class Uragaan extends Monster {
  id = 'uragaan';
  value = 'Uragaan';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new BruteWyvern,
    new EldersRecess,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Chest([
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Back([
      new Breakable,
    ]),
    new Tail([
      new Severable,
    ]),
    // Weaknesses
    new Fire(-1), new Water(3), new Thunder(1), new Ice(2), new Dragon(2),
    new Poison(3), new Sleep(1), new Paralysis(2), new Blast(2), new Stun(3),
  ]);

  attacks: Collection = new Collection([
    new Fire, new Fireblight, new Sleep
  ]);

  carves: Collection = new Collection([
    new UragaanCarapace(0, 4),
    new UragaanScale(0, 5),
    new UragaanScute(0, 4),
    new UragaanMarrow(0, 3),
    new UragaanRuby(0, 1),
    new UragaanJaw(0, 2),
  ]);

  rewards: Collection = new Collection([
    new UragaanCarapace(0, 4),
    new UragaanScale(0, 4),
    new UragaanScute(0, 3),
    new UragaanMarrow(0, 2),
    new MonsterHardbone(0, 3),
    new InfernoSac(0, 3),
    new FirecellStone(0, 2),
  ]);
}

export class UragaanCarapace extends Carve {
  id = 'uragaan-carapace';
  value = 'Uragaan Carapace';
}

export class UragaanScale extends Drop {
  id = 'uragaan-scale';
  value = 'Uragaan Scale +';
}

export class UragaanScute extends Carve {
  id = 'uragaan-scute';
  value = 'Uragaan Scute';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyAndAnother([new Back, new Tail]);
}

export class UragaanMarrow extends Carve {
  id = 'uragaan-marrow';
  value = 'Uragaan Marrow';
}

export class UragaanRuby extends Carve {
  id = 'uragaan-ruby';
  value = 'Uragaan Ruby';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyAndAnother([new Back, new Tail]);
}

export class UragaanJaw extends Carve {
  id = 'uragaan-jaw';
  value = 'Uragaan Jaw';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}
