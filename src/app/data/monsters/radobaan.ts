import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy, AnatomyOrAnother, AnatomyAndAnother } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { BruteWyvern } from '../types';
import { RottenVale } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Back, Legs } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun
} from '../damages';

import {
  MonsterBoneL, SleepSac, MonsterKeenbone, ComaSac
} from '../materials';

export class Radobaan extends Monster {
  id = 'radobaan';
  value = 'Radobaan';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new BruteWyvern,
    new RottenVale,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Back([
      new Breakable,
    ]),
    new Legs([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Tail([
      new Severable,
    ]),
    // Weaknesses
    new Fire(1), new Water(1), new Thunder(1), new Ice(2), new Dragon(3),
    new Poison(2), new Sleep(1), new Paralysis(2), new Blast(3), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Sleep,
  ]);

  carves: Collection = new Collection([
    new RadobaanShell(4),
    new RadobaanScale(5),
    new RadobaanOilshell(4),
    new RadobaanMarrow(3),
    new WyvernGem(1),
    new RadobaanCarapace(0, 4),
    new RadobaanScalePlus(0, 5),
    new RadobaanMedulla(0, 3),
  ]);

  rewards: Collection = new Collection([
    new RadobaanShell(4),
    new RadobaanScale(4),
    new RadobaanOilshell(3),
    new RadobaanMarrow(3),
    new MonsterBoneL(3),
    new SleepSac(3),
    new RadobaanCarapace(0, 4),
    new RadobaanScalePlus(0, 4),
    new RadobaanMedulla(0, 3),
    new MonsterKeenbone(0, 3),
    new ComaSac(0, 3),
    new WyvernGem(0, 1),
  ]);
}

/* -------------------------------------------------------------------------- */
// Radobaan requires several broken parts before the gem will drop as a carve

export class WyvernGem extends Drop {
  id = 'wyvern-gem';
  value = 'Wyvern Gem';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyAndAnother([new Back, new Legs]);
}

/* -------------------------------------------------------------------------- */

export class RadobaanShell extends Carve {
  id = 'radobaan-shell';
  value = 'Radobaan Shell';
}

export class RadobaanScale extends Carve {
  id = 'radobaan-scale';
  value = 'Radobaan Scale';
}

export class RadobaanOilshell extends Carve {
  id = 'radobaan-oilshell';
  value = 'Radobaan Oilshell';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyOrAnother([new Head, new Tail]);
}

export class RadobaanMarrow extends Carve {
  id = 'radobaan-marrow';
  value = 'Radobaan Marrow';
  anatomy: Anatomy = new Tail;
}

export class RadobaanCarapace extends Carve {
  id = 'radobaan-carapace';
  value = 'Radobaan Carapace';
}

export class RadobaanScalePlus extends Drop {
  id = 'radobaan-scale-plus';
  value = 'Radobaan Scale +';
}

export class RadobaanMedulla extends Carve {
  id = 'radobaan-medulla';
  value = 'Radobaan Medulla';
}
