import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy, AnatomyOrAnother, AnatomyAndAnother } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { FlyingWyvern } from '../types';
import { AncientForest } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Back, Wings } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Fireblight
} from '../damages';

import {
  FlameSac, MonsterBonePlus, MonsterHardbone, InfernoSac, RathWingtalon
} from '../materials';

export class Rathalos extends Monster {
  id = 'rathalos';
  value = 'Rathalos';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new FlyingWyvern,
    new AncientForest,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Wings([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Back([
      new Breakable,
    ]),
    new Tail([
      new Severable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(-1), new Water(1), new Thunder(2), new Ice(1), new Dragon(3),
    new Poison(1), new Sleep(2), new Paralysis(2), new Blast(1), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Fire, new Fireblight, new Poison, new Stun,
  ]);

  // 1% Carve, 2% Tail Carve, 3% Head Break and 1% Back Break
  carves: Collection = new Collection([
    new RathalosScale(5),
    new RathalosMarrow(2),
    new RathalosShell(4),
    new RathalosWebbing(4),
    new RathalosPlate(1, 1),
    new RathalosTail(1),
    new RathalosScalePlus(0, 5),
    new RathalosCarapace(0, 4),
    new RathalosWing(0, 4),
    new RathalosMedulla(0, 3),
    new RathalosRuby(0, 1),
  ]);

  // 1% Capture, 6% Silver Reward and 13% Gold Reward
  rewards: Collection = new Collection([
    new RathalosShell(4),
    new RathalosScale(3),
    new RathWingtalon(3),
    new RathalosWebbing(2),
    new RathalosTail(2, 2),
    new RathalosPlate(1, 1),
    new MonsterBonePlus(3),
    new FlameSac(3),
    new RathalosCarapace(0, 4),
    new RathalosScalePlus(0, 4),
    new RathalosWing(0, 3),
    new MonsterHardbone(0, 3),
    new InfernoSac(0, 3),
    new RathalosRuby(0),
  ]);
}

export class RathalosScale extends Carve {
  id = 'rathalos-scale';
  value = 'Rathalos Scale';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class RathalosMarrow extends Carve {
  id = 'rathalos-marrow';
  value = 'Rathalos Marrow';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Back;
}

export class RathalosShell extends Drop {
  id = 'rathalos-shell';
  value = 'Rathalos Shell';
}

export class RathalosWebbing extends Carve {
  id = 'rathalos-webbing';
  value = 'Rathalos Webbing';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Wings;
}

export class RathalosPlate extends Carve {
  id = 'rathalos-plate';
  value = 'Rathalos Plate';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyAndAnother([new Head, new Back, new Tail]);
}

export class RathalosTail extends Carve {
  id = 'rathalos-tail';
  value = 'Rathalos Tail';
  injury: Injury = new Severable;
  anatomy: Anatomy = new Tail;
}

export class RathalosScalePlus extends Carve {
  id = 'rathalos-scale-plus';
  value = 'Rathalos Scale +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class RathalosCarapace extends Drop {
  id = 'rathalos-carapace';
  value = 'Rathalos Carapace';
}

export class RathalosWing extends Carve {
  id = 'rathalos-wing';
  value = 'Rathalos Wing';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Wings;
}

export class RathalosMedulla extends Carve {
  id = 'rathalos-medulla';
  value = 'Rathalos Medulla';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Back;
}

export class RathalosRuby extends Carve {
  id = 'rathalos-ruby';
  value = 'Rathalos Ruby';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyOrAnother([new Head, new Back, new Tail]);
}
