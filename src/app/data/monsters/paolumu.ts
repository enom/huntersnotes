import { Monster } from '../../monsters/monster';
import { Injury, Ailment } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { FlyingWyvern } from '../types';
import { CloralHighlands } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Back, Wings, NeckPouch } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun
} from '../damages';

import {
  NourishingExtract, MonsterKeenbone, MonsterBoneL, WyvernGem
} from '../materials';

export class Paolumu extends Monster {
  id = 'paolumu';
  value = 'Paolumu';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new FlyingWyvern,
    new CloralHighlands,
    // Physiology
    new Head([
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new NeckPouch([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Wings([
      new Breakable,
    ]),
    new Back([
      new Breakable,
    ]),
    new Tail([
      new Breakable,
    ]),
    // Weaknesses
    new Fire(3), new Water(-1), new Thunder(2), new Ice(1), new Dragon(1),
    new Poison(2), new Sleep(2), new Paralysis(2), new Blast(3), new Stun(3),
  ]);

  attacks: Collection = new Collection([
    new class extends Ailment {
      id = 'wind-pression';
      value = 'Wind Pressure';
    }
  ]);

  // 2% Carve
  carves: Collection = new Collection([
    new PaolumuScale(5),
    new PaolumuShell(3),
    new PaolumuPelt(4),
    new PaolumuWebbing(4),
    new PaolumuPeltPlus(0, 4),
    new PaolumuScalePlus(0, 5),
    new PaolumuCarapacePlus(0, 3),
    new PaolumuWing(0, 4),
    new WyvernGem(0, 1),
  ]);

  // 2% Capture, 1% Expedition, 1% Quest Reward, 8% Silver Reward and 16% Gold Reward
  rewards: Collection = new Collection([
    new PaolumuScale(4),
    new PaolumuPelt(4),
    new PaolumuShell(3),
    new PaolumuWebbing(2),
    new MonsterBoneL(3),
    new NourishingExtract(3, 3),
    new PaolumuScalePlus(0, 4),
    new PaolumuPeltPlus(0, 4),
    new PaolumuCarapacePlus(0, 3),
    new PaolumuWing(0, 2),
    new MonsterKeenbone(0, 3),
    new WyvernGem(0, 1),
  ]);
}

export class PaolumuScale extends Carve {
  id = 'paolumu-scale';
  value = 'Paolumu Scale';
}

export class PaolumuShell extends Carve {
  id = 'paolumu-shell';
  value = 'Paolumu Shell';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Tail;
}

export class PaolumuPelt extends Carve {
  id = 'paolumu-pelt';
  value = 'Paolumu Pelt';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Back;
}

export class PaolumuWebbing extends Carve {
  id = 'paolumu-webbing';
  value = 'Paolumu Webbing';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Wings;
}

export class PaolumuPeltPlus extends Carve {
  id = 'paolumu-pelt-plus';
  value = 'Paolumu Pelt +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Back;
}

export class PaolumuScalePlus extends Carve {
  id = 'paolumu-scale-plus';
  value = 'Paolumu Scale +';
}

export class PaolumuCarapacePlus extends Carve {
  id = 'paolumu-carapace-plus';
  value = 'Paolumu Carapace +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Tail;
}

export class PaolumuWing extends Carve {
  id = 'paolumu-wing';
  value = 'Paolumu Wing';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Wings;
}
