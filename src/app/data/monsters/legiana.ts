import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy, AnatomyAndAnother, AnatomyOrAnother } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { FlyingWyvern } from '../types';
import { CloralHighlands } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Back, Wings, Legs } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Iceblight,
} from '../damages';

import {
  MonsterBonePlus, FrostSac, MonsterHardbone, FreezerSac
} from '../materials';

export class Legiana extends Monster {
  id = 'legiana';
  value = 'Legiana';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new FlyingWyvern,
    new CloralHighlands,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Wings([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Back([
      new Breakable,
    ]),
    new Tail([
      new Severable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(2), new Water(1), new Thunder(3), new Ice(-1), new Dragon(1),
    new Poison(3), new Sleep(2), new Paralysis(2), new Blast(2), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Ice, new Iceblight
  ]);

  // 2% Carve, 1% Head Break, 1% Back Break and 1% Tail Break
  carves: Collection = new Collection([
    new LegianaHide(4),
    new LegianaScale(5),
    new LegianaClaw(4),
    new LegianaWebbing(3),
    new LegianaTailWebbing(1),
    new LegianaPlate(1, 1),
    new LegianaHidePlus(0, 4),
    new LegianaScalePlus(0, 5),
    new LegianaClawPlus(0, 4),
    new LegianaWing(0, 3),
    new LegianaGem(0, 1),
  ]);

  // 6% Silver Reward, 13% Gold Reward and 2% Capture
  rewards: Collection = new Collection([
    new LegianaHide(4),
    new LegianaScale(4),
    new LegianaClaw(3),
    new LegianaWebbing(2),
    new MonsterBonePlus(3),
    new FrostSac(3),
    new LegianaPlate(1, 1),
    new LegianaHidePlus(0, 4),
    new LegianaScalePlus(0, 4),
    new LegianaClawPlus(0, 3),
    new LegianaWing(0, 2),
    new MonsterHardbone(0, 3),
    new FreezerSac(0, 3),
    new LegianaGem(0, 1),
  ]);

}

export class LegianaHide extends Carve {
  id = 'legiana-hide';
  value = 'Legiana Hide';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyOrAnother([new Head, new Back]);
}

export class LegianaScale extends Carve {
  id = 'legiana-scale';
  value = 'Legiana Scale';
}

export class LegianaClaw extends Drop {
  id = 'legiana-claw';
  value = 'Legiana Claw';
}

export class LegianaWebbing extends Carve {
  id = 'legiana-webbing';
  value = 'Legiana Webbing';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Wings;
}

export class LegianaTailWebbing extends Carve {
  id = 'legiana-tail-webbing';
  value = 'Legiana Tail Webbing';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Tail;
}

export class LegianaPlate extends Carve {
  id = 'legiana-plate';
  value = 'Legiana Plate';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyOrAnother([new Head, new Back, new Tail]);
}

export class LegianaHidePlus extends Carve {
  id = 'legiana-hide-plus';
  value = 'Legiana Hide +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyOrAnother([new Head, new Back]);
}

export class LegianaScalePlus extends Carve {
  id = 'legiana-scale-plus';
  value = 'Legiana Scale +';
}

export class LegianaClawPlus extends Drop {
  id = 'legiana-claw-plus';
  value = 'Legiana Claw +';
}

export class LegianaWing extends Carve {
  id = 'legiana-wing';
  value = 'Legiana Wing';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Wings;
}

export class LegianaGem extends Carve {
  id = 'legiana-gem';
  value = 'Legiana Gem';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyOrAnother([new Head, new Back, new Tail]);
}
