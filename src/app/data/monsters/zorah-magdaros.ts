import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import {ElderDragon} from '../types';
import { AncientForest } from '../expeditions';
import { Head, Chest, Magmacore, Tail } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Fireblight,
} from '../damages';

export class ZorahMagdaros extends Monster {
  id = 'zorah-magdaros';
  value = 'Zorah Magdaros';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new ElderDragon,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(1), new Bludgeoning(1), new Piercing(1),
    ]),
    new Magmacore([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Chest([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(-1), new Water(3), new Thunder(1), new Ice(2), new Dragon(3),
    new Poison(-1), new Sleep(-1), new Paralysis(-1), new Blast(-1), new Stun(-1),
  ]);

  attacks: Collection = new Collection([
    new Fire, new Fireblight,
  ]);

  carves: Collection = new Collection([
    // n/a
  ]);

  rewards: Collection = new Collection([
    new ZorahMagdarosHeatScale(4),
    new ZorahMagdarosRidge(4),
    new ZorahMagdarosPleura(3),
    new ZorahMagdarosMagma(3),
    new ZorahMagdarosCarapace(3),
    new ZorahMagdarosGem(1),
  ]);
}

export class ZorahMagdarosHeatScale extends Carve {
  id = 'zorah-magdaros-heat-scale';
  value = 'Zorah Magdaros Heat Scale';
}

export class ZorahMagdarosRidge extends Carve {
  id = 'zorah-magdaros-ridge';
  value = 'Zorah Magdaros Ridge';
}

export class ZorahMagdarosPleura extends Carve {
  id = 'zorah-magdaros-pleura';
  value = 'Zorah Magdaros Pleura';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Chest;
}

export class ZorahMagdarosMagma extends Carve {
  id = 'zorah-magdaros-magma';
  value = 'Zorah Magdaros Magma';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class ZorahMagdarosCarapace extends Drop {
  id = 'zorah-magdaros-carapace';
  value = 'Zorah Magdaros Carapace';
  injury: Injury = new Breakable;
}

export class ZorahMagdarosGem extends Carve {
  id = 'zorah-magdaros-gem';
  value = 'Zorah Magdaros Gem';
}
