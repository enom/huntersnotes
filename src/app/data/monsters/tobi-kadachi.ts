import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy, AnatomyOrAnother, AnatomyAndAnother } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { FangedWyvern } from '../types';
import { AncientForest } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Back } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Thunderblight,
} from '../damages';

import {
  MonsterBoneM, MonsterKeenbone, ElectroSac, ThunderSac, WyvernGem
} from '../materials';

export class TobiKadachi extends Monster {
  id = 'tobi-kadachi';
  value = 'Tobi-Kadachi';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new FangedWyvern,
    new AncientForest,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Back([
      new Breakable,
    ]),
    new Forelegs([
      new Breakable,
    ]),
    new Tail([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    // Weaknesses
    new Fire(2), new Water(3), new Thunder(-1), new Ice(2), new Dragon(1),
    new Poison(3), new Sleep(2), new Paralysis(2), new Blast(2), new Stun(2),
  ]);

  attacks: Collection = new Collection([
    new Thunder, new Thunderblight,
  ]);

  // 2% Carve
  carves: Collection = new Collection([
    new TobiKadachiPelt(4, 4),
    new TobiKadachiElectrode(3, 3),
    new TobiKadachiMembrane(3, 3),
    new TobiKadachiClaw(3),
    new TobiKadachiScale(5, 4),
    new TobiKadachiScalePlus(0, 4),
    new TobiKadachiClawPlus(0, 3),
    new WyvernGem(0, 1),
  ]);

  // 2% Capture, 1% Expedition, 1% Quest Reward, 8% Silver Reward and 13% Gold Reward
  rewards: Collection = new Collection([
    new TobiKadachiPelt(4, 4),
    new TobiKadachiScale(4, 4),
    new TobiKadachiMembrane(3, 3),
    new TobiKadachiClaw(2),
    new MonsterBoneM(3),
    new ElectroSac(3),
    new TobiKadachiClawPlus(0, 2),
    new MonsterKeenbone(0, 3),
    new ThunderSac(0, 3),
    new WyvernGem(0, 1),
  ]);
}

export class TobiKadachiPelt extends Carve {
  id = 'tobi-kadachi-pelt';
  value = 'Tobi-Kadachi Pelt';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Back;
}

export class TobiKadachiElectrode extends Carve {
  id = 'tobi-kadachi-electrode';
  value = 'Tobi-Kadachi Electrode';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyAndAnother([new Head, new Tail]);
}

export class TobiKadachiMembrane extends Carve {
  id = 'tobi-kadachi-membrane';
  value = 'Tobi-Kadachi Membrane';
}

export class TobiKadachiClaw extends Carve {
  id = 'tobi-kadachi-claw';
  value = 'Tobi-Kadachi Claw';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Forelegs;
}

export class TobiKadachiScale extends Carve {
  id = 'tobi-kadachi-scale';
  value = 'Tobi-Kadachi Scale';
}

export class TobiKadachiScalePlus extends Carve {
  id = 'tobi-kadachi-scale-plus';
  value = 'Tobi-Kadachi Scale +';
}

export class TobiKadachiClawPlus extends Carve {
  id = 'tobi-kadachi-claw-plus';
  value = 'Tobi-Kadachi Claw +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Forelegs;
}
