import { Monster } from '../../monsters/monster';
import { Injury } from '../../monsters/damage';
import { Anatomy, AnatomyOrAnother, AnatomyAndAnother } from '../../monsters/anatomy';
import { Collection } from '../data';
import { Drop, Carve } from '../../drops/drop';

import { Large } from '../sizes';
import { FlyingWyvern } from '../types';
import { CloralHighlands, WildspireWaste } from '../expeditions';
import { Head, Chest, Forelegs, Tail, Back, Wings } from '../anatomies';

import {
  Breakable, Severable,
  Bludgeoning, Slashing, Piercing,
  Fire, Water, Thunder, Ice, Dragon,
  Poison, Sleep, Paralysis, Blast, Stun,
  Fireblight,
} from '../damages';

import {
  MonsterBoneL, FlameSac, MonsterKeenbone, InfernoSac,
  RathWingtalon, MonsterHardbone,
} from '../materials';

export class PinkRathian extends Monster {
  id = 'pink-rathian';
  value = 'PinkRathian';

  notes: Collection = new Collection([
    // Ecology
    new Large,
    new FlyingWyvern,
    new CloralHighlands, new WildspireWaste,
    // Physiology
    new Head([
      new Breakable,
      new Slashing(3), new Bludgeoning(3), new Piercing(3),
    ]),
    new Wings([
      new Breakable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    new Back([
      new Breakable,
    ]),
    new Tail([
      new Severable,
      new Slashing(2), new Bludgeoning(2), new Piercing(2),
    ]),
    // Weaknesses
    new Fire(-1), new Water(1), new Thunder(2), new Ice(1), new Dragon(3),
    new Poison(1), new Sleep(2), new Paralysis(2), new Blast(1), new Stun(3),
  ]);

  attacks: Collection = new Collection([
    new Fire, new Poison,
  ]);

  // 1% Carve, 1% Tail Carve, 1% Back Break and 3% Head Break
  carves: Collection = new Collection([
    new PinkRathianCarapace(0, 4),
    new PinkRathianScale(0, 5),
    new RathWingtalon(0, 4),
    new RathianWebbing(0, 3),
    new RathianPlate(0, 1),
    new RathianRuby(0, 1),
    new RathianSpikePlus(0, 2),
  ]);

  // 1% Capture, 1% Quest Reward, 8% Silver Reward and 13% Gold Reward
  rewards: Collection = new Collection([
    new PinkRathianCarapace(0, 4),
    new PinkRathianScale(0, 4),
    new RathWingtalon(0, 3),
    new RathianWebbing(0, 2),
    new RathianPlate(0, 1),
    new MonsterHardbone(0, 3),
    new InfernoSac(0, 3),
    new RathianRuby(0, 1),
  ]);
}

export class PinkRathianCarapace extends Drop {
  id = 'pink-rathian-carapace';
  value = 'Pink Rathian Carapace';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Head;
}

export class PinkRathianScale extends Carve {
  id = 'pink-rathian-scale';
  value = 'Pink Rathian Scale +';
}

export class RathianWebbing extends Carve {
  id = 'rathian-webbing';
  value = 'Rathian Webbing';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new Wings;
}

export class RathianPlate extends Carve {
  id = 'rathian-plate';
  value = 'Rathian Plate';
}

export class RathianRuby extends Carve {
  id = 'rathian-ruby';
  value = 'Rathian Ruby';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyOrAnother([new Head, new Back, new Tail]);
}

export class RathianSpikePlus extends Carve {
  id = 'rathian-spike-plus';
  value = 'Rathian Spike +';
  injury: Injury = new Breakable;
  anatomy: Anatomy = new AnatomyAndAnother([new Back, new Tail]);
}
