import { Drop, Carve } from '../drops/drop';

export class IronOre extends Drop {
  id = 'iron-ore';
  value = 'Iron Ore';
}

export class MonsterBoneS extends Drop {
  id = 'monster-bone-s';
  value = 'Monster Bone S';
}

export class MonsterBoneL extends Drop {
  id = 'monster-bone-l';
  value = 'Monster Bone L';
}

export class FlameSac extends Drop {
  id = 'flame-sac';
  value = 'Flame Sac';
}

export class MonsterKeenbone extends Drop {
  id = 'monster-keenbone';
  value = 'Monster Keenbone';
}

export class InfernoSac extends Drop {
  id = 'inferno-sac';
  value = 'Inferno Sac';
}

export class MonsterBoneM extends Drop {
  id = 'monster-bone-m';
  value = 'Monster Bone M';
}

export class FertileMud extends Drop {
  id = 'fertile-mud';
  value = 'Fertile Mud';
}

export class WyvernGem extends Carve {
  id = 'wyvern-gem';
  value = 'WyvernGem';
}

export class MonsterHardbone extends Drop {
  id = 'monster-hardbone';
  value = 'Monster Hardbone';
}

export class AetheryteShard extends Drop {
  id = 'aetheryte-shard';
  value = 'Aetheryte Shard';
}

export class MonsterBonePlus extends Drop {
  id = 'monster-bone-plus';
  value = 'Monster Bone +';
}

export class DashExtract extends Drop {
  id = 'dash-extract';
  value = 'Dash Extract';
}

export class NourishingExtract extends Drop {
  id = 'nourishing-extract';
  value = 'Nourishing Extract';
}

export class OmniplegiaSac extends Drop {
  id = 'omniplegia-sac';
  value = 'Omniplegia Sac';
}

export class ParalysisSac extends Drop {
  id = 'paralysis-sac';
  value = 'Paralysis Sac';
}

export class TorrentSac extends Drop {
  id = 'torrent-sac';
  value = 'Torrent Sac';
}

export class AquaSac extends Drop {
  id = 'aqua-sac';
  value = 'Aqua Sac';
}

export class Novacrystal extends Drop {
  id = 'novacrystal';
  value = 'Novacrystal';
}

export class ElderDragonBone extends Drop {
  id = 'elder-dragon-bone';
  value = 'Elder Dragon Bone';
}

export class ElderDragonBlood extends Drop {
  id = 'elder-dragon-blood';
  value = 'Elder Dragon Blood';
}

export class Lightcrystal extends Drop {
  id = 'lightcrystal';
  value = 'Lightcrystal';
}

export class FreezerSac extends Drop {
  id = 'freezer-sac';
  value = 'Freezer Sac';
}

export class FrostSac extends Drop {
  id = 'frost-sac';
  value = 'Frost Sac';
}

export class ToxinSac extends Drop {
  id = 'toxin-sac';
  value = 'Toxin Sac';
}

export class PoisonSac extends Drop {
  id = 'poison-sac';
  value = 'Poison Sac';
}

export class ComaSac extends Drop {
  id = 'coma-sac';
  value = 'Coma Sac';
}

export class SleepSac extends Drop {
  id = 'sleep-sac';
  value = 'Sleep Sac';
}

export class ThunderSac extends Drop {
  id = 'thunder-sac';
  value = 'Thunder Sac';
}

export class ElectroSac extends Drop {
  id = 'electro-sac';
  value = 'Electro Sac';
}

export class FirecellStone extends Drop {
  id = 'firecell-stone';
  value = 'Firecell Stone';
}

export class BirdWyvernGem extends Carve {
  id = 'bird-wyvern-gem';
  value = 'Bird Wyvern Gem';
}

export class RathWingtalon extends Carve {
  id = 'rath-wingtalon';
  value = 'Rath Wingtalon';
}

export class FireDragonScalePlus extends Drop {
  id = 'fire-dragon-scale-plus';
  value = 'Fire Dragon Scale +';
}

export class TeostraCarapace extends Carve {
  id = 'teostra-carapace';
  value = 'Teostra Carapace';
}

export class TeostraClawPlus extends Carve {
  id = 'teostra-claw-plus';
  value = 'Teostra Claw +';
}

export class TeostraHornPlus extends Carve {
  id = 'teostra-horn-plus';
  value = 'Teostra Horn +';
}

export class TeostraMane extends Carve {
  id = 'teostra-mane';
  value = 'Teostra Mane';
}

export class TeostraTail extends Carve {
  id = 'teostra-tail';
  value = 'Teostra Tail';
}

export class TeostraWebbing extends Carve {
  id = 'teostra-webbing';
  value = 'Teostra Webbing';
}

export class TeostraPowder extends Carve {
  id = 'teostra-powder';
  value = 'Lunastra Powder';
}
