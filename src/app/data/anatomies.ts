import { Anatomy } from '../monsters/anatomy';

export class Head extends Anatomy {
  id = 'head';
  value = 'Head';
}

export class Chest extends Anatomy {
  id = 'chest';
  value = 'Chest';
}

export class Forelegs extends Anatomy {
  id = 'forelegs';
  value = 'Forelegs';
}

export class Legs extends Anatomy {
  id = 'legs';
  value = 'Legs';
}

export class Wings extends Anatomy {
  id = 'wings';
  value = 'Wings';
}

export class Back extends Anatomy {
  id = 'back';
  value = 'Back';
}

export class Tail extends Anatomy {
  id = 'tail';
  value = 'Tail';
}

export class NeckPouch extends Anatomy {
  id = 'neck-pouch';
  value = 'Neck Pouch';
}

export class Horn extends Anatomy {
  id = 'horn';
  value = 'Horn';
}

export class Magmacore extends Anatomy {
  id = 'magmacore';
  value = 'Magmacore';
}

export class StomachSac extends Anatomy {
  id = 'stomach-sac';
  value = 'Stomach Sac';
}
