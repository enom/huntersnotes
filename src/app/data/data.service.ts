import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Monster } from '../monsters/monster';
import { Expedition } from '../expeditions/expedition';
import { Size } from '../sizes/size';
import { Type } from '../types/type';
import { Drop } from '../drops/drop';
import { Data, Collection } from './data';

import * as Large from './large';
import * as Small from './small';
import * as Endemic from './endemic';
import * as Expeditions from './expeditions';
import * as Sizes from './sizes';
import * as Types from './types';
import * as Drops from './drops';

@Injectable({
  providedIn: 'root'
})

export class DataService {
  private cache: Collection;

  constructor() {
    const cache = new Collection;
    const collections = [Large, Small, Endemic, Expeditions, Sizes, Types, Drops];

    collections.forEach((collection) => {
      for (const item in collection) {
        if (collection.hasOwnProperty(item)) { cache.add(new collection[item]); }
      }
    });

    this.cache = cache;
  }

  getData(id?: string): Observable<Data | Collection> {
    return Observable.create((observer) => {
      observer.next(id ? this.cache.one(id) : this.cache.all());
      observer.complete();
    });
  }

  getFiltered(filter: Function | object): Observable<Collection> {
    return Observable.create((observer) => {
      observer.next(this.cache.reset().some(filter).fork());
      observer.complete();
    });
  }

  getMonster(id: string): Observable<Monster> {
    return Observable.create((observer) => {
      observer.next(this.cache.one(id));
      observer.complete();
    });
  }

  getMonsters(): Observable<Collection> {
    return this.getFiltered({ type: 'monster' });
  }

  getSize(id: string): Observable<Size> {
    return Observable.create((observer) => {
      observer.next(this.cache.one(id));
      observer.complete();
    });
  }

  getSizes(): Observable<Collection> {
    return this.getFiltered({ type: 'size' });
  }

  getType(id: string): Observable<Type> {
    return Observable.create((observer) => {
      observer.next(this.cache.one(id));
    });
  }

  getTypes(): Observable<Collection> {
    return this.getFiltered({ type: 'type' });
  }

  getExpedition(id: string): Observable<Expedition> {
    return Observable.create((observer) => {
      observer.next(this.cache.one(id));
      observer.complete();
    });
  }

  getExpeditions(): Observable<Collection> {
    return this.getFiltered({ type: 'expedition' });
  }

  getDrop(id: string): Observable<Drop> {
    return Observable.create((observer) => {
      observer.next(this.cache.one(id));
      observer.complete();
    });
  }

  getDrops(): Observable<Collection> {
    return this.getFiltered({ type: /carve|drop/ });
  }

}
