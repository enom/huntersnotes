import { Collection } from './data';
import { Monster } from '../monsters/monster';

import { Endemic } from './sizes';
// import { AquaticLife } from './types'
import {
  AncientForest,
  WildspireWaste,
  CloralHighlands,
  RottenVale
} from './expeditions';

export class PinkParexus extends Monster {
  id = 'pink-parexus';
  value = 'Pink Parexus';
  notes: Collection = new Collection([
    new Endemic,
    // new AquaticLife,
    new AncientForest, new WildspireWaste,
    new CloralHighlands, new RottenVale
  ]);
}

export class Whetfish extends Monster {
  id = 'whetfish';
  value = 'Whetfish';
  notes: Collection = new Collection([
    new Endemic,
    // new AquaticLife,
    new AncientForest, new WildspireWaste,
    new CloralHighlands, new RottenVale
  ]);
}

export class Gunpowderfish extends Monster {
  id = 'gunpowderfish';
  value = 'Gunpowderfish';
  notes: Collection = new Collection([
    new Endemic,
    // new AquaticLife,
    new CloralHighlands, new RottenVale
  ]);
}
