import { Type } from '../types/type';

export class FangedWyvern extends Type {
  id = 'fanged-wynvern';
  value = 'Fanged Wyvern';
}

export class BirdWyvern extends Type {
  id = 'bird-wynvern';
  value = 'Bird Wyvern';
}

export class BruteWyvern extends Type {
  id = 'brute-wynvern';
  value = 'Brute Wyvern';
}

export class PiscineWyvern extends Type {
  id = 'piscine-wynvern';
  value = 'Piscine Wyvern';
}

export class FlyingWyvern extends Type {
  id = 'flying-wynvern';
  value = 'Flying Wyvern';
}

export class ElderDragon extends Type {
  id = 'elder-dragon';
  value = 'Elder Dragon';
}

// export class AquaticLife extends Type {
//   id: string = 'aquatic-life'
//   value: string = 'Aquatic Life'
// }
