import { Monster } from '../monsters/monster';
import { Collection } from './data';

import { Small } from './sizes';
import {
  AncientForest,
  WildspireWaste,
  CloralHighlands,
  RottenVale
} from './expeditions';

export class Aptonoth extends Monster {
  id = 'aptonoth';
  value = 'Aptonoth';
  notes: Collection = new Collection([
    new Small,
    new AncientForest
  ]);
}

export class Jagras extends Monster {
  id = 'jagras';
  value = 'Jagras';
  notes: Collection = new Collection([
    new Small,
    new AncientForest
  ]);
}

export class Mernos extends Monster {
  id = 'mernos';
  value = 'Mernos';
  notes: Collection = new Collection([
    new Small,
    new AncientForest, new WildspireWaste
  ]);
}

export class Vespoid extends Monster {
  id = 'vespoid';
  value = 'Vespoid';
  notes: Collection = new Collection([
    new Small,
    new AncientForest, new WildspireWaste,
    new CloralHighlands, new RottenVale
  ]);
}

export class Mosswine extends Monster {
  id = 'mosswine';
  value = 'Mosswine';
  notes: Collection = new Collection([
    new Small,
    new AncientForest, new WildspireWaste,
    new RottenVale
  ]);
}

export class Apceros extends Monster {
  id = 'apceros';
  value = 'Apceros';
  notes: Collection = new Collection([
    new Small,
    new WildspireWaste
  ]);
}

export class Kestodon extends Monster {
  id = 'kestodon';
  value = 'Kestodon';
  notes: Collection = new Collection([
    new Small,
    new AncientForest, new WildspireWaste
  ]);
}

export class Noios extends Monster {
  id = 'noios';
  value = 'Noios';
  notes: Collection = new Collection([
    new Small,
    new WildspireWaste
  ]);
}

export class Gajau extends Monster {
  id = 'gajau';
  value = 'Gajau';
  notes: Collection = new Collection([
    new Small,
    new AncientForest, new WildspireWaste
  ]);
}

export class Kelbi extends Monster {
  id = 'kelbi';
  value = 'Kelbi';
  notes: Collection = new Collection([
    new Small,
    new WildspireWaste, new CloralHighlands
  ]);
}

export class Raphinos extends Monster {
  id = 'raphinos';
  value = 'Raphinos';
  notes: Collection = new Collection([
    new Small,
    new CloralHighlands, new RottenVale
  ]);
}

export class Shamos extends Monster {
  id = 'shamos';
  value = 'Shamos';
  notes: Collection = new Collection([
    new Small,
    new CloralHighlands
  ]);
}

export class Girros extends Monster {
  id = 'girros';
  value = 'Girros';
  notes: Collection = new Collection([
    new Small,
    new RottenVale
  ]);
}

export class Hornetaur extends Monster {
  id = 'hornetaur';
  value = 'Hornetaur';
  notes: Collection = new Collection([
    new Small,
    new RottenVale
  ]);
}

export class Barnos extends Monster {
  id = 'barnos';
  value = 'Barnos';
  notes: Collection = new Collection([
    new Small
  ]);
}
