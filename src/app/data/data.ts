export interface Data {
  id: string;
  type: string;
  value: any;
}

export interface Collector {
  data: any[];
}

export interface Decorator {
  id: string;
  name: string;
  data: Data | Collector;
}

export class Collection implements Collector {
  data: any[];
  filtered: any[];

  constructor(
    data: Collection | Data[] = []
  ) {
    if (Array.isArray(data)) {
      this.data = data;
      this.filtered = data;
    } else {
      this.data = data.all();
      this.filtered = data.all();
    }
  }

  add(data: Data): Collection {
    this.data.push(data);

    return this;
  }

  remove(id: string): Collection {
    throw new Error(`Collection.remove() not yet implemented`);
  }

  one(id: string): Data {
    return this.data.find((data: Data) => data.id === id);
  }

  get(index: number = 0): Data {
    return this.data[index];
  }

  has(has: string | Data, filtered: boolean = false): boolean {
    const partial: Data[] = filtered ? this.filtered : this.data;

    for (let i = 0; i < partial.length; i++) {
      const data = partial[i];
      if (typeof has === 'string' && data.id === has) { return true; }
      if (typeof has !== 'string' && data.id === has.id) { return true; }
    }

    return false;
  }

  someFunction(predicate: Function, filtered: boolean = false): Collection {
    const data = filtered ? this.filtered : this.data;

    this.filtered = data.filter((item: Data) => predicate(item));

    return this;
  }

  someObject(filter: object, filtered: boolean = false): Collection {
    const has = Object.prototype.hasOwnProperty;
    const data = filtered ? this.filtered : this.data;

    this.filtered = data.filter((item: Data) => {
      for (const key in filter) {
        if (filter.hasOwnProperty(key)) {
          const accepts = filter[key];
          const value = item[key];

          switch (typeof accepts) {
            case 'string':
            case 'number': if (accepts !== value) { return false; } break;
            case 'function': if (!accepts(value)) { return false; } break;
            case 'object':
              if (typeof accepts.test !== 'function') {
                throw new Error(`Collection.some() Key ${key} must be a RexExp object`);
              }

              if (!accepts.test(value)) { return false; }
          }
        }
      }

      return true;
    });

    return this;
  }

  some(predicate: Function | object, filtered: boolean = false): Collection {
    if (typeof predicate === 'function') { return this.someFunction(predicate, filtered); }

    return this.someObject(predicate, filtered);
  }


  all(): Data[] {
    return this.data;
  }

  partial(): Data[] {
    return this.filtered;
  }

  each(executor: Function): Collection {
    this.data.forEach((data: Data) => executor(data));

    return this;
  }

  find(predicate: Function): Data {
    return this.data.find((data: Data) => predicate(data));
  }

  reset(): Collection {
    this.filtered = this.data;

    return this;
  }

  fork(): Collection {
    return new Collection(this.filtered);
  }

  clone(): Collection {
    return new Collection(this.data);
  }

}
