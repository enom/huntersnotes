import { Data, Collection } from './data';

import { Element, Ailment, Weakness, Injury } from '../monsters/damage';

/* -----------------------------------------------------------------------------
 * Elements
 * ---------------------------------------------------------------------------*/

export class Fire extends Element {
  id = 'fire';
  value = 'Fire';
}

export class Water extends Element {
  id = 'water';
  value = 'Water';
}

export class Thunder extends Element {
  id = 'thunder';
  value = 'Thunder';
}

export class Ice extends Element {
  id = 'ice';
  value = 'Ice';
}

export class Dragon extends Element {
  id = 'dragon';
  value = 'Dragon';
}

/* -----------------------------------------------------------------------------
 * Ailments
 * ---------------------------------------------------------------------------*/

export class Poison extends Ailment {
  id = 'poison';
  value = 'Poison';
}

export class Sleep extends Ailment {
  id = 'sleep';
  value = 'Sleep';
}

export class Paralysis extends Ailment {
  id = 'paralysis';
  value = 'Paralysis';
}

export class Blast extends Ailment {
  id = 'blast';
  value = 'Blast';
}

export class Stun extends Ailment {
  id = 'stun';
  value = 'Stun';
}

export class Bleeding extends Ailment {
  id = 'bleeding';
  value = 'Bleeding';
}

export class EffluvialBuildup extends Ailment {
  id = 'effluvial-buildup';
  value = 'Effluvial Buildup';
}

export class Fireblight extends Ailment {
  id = 'firelight';
  value = 'Fireblight';
}

export class Waterblight extends Ailment {
  id = 'waterblight';
  value = 'Waterblight';
}

export class Thunderblight extends Ailment {
  id = 'thunderblight';
  value = 'Thunderblight';
}

export class Iceblight extends Ailment {
  id = 'iceblight';
  value = 'Iceblight';
}

export class Dragonblight extends Ailment {
  id = 'dragonblight';
  value = 'Dragonblight';
}

export class Blastblight extends Ailment {
  id = 'blastblight';
  value = 'Blastblight';
}

export class Miasma extends Ailment {
  id = 'miasma';
  value = 'Miasma';
}

/* -----------------------------------------------------------------------------
 * Physical
 * ---------------------------------------------------------------------------*/

export class Slashing extends Weakness {
  id = 'slashing';
  value = 'Slashing';
}

export class Bludgeoning extends Weakness {
  id = 'bludgeoning';
  value = 'Bludgeoning';
}

export class Piercing extends Weakness {
  id = 'piercing';
  value = 'Piercing';
}

/* -----------------------------------------------------------------------------
 * Injuries
 * ---------------------------------------------------------------------------*/

export class Breakable extends Injury {
  id = 'breakable';
  value = 'Breakable';
}

export class Severable extends Injury {
  id = 'severable';
  value = 'Severable';
}
