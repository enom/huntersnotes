export interface Linkable {
  url(): string;
  label(): string;
}
