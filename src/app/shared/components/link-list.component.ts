import { Component, OnInit, Input } from '@angular/core';

import { Linkable } from '../linkable';

@Component({
  selector: 'app-link-list',
  templateUrl: './link-list.component.html',
  styleUrls: ['./link-list.component.scss']
})

export class LinkListComponent implements OnInit {
  @Input() links: Linkable[];

  constructor() { }

  ngOnInit() { }

}
