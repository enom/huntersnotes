import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkPillsComponent } from './link-pills.component';

describe('LinkPillsComponent', () => {
  let component: LinkPillsComponent;
  let fixture: ComponentFixture<LinkPillsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkPillsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkPillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
