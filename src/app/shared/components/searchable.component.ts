import { Component, ViewChild, AfterViewInit, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-searchable',
  templateUrl: './searchable.component.html',
  styleUrls: ['./searchable.component.scss']
})

export class SearchableComponent implements AfterViewInit {
    @Input() items: any[];
    @Input() filter: Function;

    @Output() filtered: EventEmitter = new EventEmitter<any[] | false>();

    @ViewChild('element') element: ElementRef;

    control: FormControl = new FormControl;

    private searching = false;

    constructor() {
      const changed = (criteria: string) => this.search(criteria);

      this.control.valueChanges
        .pipe(debounceTime(350), distinctUntilChanged())
        .subscribe(changed);
    }

    ngAfterViewInit() {
      this.focus();
    }

    clear() {
      this.control.setValue('');
      this.focus();
    }

    focus(): void {
      this.element.nativeElement.focus();
    }

    search(criteria: string) {
      if (criteria.length < 2) {
        if (this.searching) {
          this.searching = false;
          this.filtered.emit(false);
        }
      } else {
        this.searching = true;
        this.filtered.emit(this.items.filter((item: any) => this.filter(item, criteria)));
      }
    }

}
