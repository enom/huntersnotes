import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-link-row',
  templateUrl: './link-row.component.html',
  styleUrls: ['./link-row.component.scss']
})

export class LinkRowComponent implements OnInit {
  @Input() link: string;

  constructor() { }

  ngOnInit() {
  }

}
