import { Component, OnInit, Input } from '@angular/core';

import { Linkable } from '../linkable';

@Component({
  selector: 'app-link-pills',
  templateUrl: './link-pills.component.html',
  styleUrls: ['./link-pills.component.scss']
})

export class LinkPillsComponent implements OnInit {
  @Input() links: Linkable[];
  @Input() active: Linkable;
  @Input() class = '';

  constructor() { }

  ngOnInit() { }

}
