import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { StarsPipe } from './pipes/stars.pipe';
import { LinkListComponent } from './components/link-list.component';
import { LinkPillsComponent } from './components/link-pills.component';
import { LinkRowComponent } from './components/link-row.component';
import { SearchableComponent } from './components/searchable.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [
    StarsPipe,
    LinkListComponent,
    LinkPillsComponent,
    LinkRowComponent,
    SearchableComponent,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    StarsPipe,
    LinkListComponent,
    LinkPillsComponent,
    LinkRowComponent,
    SearchableComponent
  ],
})

export class SharedModule { }
