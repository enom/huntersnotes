import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stars'
})

export class StarsPipe implements PipeTransform {

  transform(value: string | number): string {
    const length = typeof value === 'string' ? parseInt(value, 10) : value;
    if (length < 0) { return 'x'; }
    if (length === 1) { return '*'; }
    return (new Array(length)).join('*');
  }

}
