import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DropsComponent } from './drops.component';

const routes: Routes = [
  {
    path: ':id',
    component: DropsComponent
  },
  {
    path: '',
    component: DropsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DropsRoutingModule { }
