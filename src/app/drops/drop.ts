import { Data } from '../data/data';
import { Linkable } from '../shared/linkable';
import { Anatomy, AnatomyDecorator } from '../monsters/anatomy';
import { Monster, MonsterDecorator } from '../monsters/monster';
import { Injury, DamageDecorator } from '../monsters/damage';

export class Drop implements Data {
  id: string;
  type = 'drop';
  lowrank: number;
  highrank: number;
  value: any;

  constructor(
    lowrank?: number,
    highrank?: number
  ) {
    this.lowrank = lowrank;
    this.highrank = highrank;
  }
}

export class Carve extends Drop {
  type = 'carve';
  anatomy: Anatomy;
  injury: Injury;
}

export class DropDecorator implements Linkable {
  drop: Drop;
  name: string;
  id: string;
  type: string;
  value: any;
  anatomy: AnatomyDecorator;
  injury: DamageDecorator;
  lowrank: number;
  highrank: number;

  constructor(
    drop: Carve | Drop
  ) {
    const has = Object.prototype.hasOwnProperty;

    this.drop = drop;
    this.name = drop.value;
    //
    this.id = drop.id;
    this.type = drop.type;
    this.value = drop.value;
    //
    this.lowrank = drop.lowrank;
    this.highrank = drop.highrank;

    if (has.call(drop, 'anatomy')) {
      this.anatomy = new AnatomyDecorator((<Carve>drop).anatomy);
    }

    if (has.call(drop, 'injury')) {
      this.injury = new DamageDecorator((<Carve>drop).injury);
    }
  }

  url(): string {
    return `/drops/${this.id}`;
  }

  label(): string {
    return this.name;
  }
}
