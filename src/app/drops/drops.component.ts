import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Params, ActivatedRoute } from '@angular/router';

import { Drop, Carve,  DropDecorator } from './drop';
import { Collection } from '../data/data';
import { DataService } from '../data/data.service';
import { Monster, MonsterDecorator } from '../monsters/monster';

@Component({
  selector: 'app-drops',
  templateUrl: './drops.component.html',
  styleUrls: ['./drops.component.scss']
})

export class DropsComponent implements OnInit {
  drops: DropDecorator[] = [];
  drop: DropDecorator;
  monsters: MonsterDecorator[] = [];
  request: Params;

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private data: DataService
  ) {
    route.params.subscribe((params: Params) => {
      this.request = params;

      if (params.id) {
        this.getDrop(params.id);
      } else {
        this.getDrops();
      }
    });
  }

  ngOnInit() { }

  getDrops(): void {
    const subscribed = (drops: Collection) => {
      this.drops = drops
        .all().map((drop: Drop) => new DropDecorator(drop))
        .sort((a: DropDecorator, b: DropDecorator) => {
          if (a.name < b.name) { return -1; }
          if (a.name > b.name) { return 1; }
          return 0;
        });
    };

    this.data
      .getDrops()
      .subscribe(subscribed);
  }

  getDrop(id: string): void {
    const maybeMonster = (monster: Monster) => {
      const pushed = this.monsters.find((decorated: MonsterDecorator) => decorated.id === monster.id);

      // Skip existing monsters
      if (pushed !== undefined) { return; }

      // Push if monster has drop
      if (monster.carves.has(id) || monster.rewards.has(id)) {
        this.monsters.push(new MonsterDecorator(monster));
      }
    };

    const getMonsters = (monsters: Collection, carve: Carve) => {
      monsters.each((monster: Monster) => maybeMonster(monster));
    };

    const getDrop = (drop: Carve) => {
      this.drop = new  DropDecorator(drop);
      this.data
        .getMonsters()
        .subscribe((monsters: Collection) => getMonsters(monsters, drop));
    };

    this.data
      .getDrop(id)
      .subscribe(getDrop);
  }

}
