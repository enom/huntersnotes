import { NgModule } from '@angular/core';
import { DropsRoutingModule } from './drops-routing.module';
import { DropsComponent } from './drops.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    DropsRoutingModule
  ],
  declarations: [
    DropsComponent
  ]
})

export class DropsModule { }
