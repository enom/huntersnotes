import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

import { Data, Collection } from '../data/data';
import { DataService } from '../data/data.service';
import { Monster, MonsterDecorator } from '../monsters/monster';
import { Drop, DropDecorator } from '../drops/drop';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit, AfterViewInit {
  @ViewChild('vc') vc: ElementRef;
  fc: FormControl = new FormControl;
  searching = false;
  monsters: MonsterDecorator[] = [];
  drops: DropDecorator[] = [];
  request: Params;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private data: DataService
  ) {
    const criteria = route.snapshot.params.text;

    const search = (params: Params) => {
      this.request = params;

      if (params.text) {
        this.search(decodeURIComponent(params.text));
      } else {
        this.clear(true);
      }
    };

    const navigate = (value: string) => {
      const options = { replaceUrl: true };

      if (value.length < 2) {
        this.router.navigate(['search'], options);
      } else {
        this.router.navigate(['search', encodeURIComponent(value)], options);
      }
    };

    if (criteria) { this.fc.setValue(decodeURIComponent(criteria)); }

    this.route.params
      .subscribe(search);

    this.fc.valueChanges
      .pipe(debounceTime(350), distinctUntilChanged())
      .subscribe(navigate);
  }

  ngOnInit() { }

  ngAfterViewInit() {
    const input: HTMLInputElement = this.vc.nativeElement;

    // Always focus input after loading page
    input.focus();
  }

  sort(a: Data, b: Data): number {
    if (a.value < b.value) { return -1; }
    if (a.value > b.value) { return 1; }
    return 0;
  }

  search(criteria: string) {
    const subscribed = (collection: Collection) => {
      this.monsters = collection.reset()
        .some((data: Data) => data.type === 'monster')
        .partial()
        .sort((a: Data, b: Data) => this.sort(a, b))
        .map((monster: Monster) => new MonsterDecorator(monster));

      this.drops = collection.reset()
        .some((data: Data) => /carve|drop/.test(data.type))
        .partial()
        .sort((a: Data, b: Data) => this.sort(a, b))
        .map((drop: Drop) => new DropDecorator(drop));
    };

    const search = new RegExp(criteria, 'i');

    this.searching = true;
    this.data
      .getFiltered((data: Data) => search.test(data.value))
      .subscribe(subscribed);
  }

  clear(criteria: boolean = false) {
    this.searching = false;
    this.monsters = [];
    this.drops = [];

    if (criteria) { this.fc.setValue(''); }
  }

}
