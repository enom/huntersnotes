import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tinker',
  templateUrl: './tinker.component.html',
  styleUrls: ['./tinker.component.scss']
})

export class TinkerComponent implements OnInit {
  exports: string[] = [];
  objects: string[] = [];
  drops: string[] = PinkRathian.rewards;

  constructor() {
    this.drops.forEach((str) => {
      if (/^[A-Z]/.test(str)) {
        // console.log(`name, not ID`, str)
        // tslint:disable-next-line
        const [ , name ] = str.match(/([\w\s+-]+)(.*)/);
        const safe = name.replace(/\s+|\t+/g, ' ').trim();
        const norank = safe.replace(/ High Rank/, '');
        const plus = norank.replace(/\+/, 'Plus');

        str = plus.replace(/\s/g, '_').toLowerCase() + str;
        // console.log(`conversion name "${name}" safe "${safe}" norank "${norank}" plus "${plus}"`, str)
      }

      const tabs = /\s+|\t+/g;
      const names = /([a-z_-]+)([A-Z][\w\s-]+\+?)/;
      const stars = /(⭐+)/;
      const highrank = /High Rank\s*(⭐+)/;
      const bothrank = /(⭐+) \(High Rank\s*(⭐+)\)/;
      const breaks = /(\([\w\s]+)\)/;

      const clean = str.replace(tabs, ' ').trim();
      const [ , tid, tname ] = clean.match(names);

      let cls = tid.split(/_/)
        .map((part) => `${part[0].toUpperCase()}${part.substr(1)}`).join('');
      const id = tid.replace(/_/g, '-').trim();
      let name = tname.trim();
      let low = 0;
      let high = 0;

      if (bothrank.test(clean)) {
        // Contains both low and high rank frequency
        // console.log(`bothrank`, { id, name })
        // console.log(clean.match(bothrank))
        const [ , tl, th ] = clean.match(bothrank);

        low = tl.length;
        high = th.length;
      } else if (highrank.test(clean)) {
        // High rank frequency
        // console.log(`highrank`, { id, name })
        // console.log(clean.match(highrank))
        const [ , th ] = clean.match(highrank);

        high = th.length;
        name = name.replace(/High Rank/, '').trim();
      } else {
        // Low rank frequency
        // console.log(`regular`, { id, name })
        // console.log(clean.match(stars))
        // console.log(clean.match(breaks))
        const [ , tl ] = clean.match(stars) || [ , ''];

        low = tl.length;
      }

      const parent = /drop/.test(clean) ? `Drop` : `Carve`;
      cls = cls.split(/-/).map((v) => v[0].toUpperCase() + v.slice(1)).join('');

      this.objects.push(`
new ${cls}(${low}` + (high ? `, ${high}` : ``) + `),`);

      this.exports.push(`
export class ${cls} extends ${parent} {
  id: string = '${id}'
  value: string = '${name}'`
+ (/break|broken/i.test(clean) ? `
  injury: Injury = new Breakable` : ``)
+ (/cut/i.test(clean) ? `
  injury: Injury = new Severable` : ``)
+ (/head/i.test(clean) ? `
  anatomy: Anatomy = new Head` : ``)
+ (/horns?/i.test(clean) ? `
  anatomy: Anatomy = new Horn` : ``)
+ (/chest/i.test(clean) ? `
  anatomy: Anatomy = new Chest` : ``)
+ (/back/i.test(clean) ? `
  anatomy: Anatomy = new Back` : ``)
+ (/wings?/i.test(clean) ? `
  anatomy: Anatomy = new Wings` : ``)
+ (/legs?/i.test(clean) ? `
  anatomy: Anatomy = new Legs` : ``)
+ (/forelegs?/i.test(clean) ? `
  anatomy: Anatomy = new Forelegs` : ``)
+ (/tail/i.test(clean) ? `
  anatomy: Anatomy = new Tail` : ``)
+ `
}
`);
    });
  }

  ngOnInit() { }

}

class Barroth {
  static carves: string[] = [
    'barroth_ridgeBarroth Ridge	⭐⭐⭐⭐	 ',
    'barroth_clawBarroth Claw (break forelegs)	⭐⭐⭐	 ',
    'barroth_tailBarroth Tail (carve severed tail)	⭐⭐⭐⭐⭐ (High Rank ⭐⭐⭐)	 ',
    'barroth_shellBarroth Shell (break legs)	⭐⭐⭐	 ',
    'barroth_scalpBarroth Scalp (break head)	⭐⭐⭐	 ',
    'fertile_mudFertile Mud (dropped)	⭐⭐⭐	 ',
    'barroth_ridge_plusBarroth Ridge +	High Rank ⭐⭐⭐⭐	 ',
    'barroth_claw_plusBarroth Claw + (break forelegs)	High Rank ⭐⭐⭐	 ',
    'barroth_carapaceBarroth Carapace (dropped, break legs)	High Rank ⭐⭐⭐⭐⭐	 ',
    'barroth_scalpBarroth Scalp (break head)	High Rank ⭐⭐⭐	 ',
    'wyvern_gemWyvern Gem	High Rank ⭐'
  ];

  static rewards: string[] = [
    'barroth_ridgeBarroth Ridge	⭐⭐⭐⭐	 ',
    'barroth_shellBarroth Shell	⭐⭐⭐⭐	 ',
    'barroth_clawBarroth Claw	⭐⭐⭐	 ',
    'barroth_scalpBarroth Scalp	⭐⭐	 ',
    'monster_bone_mMonster Bone M	⭐⭐⭐	 ',
    'fertile_mudFertile Mud	⭐⭐⭐	 ',
    'barroth_ridge_plusBarroth Ridge +	High Rank ⭐⭐⭐⭐	 ',
    'barroth_carapaceBarroth Carapace	High Rank ⭐⭐⭐⭐	 ',
    'barroth_claw_plusBarroth Claw +	High Rank ⭐⭐⭐	 ',
    'barroth_scalpBarroth Scalp	High Rank ⭐⭐⭐	 ',
    'monster_keenboneMonster Keenbone	High Rank ⭐⭐⭐	 ',
    'wyvern_gemWyvern Gem	High Rank ⭐'
  ];
}

class Bazelgeuse {
  static carves = [
    'bazelgeuse_scale_plusBazelgeuse Scale +	⭐⭐⭐⭐⭐	 ',
    'bazelgeuse_carapaceBazelgeuse Carapace (break back)	⭐⭐⭐⭐	 ',
    'bazelgeuse_talonBazelgeuse Talon (dropped)	⭐⭐⭐	 ',
    'bazelgeuse_fuseBazelgeuse Fuse (break head)	⭐⭐⭐	 ',
    'bazelgeuse_wingBazelgeuse Wing (break wings)	⭐⭐⭐	 ',
    'Bazelgeuse-gem-mhwBazelgeuse Gem (break head, tail or legs)	⭐',
    'bazelgeuse_tailBazelgeuse Tail (Cut tail)	 ',
  ];

  static rewards = [
    'bazelgeuse_carapaceBazelgeuse Carapace	⭐⭐⭐⭐	 ',
    'bazelgeuse_scale_plusBazelgeuse Scale +	⭐⭐⭐⭐	 ',
    'bazelgeuse_fuseBazelgeuse Fuse	⭐⭐⭐	 ',
    'bazelgeuse_talonBazelgeuse Talon	⭐⭐⭐	 ',
    'bazelgeuse_wingBazelgeuse Wing	⭐⭐	 ',
    'bazelgeuse_tailBazelgeuse Tail	⭐⭐	 ',
    'monster_hardboneMonster Hardbone	⭐⭐⭐	 ',
    'Bazelgeuse-gem-mhwBazelgeuse Gem	 ',
  ];
}

class Behemoth {
  static carves = [
    'behemoth_maneBehemoth Mane (dropped, carved)	High Rank⭐⭐⭐⭐⭐	 ',
    'behemoth_boneBehemoth Bone	High Rank⭐⭐⭐⭐	 ',
    'behemoth_tailBehemoth Tail (cut tail)	High Rank⭐⭐⭐	 ',
    'behemoth_shearclawBehemoth Shearclaw (forelegs broken)	High Rank⭐⭐⭐	 ',
    'behemoth_great_hornBehemoth Great Horn (Horn Broken)	High Rank⭐⭐',
  ];

  static rewards = [
    'behemoth_boneBehemoth Bone	High Rank ⭐⭐⭐⭐⭐	 ',
    'behemoth_maneBehemoth Mane	High Rank ⭐⭐⭐⭐	 ',
    'behemoth_shearclawBehemoth Shearclaw	High Rank ⭐⭐⭐	 ',
    'behemoth_tailBehemoth Tail	High Rank ⭐⭐	 ',
    'behemoth_great_hornBehemoth Great Horn	High Rank ⭐	 ',
    'aetheryte_shardAetheryte Shard	High Rank ⭐⭐⭐⭐	 ',
  ];
}

class Deviljho {
  static carves = [
    'deviljho_hideDeviljho Hide	⭐⭐⭐⭐⭐	 ',
    'deviljho_scaleDeviljho Scale (break chest)	⭐⭐⭐⭐	 ',
    'deviljho_talonDeviljho Talon	⭐⭐⭐	 ',
    'deviljho_tallfangDeviljho Tallfang (break head)	⭐⭐⭐	 ',
    'deviljho_scalpDeviljho Scalp	⭐⭐	 ',
    'deviljho_salivaDeviljho Saliva (dropped)	⭐⭐⭐	 ',
    'deviljho_tailDeviljho Tail (cut tail)	⭐⭐	 ',
    'deviljho_gemDeviljho Gem (break head, chest or tail)	⭐'
  ];

  static rewards = [
    'deviljho_hideDeviljho Hide	⭐⭐⭐⭐	 ',
    'deviljho_scaleDeviljho Scale	⭐⭐⭐⭐	 ',
    'deviljho_talonDeviljho Talon	⭐⭐⭐	 ',
    'deviljho_tallfangDeviljho Tallfang	⭐⭐⭐	 ',
    'deviljho_salivaDeviljho Saliva	⭐⭐⭐	 ',
    'deviljho_tailDeviljho Tail	⭐⭐	 ',
    'monster_hardboneMonster Hardbone	⭐⭐⭐',
  ];
}

class Diablos {
  static carves = [
    'diablos_ridgeDiablos Ridge (dropped)	⭐⭐⭐⭐	 ',
    'diablos_fangDiablos Fang	⭐⭐⭐	 ',
    'diablos_shellDiablos Shell	⭐⭐⭐⭐⭐	 ',
    'diablos_tailcaseDiablos Tailcase (carve tail)	⭐⭐⭐	 ',
    'diablos_marrowDiablos Marrow  (break back)	⭐⭐⭐	 ',
    'twisted_hornTwisted Horn (break horns)	⭐	 ',
    'diablos_ridge_plusDiablos Ridge + (dropped)	High Rank ⭐⭐⭐⭐	 ',
    'diablos_fangDiablos Fang	High Rank⭐⭐⭐	 ',
    'diablos_carapaceDiablos Carapace	High Rank⭐⭐⭐⭐⭐	 ',
    'diablos_tailcaseDiablos Tailcase (carve tail)	High Rank⭐⭐⭐	 ',
    'blos_medullaBlos Medulla (break back)	High Rank⭐⭐⭐	 ',
    'majestic_hornMajestic Horn (break both horns)	High Rank ⭐	 ',
    'wyvern_gemWyvern Gem (cut tail)	High Rank ⭐	 ',
    'wyvern_gemWyvern Gem	 	3% Carve and 3% Tail Carve',
  ];

  static rewards = [
    'diablos_ridgeDiablos Ridge	⭐⭐⭐⭐	 ',
    'diablos_shellDiablos Shell	⭐⭐⭐⭐	 ',
    'diablos_fangDiablos Fang	⭐⭐⭐	 ',
    'diablos_marrowDiablos Marrow	⭐⭐	 ',
    'monster_bone_plusMonster Bone +	⭐⭐⭐	 ',
    'dash_extractDash Extract	⭐⭐⭐	 ',
    'diablos_ridge_plusDiablos Ridge +	High Rank⭐⭐⭐⭐	 ',
    'diablos_carapaceDiablos Carapace	High Rank⭐⭐⭐⭐	 ',
    'diablos_fangDiablos Fang	High Rank⭐⭐⭐	 ',
    'blos_medullaBlos Medulla	High Rank⭐⭐	 ',
    'monster_hardboneMonster Hardbone	High Rank⭐⭐⭐	 ',
    'dash_extractDash Extract	High Rank⭐⭐⭐	 ',
    'wyvern_gemWyvern Gem	High Rank ⭐',
  ];
}

class Dodogama {
  static carves = [
    'dodogama_hide_plusDodogama Hide +	High Rank⭐⭐⭐⭐',
    'dodogama_jawDodogama Jaw (break head)	High Rank⭐⭐⭐',
    'dodogama_scale_plusDodogama Scale +	High Rank⭐⭐⭐⭐⭐',
    'dodogama_tailDodogama Tail (carve tail)	High Rank⭐⭐⭐',
    'dodogama_talonDodogama Talon (dropped, break forelegs)	High Rank⭐⭐⭐',
  ];

  static rewards = [
    'dodogama_hide_plusDodogama Hide +	High Rank⭐⭐⭐⭐',
    'dodogama_scale_plusDodogama Scale +	High Rank⭐⭐⭐⭐',
    'dodogama_jawDodogama Jaw	High Rank⭐⭐',
    'dodogama_talonDodogama Talon	High Rank⭐⭐⭐',
    'monster_keenboneMonster Keenbone	High Rank⭐⭐⭐',
    'nourishing_extractNourishing Extract	High Rank⭐⭐⭐'
  ];
}

class GreatGirros {
  static carves = [
    'great_girros_hideGreat Girros Hide (break forelegs)	⭐⭐⭐⭐',
    'great_girros_hoodGreat Girros Hood (break head)	⭐⭐⭐',
    'great_girros_scaleGreat Girros Scale	⭐⭐⭐⭐⭐',
    'great_girros_tailGreat Girros Tail (carve tail)	⭐⭐⭐',
    'great_girros_fangGreat Girros Fang (dropped)	⭐⭐⭐',
    'great_girros_fang_plusGreat Girros Hide + (break forelegs)	High Rank ⭐⭐⭐⭐',
    'great_girros_hood_plusGreat Girros Hood + (break head)	High Rank ⭐⭐⭐',
    'great_girros_scale_plusGreat Girros Scale +	High Rank ⭐⭐⭐⭐⭐',
    'great_girros_tailGreat Girros Tail (carve tail)	High Rank ⭐⭐⭐',
    'great_girros_fang_plusGreat Girros Fang + (dropped)	High Rank ⭐⭐⭐'
  ];

  static rewards = [
    'great_girros_hideGreat Girros Hide	⭐⭐⭐⭐',
    'great_girros_scaleGreat Girros Scale	⭐⭐⭐⭐',
    'great_girros_hoodGreat Girros Hood	⭐⭐',
    'great_girros_fangGreat Girros Fang	⭐⭐⭐',
    'monster_bone_lMonster Bone L	⭐⭐⭐',
    'paralysis_sacParalysis Sac	⭐⭐⭐',
    'great_girros_fang_plusGreat Girros Hide +	High Rank⭐⭐⭐⭐',
    'great_girros_scale_plusGreat Girros Scale +	High Rank⭐⭐⭐⭐',
    'great_girros_hood_plusGreat Girros Hood +	High Rank⭐⭐',
    'great_girros_fang_plusGreat Girros Fang +	High Rank⭐⭐⭐',
    'monster_bone_plusMonster Bone +	High Rank⭐⭐⭐',
    'omniplegia_sacOmniplegia Sac	High Rank⭐⭐⭐'
  ];
}

class GreatJagras {
  static carves = [
    'great_jagras_scaleGreat Jagras Scale	⭐⭐⭐⭐⭐',
    'great_jagras_hideGreat Jagras Hide	⭐⭐⭐⭐',
    'great_jagras_maneGreat Jagras Mane (break head)	⭐⭐⭐',
    'great_jagras_clawGreat Jagras Claw	⭐⭐⭐⭐',
    'great_jagras_scale_plusGreat Jagras Scale +	High Rank⭐⭐⭐⭐⭐',
    'great_jagras_hide_plusGreat Jagras Hide + (break chest)	High Rank⭐⭐⭐⭐',
    'great_jagras_maneGreat Jagras Mane	High Rank⭐⭐⭐',
    'great_jagras_claw_plusGreat Jagras Claw + (dropped, break forelegs)	High Rank⭐⭐⭐⭐',
  ];

  static rewards = [
    'great_jagras_hideGreat Jagras Hide	⭐⭐⭐⭐⭐',
    'great_jagras_scaleGreat Jagras Scale	⭐⭐⭐⭐',
    'great_jagras_maneGreat Jagras Mane	⭐⭐⭐',
    'great_jagras_clawGreat Jagras Claw	⭐⭐',
    'iron_oreIron Ore	⭐⭐⭐',
    'monster_bone_sMonster Bone S	⭐⭐⭐',
    'great_jagras_hide_plusGreat Jagras Hide +	High Rank⭐⭐⭐⭐⭐',
    'great_jagras_scale_plusGreat Jagras Scale +	High Rank⭐⭐⭐⭐',
    'great_jagras_maneGreat Jagras Mane	High Rank⭐⭐⭐',
    'great_jagras_claw_plusGreat Jagras Claw +	High Rank⭐⭐',
    'monster_bone_plusMonster Bone +	High Rank⭐⭐⭐⭐',
  ];
}

class Jyuratodus {
  static carves = [
    'jyuratodus_scaleJyuratodus Scale	⭐⭐⭐⭐⭐	 ',
    'jyuratodus_shellJyuratodus Shell	⭐⭐⭐⭐	 ',
    'jyuratodus_fangJyuratodus Fang (break head)	⭐⭐⭐	 ',
    'jyuratodus_finJyuratodus Fin (dropped, break chest)	⭐⭐⭐⭐	 ',
    'jyuratodus_scale_plusJyuratodus Scale +	High Rank ⭐⭐⭐⭐⭐	 ',
    'jyuratodus_carapaceJyuratodus Carapace	High Rank ⭐⭐⭐⭐	 ',
    'jyuratodus_fang_plusJyuratodus Fang + (Break head)	High Rank ⭐⭐⭐	 ',
    'jyuratodus_fin_plusJyuratodus Fin + (Dropped, break chest)	High Rank ⭐⭐⭐⭐	 ',
    'wyvern_gemWyvern Gem	High Rank ⭐	2% Carve',
  ];

  static rewards = [
    'jyuratodus_shellJyuratodus Shell	⭐⭐⭐⭐	 ',
    'jyuratodus_scaleJyuratodus Scale	⭐⭐⭐⭐	 ',
    'jyuratodus_fangJyuratodus Fang	⭐⭐⭐	 ',
    'jyuratodus_finJyuratodus Fin	⭐⭐	 ',
    'monster_bone_mMonster Bone M	⭐⭐⭐	 ',
    'aqua_sacAqua Sac	⭐⭐⭐	 ',
    'jyuratodus_carapaceJyuratodus Carapace	High Rank ⭐⭐⭐⭐	 ',
    'jyuratodus_scale_plusJyuratodus Scale +	High Rank ⭐⭐⭐⭐	 ',
    'jyuratodus_fang_plusJyuratodus Fang +	High Rank ⭐⭐⭐	 ',
    'jyuratodus_fin_plusJyuratodus Fin +	High Rank ⭐⭐	 ',
    'monster_keenboneMonster Keenbone	High Rank ⭐⭐⭐	 ',
    'torrent_sacTorrent Sac	High Rank ⭐⭐⭐	 ',
    'wyvern_gemWyvern Gem	High Rank ⭐',
  ];
}

class Kirin {
  static carves = [
    'Kirin Thunderhorn (break horn)	⭐⭐⭐',
    'Kirin Hide	⭐⭐⭐⭐⭐',
    'Kirin Mane (dropped)	⭐⭐⭐⭐',
    'Kirin Tail	⭐⭐⭐⭐',
    'Kirin Azure Horn (break horn)	High Rank⭐⭐⭐',
    'Kirin Hide + (dropped)	High Rank⭐⭐⭐⭐⭐',
    'Kirin Mane	High Rank⭐⭐⭐⭐',
    'Kirin Thundertail	High Rank⭐⭐⭐⭐',
  ];

  static rewards = [
    'Kirin Hide	⭐⭐⭐⭐',
    'Kirin Thunderhorn	⭐⭐⭐',
    'Kirin Mane	⭐⭐⭐⭐⭐',
    'Kirin Tail	⭐⭐⭐⭐',
    'Lightcrystal	⭐⭐',
    'Kirin Hide +	High Rank⭐⭐⭐⭐',
    'Kirin Mane	High Rank⭐⭐⭐⭐',
    'Kirin Thundertail	High Rank⭐⭐⭐',
    'Novacrystal	High Rank⭐⭐',
    'elder_dragon_boneElder Dragon Bone	High Rank⭐⭐⭐',
    'elder_dragon_bloodElder Dragon Blood	High Rank⭐⭐⭐',
  ];
}

class KuluYaKu {
  static carves = [
    'kulu-ya-ku_scaleKulu-Ya-Ku Scale	⭐⭐⭐⭐⭐',
    'kulu-ya-ku_hideKulu-Ya-Ku Hide	⭐⭐⭐⭐',
    'kulu-ya-ku_plumeKulu-Ya-Ku Plume (dropped, break forelegs)	⭐⭐⭐',
    'kulu-ya-ku_beakKulu-Ya-Ku Beak (break head)	⭐⭐⭐⭐',
    'kulu-ya-ku_scale_plusKulu-Ya-Ku Scale +	High Rank ⭐⭐⭐⭐⭐',
    'kulu-ya-ku_hide_plusKulu-Ya-Ku Hide +	High Rank ⭐⭐⭐⭐',
    'kulu-ya-ku_plume_plusKulu-Ya-Ku Plume + (dropped, break forelegs)	High Rank ⭐⭐⭐',
    'kulu-ya-ku_beak_plusKulu-Ya-Ku Beak + (break head)	High Rank ⭐⭐⭐⭐',
    'bird_wyvern_gemBird Wyvern Gem	High Rank ⭐',
  ];

  static rewards = [
    'kulu-ya-ku_hideKulu-Ya-Ku Hide	⭐⭐⭐⭐⭐',
    'kulu-ya-ku_scaleKulu-Ya-Ku Scale	⭐⭐⭐⭐',
    'kulu-ya-ku_plumeKulu-Ya-Ku Plume	⭐⭐⭐',
    'kulu-ya-ku_beakKulu-Ya-Ku Beak	⭐⭐',
    'monster_bone_mMonster Bone S	⭐⭐⭐⭐',
    'kulu-ya-ku_hide_plusKulu-Ya-Ku Hide +	High Rank ⭐⭐⭐⭐⭐',
    'kulu-ya-ku_scale_plusKulu-Ya-Ku Scale +	High Rank ⭐⭐⭐⭐',
    'kulu-ya-ku_plume_plusKulu-Ya-Ku Plume +	High Rank ⭐⭐⭐',
    'kulu-ya-ku_beak_plusKulu-Ya-Ku Beak +	High Rank ⭐⭐⭐',
    'monster_bone_plusMonster Bone +	High Rank ⭐⭐⭐',
    'bird_wyvern_gemBird Wyvern Gem	High Rank ⭐',
  ];
}

class KulveTaroth {
  static carves = [
    'kulve_taroth_golden_spiralhornKulve Taroth Golden Spiralhorn (break horns)	⭐⭐⭐⭐	 ',
    'kulve_taroth_golden_shellKulve Taroth Golden Shell (carve gold plating)	⭐⭐⭐⭐⭐	 ',
    'Melded Weapon	⭐⭐⭐⭐⭐	 ',
    'Dissolved Weapon	⭐⭐	 ',
    'Sublimated Weapon	⭐⭐	 ',
    'kulve_taroth_golden_scaleKulve Taroth Golden Scale (dropped)	⭐⭐⭐⭐	 ',
    'kulve_taroth_golden_tailshellKulve Taroth Golden Tailshell (break tail)	⭐⭐⭐⭐	 ',
    'kulve_taroth_golden_nuggetKulve Taroth Golden Nugget	⭐⭐⭐⭐⭐	 ',
    'kulve_taroth_golden_glimstoneKulve Taroth Golden Glimstone (carve gold plating, break horns, break leg)	⭐',
  ];

  static rewards = [
    'kulve_taroth_golden_scaleKulve Taroth Golden Scale	⭐⭐⭐⭐	 ',
    'kulve_taroth_golden_shellKulve Taroth Golden Shell	⭐⭐⭐⭐⭐	 ',
    'kulve_taroth_golden_spiralhornKulve Taroth Golden Spiralhorn	⭐⭐⭐	 ',
    'kulve_taroth_golden_tailshellKulve Taroth Golden Tailshell	⭐⭐⭐	 ',
    'kulve_taroth_golden_glimstoneKulve Taroth Golden Glimstone	⭐⭐	 ',
    'elder_dragon_boneElder Dragon Bone	⭐⭐	 ',
    'elder_dragon_bloodElder Dragon Blood	⭐⭐',
  ];
}

class KushalaDaora {
  static carves = [
    'Daora Dragon Scale +	⭐⭐⭐⭐⭐	 ',
    'Daora Carapace (dropped)	⭐⭐⭐⭐	 ',
    'Daora Claw +	⭐⭐⭐	 ',
    'Daora Horn + (break head)	⭐⭐	 ',
    'Daora Webbing (break wings)	⭐⭐⭐	 ',
    'Daora Gem (break head, break tail)	⭐',
    'Daora Tail (carve tail)	⭐⭐',
  ];

  static rewards = [
    'Daora Carapace	⭐⭐⭐⭐	 ',
    'Daora Dragon Scale +	⭐⭐⭐⭐	 ',
    'Daora Claw +	⭐⭐⭐	 ',
    'Daora Webbing	⭐⭐⭐	 ',
    'elder_dragon_boneElder Dragon Bone	⭐⭐	 ',
    'elder_dragon_bloodElder Dragon Blood	⭐⭐⭐ 	 ',
    'Daora Gem (break head, break tail)	⭐ ',
  ];
}

class Lavasioth {
  static carves = [
    'lavasioth_scale_plusLavasioth Scale +	High Rank⭐⭐⭐⭐⭐',
    'lavasioth_carapaceLavasioth Carapace	High Rank⭐⭐⭐⭐',
    'lavasioth_fang_plusLavasioth Fang + (break head)	High Rank⭐⭐⭐',
    'lavasioth_fin_plusLavasioth Fin + (dropped, break chest)	High Rank⭐⭐⭐⭐',
    'wyvern_gemWyvern Gem	High Rank⭐',
  ];

  static rewards = [
    'lavasioth_carapaceLavasioth Carapace	High Rank⭐⭐⭐⭐',
    'lavasioth_scale_plusLavasioth Scale +	High Rank⭐⭐⭐⭐',
    'lavasioth_fang_plusLavasioth Fang +	High Rank⭐⭐⭐',
    'lavasioth_fin_plusLavasioth Fin +	High Rank⭐⭐',
    'monster_hardboneMonster Hardbone	High Rank⭐⭐⭐',
    'inferno_sacInferno Sac	High Rank⭐⭐⭐',
    'wyvern_gemWyvern Gem	High Rank⭐',
  ];
}

class Legiana {
  static carves = [
    'legiana_hideLegiana Hide (break head or back)	⭐⭐⭐⭐	 ',
    'legiana_scaleLegiana Scale	⭐⭐⭐⭐⭐	 ',
    'legiana_clawLegiana Claw (dropped)	⭐⭐⭐⭐	 ',
    'legiana_webbingLegiana Webbing (break wings)	⭐⭐⭐	 ',
    'legiana_tail_webbingLegiana Tail Webbing (break tail any rank)	⭐	 ',
    'legiana_plateLegiana Plate (break tail, back, or head)	⭐	 ',
    'legiana_hide_plusLegiana Hide + (break head, break back)	High Rank ⭐⭐⭐⭐	 ',
    'legiana_scale_plusLegiana Scale +	High Rank ⭐⭐⭐⭐⭐	 ',
    'legiana_claw_plusLegiana Claw + (dropped)	High Rank ⭐⭐⭐⭐	 ',
    'legiana_wingLegiana Wing (break wings)	High Rank ⭐⭐⭐	 ',
    'legiana_plateLegiana Plate	High Rank ⭐	 ',
    'legiana_gemLegiana Gem (break head, back, or tail)	High Rank ⭐',
  ];

  static rewards = [
    'legiana_hideLegiana Hide	⭐⭐⭐⭐	 ',
    'legiana_scaleLegiana Scale	⭐⭐⭐⭐	 ',
    'legiana_clawLegiana Claw	⭐⭐⭐	 ',
    'legiana_webbingLegiana Webbing	⭐⭐	 ',
    'monster_bone_plusMonster Bone +	⭐⭐⭐	 ',
    'frost_sacFrost Sac	⭐⭐⭐	 ',
    'legiana_plateLegiana Plate	⭐	 ',
    'legiana_hide_plusLegiana Hide +	High Rank ⭐⭐⭐⭐	 ',
    'legiana_scale_plusLegiana Scale +	High Rank ⭐⭐⭐⭐	 ',
    'legiana_claw_plusLegiana Claw +	High Rank ⭐⭐⭐	 ',
    'legiana_wingLegiana Wing	High Rank ⭐⭐	 ',
    'legiana_plateLegiana Plate	High Rank ⭐	 ',
    'monster_hardboneMonster Hardbone	High Rank ⭐⭐⭐	 ',
    'freezer_sacFreezer Sac	High Rank ⭐⭐⭐	 ',
    'legiana_gemLegiana Gem	High Rank ⭐',
  ];
}

class Lunastra {
  static carves = [
    'fire_dragon_scale_plusFire Dragon Scale + (dropped)	High Rank⭐⭐⭐⭐⭐	 ',
    'teostra_carapaceLunastra Carapace	High Rank⭐⭐⭐⭐	 ',
    'teostra_claw_plusLunastra Claw +	High Rank⭐⭐⭐	 ',
    'teostra_horn_plusLunastra Horn + (break head)	High Rank⭐⭐	 ',
    'teostra_maneLunastra Mane	High Rank⭐⭐⭐	 ',
    'Lunastra Gem (break head, break tail)	High Rank⭐',
    'teostra_tailLunastra Tail (break tail)	High Rank⭐⭐⭐	 ',
    'teostra_webbingLunastra Webbing (break wings)	High Rank⭐⭐',
  ];

  static rewards = [
    'teostra_carapaceLunastra Carapace	High Rank ⭐⭐⭐⭐	 ',
    'fire_dragon_scale_plusFire Dragon Scale +	High Rank ⭐⭐⭐⭐	 ',
    'teostra_powderLunastra Powder	High Rank ⭐⭐⭐	 ',
    'teostra_maneLunastra Mane	High Rank ⭐⭐⭐	 ',
    'teostra_claw_plusLunastra Claw +	High Rank ⭐⭐⭐	 ',
    'elder_dragon_boneElder Dragon Bone	High Rank ⭐⭐	 ',
    'elder_dragon_bloodElder Dragon Blood	High Rank ⭐⭐	 ',
    'Lunastra Gem	High Rank ⭐',
  ];
}

class Nergigante {
  static carves = [
    'immortal_dragonscaleImmortal Dragonscale	⭐⭐⭐⭐⭐	 ',
    'nergigante_carapaceNergigante Carapace	⭐⭐⭐⭐	 ',
    'nergigante_talonNergigante Talon	⭐⭐⭐	 ',
    'Nergigante Horn + (break horns)	⭐⭐	 ',
    'nergigante_regrowth_plateNergigante Regrowth Plate (dropped)	⭐⭐⭐	 ',
    'Nergigante Gem (break horns, tail)	⭐',
    'nergigante_tailNergigante Tail (cut tail)	 ',
  ];

  static rewards = [
    'nergigante_carapaceNergigante Carapace	⭐⭐⭐⭐	 ',
    'immortal_dragonscaleImmortal Dragonscale	⭐⭐⭐⭐	 ',
    'nergigante_regrowth_plateNergigante Regrowth Plate	⭐⭐⭐	 ',
    'nergigante_talonNergigante Talon	⭐⭐⭐	 ',
    'elder_dragon_boneElder Dragon Bone	⭐⭐⭐	 ',
    'elder_dragon_bloodElder Dragon Blood	⭐⭐	 ',
    'Nergigante Gem ',
  ];
}

class Odogaron {
  static carves = [
    'odogaron_sinewOdogaron Sinew (break legs)	⭐⭐⭐⭐	 ',
    'odogaron_scaleOdogaron Scale	⭐⭐⭐⭐⭐	 ',
    'odogaron_clawOdogaron Claw (break forelegs)	⭐⭐⭐⭐	 ',
    'odogaron_fangOdogaron Fang (dropped, break head)	⭐⭐⭐	 ',
    'odogaron_tailOdogaron Tail (carve tail)	High Rank⭐⭐	 ',
    'odogaron_sinew_plusOdogaron Sinew + (Legs Broken)	High Rank⭐⭐⭐⭐	 ',
    'odogaron_scale_plusOdogaron Scale +	High Rank⭐⭐⭐⭐⭐	 ',
    'odogaron_claw_plusOdogaron Claw + (break forelegs)	High Rank⭐⭐⭐⭐	 ',
    'odogaron_fang_plusOdogaron Fang + (dropped, break head)	High Rank⭐⭐⭐	 ',
    'Odogaron Gem (Head Broken, carve tail)	High Rank⭐',
    'Odogaron Plate (Carve Tail)	High Rank⭐',
  ];

  static rewards = [
    'odogaron_sinewOdogaron Sinew	⭐⭐⭐⭐',
    'odogaron_scaleOdogaron Scale	⭐⭐⭐⭐',
    'odogaron_clawOdogaron Claw	⭐⭐⭐',
    'odogaron_tailOdogaron Tail	⭐⭐',
    'monster_bone_plusMonster Bone +	⭐⭐⭐',
    'Nourishing Extract	⭐⭐⭐',
    'odogaron_sinew_plusOdogaron Sinew +	High Rank⭐⭐⭐⭐',
    'odogaron_scale_plusOdogaron Scale +	High Rank⭐⭐⭐⭐',
    'odogaron_claw_plusOdogaron Claw +	High Rank⭐⭐⭐',
    'odogaron_tailOdogaron Tail	High Rank⭐⭐',
    'Odogaron Plate	High Rank⭐',
    'monster_hardboneMonster Hardbone	High Rank⭐⭐⭐',
    'Nourishing Extract	High Rank⭐⭐⭐',
    'Odogaron Gem',
  ];
}

class Paolumu {
  static carves = [
    'paolumu_scalePaolumu Scale	⭐⭐⭐⭐⭐	 ',
    'paolumu_shellPaolumu Shell (Dropped, break tail)	⭐⭐⭐	 ',
    'paolumu_peltPaolumu Pelt (Break back, break neck pouch)	⭐⭐⭐⭐	 ',
    'paolumu_webbingPaolumu Webbing (Break wings)	⭐⭐⭐⭐	 ',
    'paolumu_pelt_plusPaolumu Pelt + (Break back, break neck pouch)	High Rank ⭐⭐⭐⭐	 ',
    'paolumu_scale_plusPaolumu Scale +	High Rank ⭐⭐⭐⭐⭐	 ',
    'paolumu_carapace_plusPaolumu Carapace + (Dropped, break tail)	High Rank ⭐⭐⭐	 ',
    'paolumu_wingPaolumu Wing (Break wings)	High Rank ⭐⭐⭐⭐	 ',
    'wyvern_gemWyvern Gem	High Rank ⭐',
  ];

  static rewards = [
    'paolumu_scalePaolumu Scale	⭐⭐⭐⭐	 ',
    'paolumu_peltPaolumu Pelt	⭐⭐⭐⭐	 ',
    'paolumu_shellPaolumu Shell	⭐⭐⭐	 ',
    'paolumu_webbingPaolumu Webbing	⭐⭐	 ',
    'monster_bone_lMonster Bone L	⭐⭐⭐	 ',
    'Nourishing Extract	⭐⭐⭐ (High Rank ⭐⭐⭐)	 ',
    'paolumu_scale_plusPaolumu Scale +	High Rank ⭐⭐⭐⭐	 ',
    'paolumu_pelt_plusPaolumu Pelt +	High Rank ⭐⭐⭐⭐	 ',
    'paolumu_carapace_plusPaolumu Carapace +	High Rank ⭐⭐⭐	 ',
    'paolumu_wingPaolumu Wing	High Rank ⭐⭐	 ',
    'monster_keenboneMonster Keenbone	High Rank ⭐⭐⭐	 ',
    'wyvern_gemWyvern Gem	High Rank ⭐',
  ];
}

class PukeiPukei {
  static carves = [
    'Pukei-Pukei Shell (break back)	⭐⭐⭐⭐',
    'Pukei-Pukei Quill (dropped, break wings)	⭐⭐⭐',
    'Pukei-Pukei Scale	⭐⭐⭐⭐⭐',
    'Pukei-Pukei Tail (break and carve tail)	⭐⭐⭐ (High Rank ⭐⭐⭐)',
    'Pukei-Pukei Sac (break head)	⭐⭐⭐',
    'Pukei-Pukei Carapace (Break back)	High Rank ⭐⭐⭐⭐',
    'Pukei-Pukei Wing (Break wings)	High Rank ⭐⭐⭐',
    'Pukei-Pukei Scale + (Dropped)	High Rank ⭐⭐⭐⭐',
    'Pukei-Pukei Sac + (Break head)	High Rank ⭐⭐⭐',
    'Bird Wyvern Gem	High Rank ⭐',
  ];

  static rewards = [
    'Pukei-Pukei Shell	⭐⭐⭐⭐',
    'Pukei-Pukei Scale	⭐⭐⭐⭐',
    'Pukei-Pukei Quill	⭐⭐⭐',
    'Pukei-Pukei Sac	⭐⭐',
    'Monster Bone M	⭐⭐⭐',
    'Poison Sac	⭐⭐⭐',
    'Pukei-Pukei Carapace	High Rank ⭐⭐⭐⭐',
    'Pukei-Pukei Scale +	High Rank ⭐⭐⭐',
    'Pukei-Pukei Wing	High Rank ⭐⭐⭐',
    'Pukei-Pukei Sac +	High Rank ⭐⭐',
    'Monster Keenbone	High Rank ⭐⭐⭐',
    'Toxin Sac	High Rank ⭐⭐⭐',
    'Bird Wyvern Gem	High Rank ⭐',
  ];
}

class Radobaan {
  static carves = [
    'Radobaan Shell	⭐⭐⭐⭐',
    'Radobaan Scale	⭐⭐⭐⭐⭐',
    'Radobaan Oilshell (break head low rank, tail high rank)	⭐⭐⭐⭐',
    'Radobaan Marrow (carve tail low rank)	⭐⭐⭐',
    'Wyvern Gem (dropped, break legs, break back)	⭐',
    'Radobaan Carapace	High Rank ⭐⭐⭐⭐',
    'Radobaan Scale + (dropped)	High Rank ⭐⭐⭐⭐⭐',
    'Radobaan Medulla	High Rank ⭐⭐⭐',
  ];

  static rewards = [
    'Radobaan Shell	⭐⭐⭐⭐',
    'Radobaan Scale	⭐⭐⭐⭐',
    'Radobaan Oilshell	⭐⭐⭐',
    'Radobaan Marrow	⭐⭐⭐',
    'Monster Bone L	⭐⭐⭐',
    'Sleep Sac	⭐⭐⭐',
    'Radobaan Carapace	High Rank ⭐⭐⭐⭐',
    'Radobaan Scale +	High Rank ⭐⭐⭐⭐',
    'Radobaan Medulla	High Rank ⭐⭐⭐',
    'Monster Keenbone	High Rank ⭐⭐⭐',
    'Coma Sac	High Rank ⭐⭐⭐',
    'Wyvern Gem	High Rank ⭐',
  ];
}

class Rathalos {
  static carves = [
    'rathalos_scaleRathalos Scale (break head)	⭐⭐⭐⭐⭐	 ',
    'rathalos_marrowRathalos Marrow (break back)	⭐⭐	 ',
    'rathalos_shellRathalos Shell (dropped)	⭐⭐⭐⭐	 ',
    'rathalos_webbingRathalos Webbing (break wings)	⭐⭐⭐⭐	 ',
    'rathalos_plateRathalos Plate (break back, break head, break tail)	⭐	 ',
    'rathalos_tailRathalos Tail (cut tail any rank)	?⭐	 ',
    'rathalos_scale_plusRathalos Scale + (break head)	High Rank⭐⭐⭐⭐⭐	 ',
    'rathalos_carapaceRathalos Carapace (dropped)	High Rank⭐⭐⭐⭐	 ',
    'rathalos_wingRathalos Wing (break wings)	High Rank⭐⭐⭐⭐	 ',
    'rathalos_medullaRathalos Medulla (break back)	High Rank⭐⭐⭐	 ',
    'rathalos_plateRathalos Plate	High Rank⭐	 ',
    'rathalos_rubyRathalos Ruby (break back, head or tail)	High Rank⭐',
  ];

  static rewards = [
    'rathalos_shellRathalos Shell	⭐⭐⭐⭐	 ',
    'rathalos_scaleRathalos Scale	⭐⭐⭐	 ',
    'rath_wingtalonRath Wingtalon	⭐⭐⭐	 ',
    'rathalos_webbingRathalos Webbing	⭐⭐	 ',
    'rathalos_tailRathalos Tail	⭐⭐ (High Rank ⭐⭐)	 ',
    'rathalos_plateRathalos Plate	⭐	 ',
    'monster_bone_plusMonster Bone +	⭐⭐⭐	 ',
    'flame_sacFlame Sac	⭐⭐⭐	 ',
    'rathalos_carapaceRathalos Carapace	High Rank⭐⭐⭐⭐	 ',
    'rathalos_scale_plusRathalos Scale +	High Rank⭐⭐⭐⭐	 ',
    'rathalos_wingRathalos Wing	High Rank⭐⭐⭐	 ',
    'rathalos_plateRathalos Plate	High Rank⭐	 ',
    'monster_hardboneMonster Hardbone	High Rank⭐⭐⭐	 ',
    'inferno_sacInferno Sac	High Rank⭐⭐⭐	 ',
    'rathalos_rubyRathalos Ruby	',
  ];
}

class Rathian {
  static carves = [
    'rathian_plateRathian Plate (break head, back or tail)	⭐	 ',
    'rathian_scaleRathian Scale	⭐⭐⭐⭐⭐	 ',
    'rathian_shellRathian Shell (dropped, break head)	⭐⭐⭐⭐	 ',
    'rathian_spikeRathian Spike (break back, carve tail)	⭐⭐	 ',
    'rathian_webbingRathian Webbing	⭐⭐⭐	 ',
    'rath_wingtalonRath Wingtalon (break wings)	⭐⭐⭐	 ',
    'rathian_carapaceRathian Carapace (dropped, break head)	High Rank⭐⭐⭐⭐	 ',
    'rathian_scale_plusRathian Scale +	High Rank⭐⭐⭐⭐⭐	 ',
    'rathian_webbingRathian Webbing (break wings)	High Rank⭐⭐⭐⭐	 ',
    'rathian_spike_plusRathian Spike + (break back, break tail)	High Rank⭐⭐⭐	 ',
    'rathian_plateRathian Plate	High Rank⭐	 ',
    'Rathian Ruby (break head, tail, back)	High Rank⭐',
  ];

  static rewards = [
    'rathian_shellRathian Shell	⭐⭐⭐⭐	 ',
    'rathian_scaleRathian Scale	⭐⭐⭐⭐	 ',
    'rath_wingtalonRath Wingtalon	⭐⭐⭐	 ',
    'rathian_webbingRathian Webbing	⭐⭐	 ',
    'rathian_plateRathian Plate	⭐	 ',
    'monster_bone_lMonster Bone L	⭐⭐⭐	 ',
    'flame_sacFlame Sac	⭐⭐⭐	 ',
    'rathian_carapaceRathian Carapace	High Rank⭐⭐⭐⭐	 ',
    'rathian_scale_plusRathian Scale +	High Rank⭐⭐⭐⭐	 ',
    'rathian_webbingRathian Webbing	High Rank⭐⭐⭐	 ',
    'rathian_spike_plusRathian Spike +	High Rank⭐⭐	 ',
    'rathian_plateRathian Plate	High Rank⭐	 ',
    'monster_keenboneMonster Keenbone	High Rank⭐⭐⭐	 ',
    'inferno_sacInferno Sac	High Rank⭐⭐⭐	 ',
    'Rathian Ruby	 ',
  ];
}

class Teostra {
  static carves = [
    'fire_dragon_scale_plusFire Dragon Scale + (dropped)	High Rank⭐⭐⭐⭐⭐	 ',
    'teostra_carapaceTeostra Carapace	High Rank⭐⭐⭐⭐	 ',
    'teostra_claw_plusTeostra Claw +	High Rank⭐⭐⭐	 ',
    'teostra_horn_plusTeostra Horn + (break head)	High Rank⭐⭐	 ',
    'teostra_maneTeostra Mane	High Rank⭐⭐⭐	 ',
    'Teostra Gem (break head, break tail)	High Rank⭐',
    'teostra_tailTeostra Tail (break tail)	High Rank⭐⭐⭐	 ',
    'teostra_webbingTeostra Webbing (break wings)	High Rank⭐⭐',
  ];

  static rewards = [
    'teostra_carapaceTeostra Carapace	High Rank ⭐⭐⭐⭐	 ',
    'fire_dragon_scale_plusFire Dragon Scale +	High Rank ⭐⭐⭐⭐	 ',
    'teostra_powderTeostra Powder	High Rank ⭐⭐⭐	 ',
    'teostra_maneTeostra Mane	High Rank ⭐⭐⭐	 ',
    'teostra_claw_plusTeostra Claw +	High Rank ⭐⭐⭐	 ',
    'elder_dragon_boneElder Dragon Bone	High Rank ⭐⭐	 ',
    'elder_dragon_bloodElder Dragon Blood	High Rank ⭐⭐	 ',
    'Teostra Gem	High Rank ⭐',
  ];
}

class TobiKadachi {
  static carves = [
    'tobi-kadachi_peltTobi-Kadachi Pelt (Break back)	⭐⭐⭐⭐	 ',
    'tobi-kadachi_electrodeTobi-Kadachi Electrode (Break head, break tail)	⭐⭐⭐	 ',
    'tobi-kadachi_membraneTobi-Kadachi Membrane	⭐⭐⭐ (High Rank ⭐⭐⭐)	 ',
    'Tobi-Kadachi Claw (Dropped, break forelegs)	⭐⭐⭐	 ',
    'tobi-kadachi_scaleTobi-Kadachi Scale	⭐⭐⭐⭐⭐	 ',
    'tobi-kadachi_peltTobi-Kadachi Pelt + (Break back)	High Rank ⭐⭐⭐⭐	 ',
    'tobi-kadachi_scaleTobi-Kadachi Scale +	High Rank ⭐⭐⭐⭐	 ',
    'tobi-kadachi_electrodeTobi-Kadachi Electrode + (Break head, break tail)	High Rank ⭐⭐⭐	 ',
    'Tobi-Kadachi Claw + (Dropped, break forelegs)	High Rank ⭐⭐⭐	 ',
    'wyvern_gemWyvern Gem	High Rank ⭐',
  ];

  static rewards = [
    'tobi-kadachi_peltTobi-Kadachi Pelt	⭐⭐⭐⭐	 ',
    'tobi-kadachi_scaleTobi-Kadachi Scale	⭐⭐⭐⭐	 ',
    'tobi-kadachi_membraneTobi-Kadachi Membrane	⭐⭐⭐	 ',
    'Tobi-Kadachi Claw	⭐⭐	 ',
    'monster_bone_mMonster Bone M	⭐⭐⭐	 ',
    'electro_sacElectro Sac	⭐⭐⭐	 ',
    'tobi-kadachi_peltTobi-Kadachi Pelt +	High Rank ⭐⭐⭐⭐	 ',
    'tobi-kadachi_scaleTobi-Kadachi Scale +	High Rank ⭐⭐⭐⭐	 ',
    'tobi-kadachi_membraneTobi-Kadachi Membrane	High Rank ⭐⭐⭐	 ',
    'Tobi-Kadachi Claw +	High Rank ⭐⭐	 ',
    'monster_keenboneMonster Keenbone	High Rank ⭐⭐⭐	 ',
    'thunder_sacThunder Sac	High Rank ⭐⭐⭐	 ',
    'wyvern_gemWyvern Gem	High Rank ⭐',
  ];
}

class TzitziYaKu {
  static carves = [
    'tzitzi-ya-ku_scaleTzitzi-Ya-Ku Scale	⭐⭐⭐⭐⭐',
    'tzitzi-ya-ku_hideTzitzi-Ya-Ku Hide	⭐⭐⭐⭐',
    'tzitzi-ya-ku_clawTzitzi-Ya-Ku Claw (dropped)	⭐⭐⭐',
    'Tzitzi-Ya-Ku Photophore (break head)	⭐⭐⭐⭐',
    'tzitzi-ya-ku_scale_plusTzitzi-Ya-Ku Scale +	High Rank⭐⭐⭐⭐⭐',
    'tzitzi-ya-ku_hide_plusTzitzi-Ya-Ku Hide +	High Rank⭐⭐⭐⭐',
    'tzitzi-ya-ku_claw_plusTzitzi-Ya-Ku Claw + (dropped)	High Rank⭐⭐⭐',
    'tzitzi-ya-ku_photophere_plusTzitzi-Ya-Ku Photophore + (break head)	High Rank⭐⭐⭐⭐',
    'bird_wyvern_gemBird Wyvern Gem	High Rank⭐',
  ];

  static rewards = [
    'tzitzi-ya-ku_hideTzitzi-Ya-Ku Hide	⭐⭐⭐⭐',
    'tzitzi-ya-ku_scaleTzitzi-Ya-Ku Scale	⭐⭐⭐⭐',
    'tzitzi-ya-ku_clawTzitzi-Ya-Ku Claw	⭐⭐⭐',
    'Tzitzi-Ya-Ku Photophore	⭐⭐',
    'monster_bone_lMonster Bone L	⭐⭐⭐',
    'Dash Extract	⭐⭐⭐',
    'tzitzi-ya-ku_hide_plusTzitzi-Ya-Ku Hide +	High Rank⭐⭐⭐⭐',
    'tzitzi-ya-ku_scale_plusTzitzi-Ya-Ku Scale +	High Rank⭐⭐⭐⭐',
    'tzitzi-ya-ku_claw_plusTzitzi-Ya-Ku Claw +	High Rank⭐⭐⭐',
    'tzitzi-ya-ku_photophere_plusTzitzi-Ya-Ku Photophore +	High Rank⭐⭐',
    'monster_bone_plusMonster Bone +	High Rank⭐⭐⭐⭐',
    'Dash Extract	High Rank⭐⭐⭐⭐',
    'bird_wyvern_gemBird Wyvern Gem	High Rank⭐',
  ];
}

class Uragaan {
  static carves = [
    'uragaan_carapaceUragaan Carapace	High Rank⭐⭐⭐⭐',
    'uragaan_scaleUragaan Scale + (dropped)	High Rank⭐⭐⭐⭐⭐',
    'uragaan_scuteUragaan Scute (break back, carve tail)	High Rank⭐⭐⭐⭐',
    'uragaan_marrowUragaan Marrow	High Rank⭐⭐⭐',
    'Uragaan Ruby (break back, cut tail)	High Rank⭐',
    'uragaan_jawUragaan Jaw (break head)	High Rank⭐⭐',
  ];

  static rewards = [
    'uragaan_carapaceUragaan Carapace	High Rank⭐⭐⭐⭐',
    'uragaan_scaleUragaan Scale +	High Rank⭐⭐⭐⭐',
    'uragaan_scuteUragaan Scute	High Rank⭐⭐⭐',
    'uragaan_marrowUragaan Marrow	High Rank⭐⭐',
    'monster_hardboneMonster Hardbone	High Rank⭐⭐⭐',
    'inferno_sacInferno Sac	High Rank⭐⭐⭐',
    'firecell_stoneFirecell Stone	High Rank⭐⭐',
  ];
}

class VaalHazak {
  static carves = [
    'Vaal Hazak Carapace	⭐⭐⭐⭐	 ',
    'Deceased Scale	⭐⭐⭐⭐⭐	 ',
    'Vaal Hazak Talon (break forelegs)	⭐⭐⭐	 ',
    'Vaal Hazak Fang + (break head)	⭐⭐	 ',
    'vaal_hazak_wingVaal Hazak Wing	⭐⭐⭐	 ',
    'Vaal Hazak Gem (break head or tail)	⭐',
    'Vaal Hazak Membrane (dropped, break chest)	⭐	 ',
    'vaal_hazak_tailVaal Hazak Tail (cut tail)	⭐',
  ];

  static rewards = [
    'Vaal Hazak Carapace	⭐⭐⭐⭐	 ',
    'Deceased Scale	⭐⭐⭐	 ',
    'vaal_hazak_wingVaal Hazak Wing	⭐⭐⭐	 ',
    'Vaal Hazak Membrane	⭐⭐⭐	 ',
    'Vaal Hazak Talon	⭐⭐⭐	 ',
    'elder_dragon_boneElder Dragon Bone	⭐⭐	 ',
    'elder_dragon_bloodElder Dragon Blood	⭐⭐⭐	 ',
    'Vaal Hazak Gem 	⭐',
  ];
}

class XenoJiiva {
  static carves = [
    'xenojiiva_shellXeno\'jiiva Shell	⭐⭐⭐⭐⭐	 ',
    'xenojiiva_soulscaleXeno\'jiiva Soulscale	⭐⭐⭐⭐	 ',
    'xenojiiva_clawXeno\'jiiva Claw (Break forelegs)	⭐⭐⭐	 ',
    'xenojiiva_hornXeno\'jiiva Horn (Break head)	⭐⭐	 ',
    'Xeno\'jiiva Wing (Break wing)	⭐⭐⭐	 ',
    'xenojiiva_gem_small-mhw-wiki-guideXeno\'jiiva Gem (break head, cut tail)	⭐',
    'xenojiiva_veilXeno\'jiiva Veil (Dropped material) 	⭐⭐⭐',
  ];

  static rewards = [
    'xenojiiva_shellXeno\'jiiva Shell	⭐⭐⭐⭐	 ',
    'xenojiiva_soulscaleXeno\'jiiva Soulscale	⭐⭐⭐	 ',
    'xenojiiva_veilXeno\'jiiva Veil	⭐⭐⭐	 ',
    'Xeno\'jiiva Wing	⭐⭐⭐	 ',
    'elder_dragon_boneElder Dragon Bone	⭐⭐⭐	 ',
    'elder_dragon_bloodElder Dragon Blood	⭐⭐⭐	 ',
    'xenojiiva_gem_small-mhw-wiki-guideXeno\'jiiva Gem	⭐',
  ];
}

class ZorahMagdaros {
  static carves = [
    // n/a
  ];

  static rewards = [
    'zorah_magdaros_heat_scaleZorah Magdaros Heat Scale	⭐⭐⭐⭐',
    'zorah_magdaros_ridgeZorah Magdaros Ridge	⭐⭐⭐⭐',
    'zorah_magdaros_pleuraZorah Magdaros Pleura (break chest)	⭐⭐⭐',
    'zorah_magdaros_magmaZorah Magdaros Magma (break head)	⭐⭐⭐',
    'zorah_magdaros_carapaceZorah Magdaros Carapace (dropped, break magmacore)	⭐⭐⭐',
    'zorah_magdaros_gemZorah Magdaros Gem	⭐',
  ];
}

class BlackDiablos {
  static carves = [
    'black_diablos_ridgeBlack Diablos Ridge + (dropped)	High Rank ⭐⭐⭐⭐',
    'Diablos Fang	High Rank ⭐⭐⭐⭐⭐',
    'black_diablos_carapaceBlack Diablos Carapace	High Rank ⭐⭐⭐',
    'Diablos Tailcase (cut tail)	High Rank ⭐⭐⭐',
    'blos_medullaBlos Medulla (break back)	High Rank ⭐⭐⭐',
    'wyvern_gemWyvern Gem (cut tail)	High Rank ⭐',
    'Black Spiral Horn + (break horns)	High Rank ⭐',
  ];

  static rewards = [
    'black_diablos_ridgeBlack Diablos Ridge +	High Rank ⭐⭐⭐⭐',
    'black_diablos_carapaceBlack Diablos Carapace	High Rank ⭐⭐⭐⭐',
    'Diablos Fang	High Rank ⭐⭐⭐',
    'blos_medullaBlos Medulla	High Rank ⭐⭐',
    'monster_hardboneMonster Hardbone	High Rank ⭐⭐⭐',
    'Dash Extract	High Rank ⭐⭐⭐',
    'wyvern_gemWyvern Gem	High Rank ⭐',
  ];
}

class AzureRathalos {
  static carves = [
    'Rathalos Scale +	⭐⭐⭐⭐⭐	',
    'azure_rathalos_carapaceAzure Rathalos Carapace	⭐⭐⭐⭐',
    'azure_rathalos_wingAzure Rathalos Wing	⭐⭐⭐⭐',
    'rathalos_medullaRathalos Medulla	⭐⭐⭐	',
    'rathalos_plateRathalos Plate	⭐	',
    'rathalos_rubyRathalos Ruby	⭐	',
    'azure_rathalos_tailAzure Rathalos Tail	⭐⭐',
  ];

  static rewards = [
    'azure_rathalos_carapaceAzure Rathalos Carapace	⭐⭐⭐⭐',
    'azure_rathalos_scale_plusAzure Rathalos Scale +	⭐⭐⭐⭐',
    'azure_rathalos_wingAzure Rathalos Wing	⭐⭐⭐	',
    'azure_rathalos_tailAzure Rathalos Tail	⭐⭐	',
    'rathalos_plateRathalos Plate	⭐	',
    'monster_hardboneMonster Hardbone	⭐⭐⭐	',
    'inferno_sacInferno Sac	⭐⭐⭐	',
    'rathalos_medullaRathalos Medulla	⭐⭐⭐',
    'rathalos_rubyRathalos Ruby	⭐	',
  ];
}

class PinkRathian {
  static carves = [
    'pink_rathian_carapacePink Rathian Carapace (dropped, break head)	High Rank⭐⭐⭐⭐	 ',
    'pink_rathian_scalePink Rathian Scale +	High Rank⭐⭐⭐⭐⭐	 ',
    'rath_wingtalonRath Wingtalon	High Rank⭐⭐⭐⭐	 ',
    'rathian_webbingRathian Webbing (break wings)	High Rank⭐⭐⭐	 ',
    'rathian_plateRathian Plate	High Rank⭐	 ',
    'Rathian Ruby (break back, carve tail, break head)	High Rank⭐',
    'rathian_spike_plusRathian Spike + (break back, carve tail)	High Rank⭐⭐',
  ];

  static rewards = [
    'pink_rathian_carapacePink Rathian Carapace	High Rank⭐⭐⭐⭐	 ',
    'pink_rathian_scalePink Rathian Scale +	High Rank⭐⭐⭐⭐	 ',
    'rath_wingtalonRath Wingtalon	High Rank⭐⭐⭐	 ',
    'rathian_webbingRathian Webbing	High Rank⭐⭐	 ',
    'rathian_plateRathian Plate	High Rank⭐	 ',
    'monster_hardboneMonster Hardbone	High Rank⭐⭐⭐	 ',
    'inferno_sacInferno Sac	High Rank⭐⭐⭐	 ',
    'Rathian Ruby	High Rank⭐',
  ];
}
