import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { ExpeditionsRoutingModule } from './expeditions-routing.module';
import { ExpeditionsComponent } from './expeditions.component';

@NgModule({
  imports: [
    SharedModule,
    ExpeditionsRoutingModule
  ],
  declarations: [
    ExpeditionsComponent
  ]
})
export class ExpeditionsModule { }
