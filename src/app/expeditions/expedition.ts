
import { Monster } from '../monsters/monster';
import { Data } from '../data/data';
import { Linkable } from '../shared/linkable';

export class Expedition implements Data {
  id: string;
  type = 'expedition';
  value: any;
}

export class ExpeditionDecorator implements Linkable {
  id: string;
  name: string;

  constructor(
    public expedition: Expedition
  ) {
    this.id = expedition.id;
    this.name = expedition.value;
  }

  url(): string {
    return `/expeditions/${this.id}`;
  }

  label(): string {
    return this.name;
  }
}
