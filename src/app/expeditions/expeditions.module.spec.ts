import { ExpeditionsModule } from './expeditions.module';

describe('ExpeditionsModule', () => {
  let expeditionsModule: ExpeditionsModule;

  beforeEach(() => {
    expeditionsModule = new ExpeditionsModule();
  });

  it('should create an instance', () => {
    expect(expeditionsModule).toBeTruthy();
  });
});
