import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExpeditionsComponent } from './expeditions.component';

const routes: Routes = [
  {
    path: ':id',
    component: ExpeditionsComponent
  },
  {
    path: '',
    component: ExpeditionsComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class ExpeditionsRoutingModule { }
