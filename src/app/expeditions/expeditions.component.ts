import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Params, ActivatedRoute } from '@angular/router';

import { DataService } from '../data/data.service';
import { Data, Collection } from '../data/data';

import { Expedition, ExpeditionDecorator } from '../expeditions/expedition';
import { Monster, MonsterDecorator } from '../monsters/monster';

@Component({
  selector: 'app-expeditions',
  templateUrl: './expeditions.component.html',
  styleUrls: ['./expeditions.component.scss']
})

export class ExpeditionsComponent implements OnInit {
  monsters: MonsterDecorator[] = [];
  expeditions: ExpeditionDecorator[] = [];
  expedition: ExpeditionDecorator;
  request: Params;

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private data: DataService
  ) {
    route.params.subscribe((params: Params) => {
      this.request = params;

      if (params.id) {
        this.getExpedition(params.id);
      } else {
        this.getExpeditions();
      }
    });
  }

  ngOnInit() { }

  getExpedition(id: string): void {
    const getMonsters = (monsters: Collection) => {
      this.monsters = monsters.all()
        .filter((monster: Monster) => monster.notes.has(id))
        .map((monster: Monster) => new MonsterDecorator(monster));
    };

    const getExpedition = (expedition: Expedition) => {
      this.expedition = new ExpeditionDecorator(expedition);

      this.data
        .getMonsters()
        .subscribe(getMonsters);
    };

    this.data
      .getExpedition(id)
      .subscribe(getExpedition);
  }

  getExpeditions(): void {
    const subscribe = (expeditions: Collection) => {
      this.expeditions = expeditions.all()
        .map((expedition: Expedition) => new ExpeditionDecorator(expedition))
        .sort((a: ExpeditionDecorator, b: ExpeditionDecorator) => {
          if (a.name < b.name) { return -1; }
          if (a.name > b.name) { return 1; }
          return 0;
        });
    };

    this.data
      .getExpeditions()
      .subscribe(subscribe);
  }

}
