import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';

import { DataService } from '../data/data.service';
import { Data, Collection, Decorator } from '../data/data';

import { Monster, MonsterDecorator } from '../monsters/monster';
import { Type, TypeDecorator } from './type';

@Component({
  selector: 'app-types',
  templateUrl: './types.component.html',
  styleUrls: ['./types.component.scss']
})

export class TypesComponent implements OnInit {
  monsters: MonsterDecorator[] = [];
  types: TypeDecorator[] = [];
  type: TypeDecorator;
  request: Params;

  constructor(
    private route: ActivatedRoute,
    private data: DataService
  ) {
    route.params.subscribe((params: Params) => {
      this.request = params;

      if (params.id) {
        this.getType(params.id);
      } else {
        this.getTypes();
      }
    });
  }

  ngOnInit() { }

  getTypes(): void {
    const subscribed = (types: Collection) => {
      this.types = types.all()
        .map((type: Type) => new TypeDecorator(type))
        .sort((a: TypeDecorator, b: TypeDecorator) => {
          if (a.name < b.name) { return -1; }
          if (a.name > b.name) { return 1; }
          return 0;
        });
    };

    this.data
      .getTypes()
      .subscribe(subscribed);
  }

  getType(id: string): void {
    const getMonsters = (monsters: Collection) => {
      this.monsters = monsters.all()
        .filter((monster: Monster) => {
          return monster.notes.find((data: Data) => data.id === id) !== undefined;
        })
        .map((monster: Monster) => new MonsterDecorator(monster));
    };

    const getType = (type: Type) => {
      this.type = new TypeDecorator(type);
      this.data
        .getMonsters()
        .subscribe(getMonsters);
    };

    this.data
      .getType(id)
      .subscribe(getType);
  }

}
