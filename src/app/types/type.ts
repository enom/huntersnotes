import { Data } from '../data/data';
import { Linkable } from '../shared/linkable';

export class Type implements Data {
  id: string;
  type = 'type';
  value: any;
}

export class TypeDecorator implements Linkable {
  type: Type;
  id: string;
  name: string;

  constructor(
     type: Type
  ) {
    this.type = type;
    this.id = type.id;
    this.name = type.value;
  }

  url(): string {
    return `/monsters/${this.id}`;
  }

  label(): string {
    return this.name;
  }

}
