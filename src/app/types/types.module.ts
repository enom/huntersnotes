import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { TypesRoutingModule } from './types-routing.module';
import { TypesComponent } from './types.component';

@NgModule({
  imports: [
    SharedModule,
    TypesRoutingModule
  ],
  declarations: [
    TypesComponent
  ]
})

export class TypesModule { }
