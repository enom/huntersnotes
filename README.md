# Huntersnotes

Web based Hunter's Notes from Monster Hunter World.

## Large Monsters

All large monsters and their notes with relevant wiki and in-game data.

## Carves and Rewards

Lists all large monsters carves and rewards and their related monsters. (See #3)

## Small Monsters and Endemic Life

A few entries here but doing data input for these isn't the current focus of the project.

# Helping Out

Please ask me before jumping in and making merge requests. This is still an emerging prototype and won't have a stable branch until v1 is released.

But if you really want to, you can clone the repo and run anglur-cli commands(e.i. `ng serve`).
